#!/bin/sh

set -x -e

cat >/etc/locale.gen <<EOF
en_US.UTF-8     UTF-8
fr_FR.UTF-8     UTF-8
EOF

locale-gen

cd /
apply-patches /context/patches/*.diff

rm -rf /context
