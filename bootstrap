#!/bin/bash

CONTAINERS="dev-redis dev-mysql dev-controller dev-ssh dev-django dev-smtpsink dev-registry dev-nginx dev-toolbox"


die()
{
	echo "error: $*" >&2
	exit 1
}

# generate the .env file for docker-compose
#
# DOCKERUSER is set to the uid:gid of the current user
#
# If the file already exists, it will never be replaced. The function just
# prints a warning if it differs from the would-be generated file.
generate_env_file()
{
	cat >".env.tmp" <<EOF
DOCKERUSER=`id -u`:`id -g`
EOF
	if [ ! -e .env ] ; then
		mv .env.tmp .env
	else
		if cmp --quiet .env .env.tmp ; then
			rm .env.tmp
		else
			echo "warning: config file '.env' already exists and is different from the generate config. Remove it if you want it to be overwritten."
			diff -u .env .env.tmp || true
		fi
	fi
}

install_secrets()
{
	# install the tokens certificate into the registry external volume
	local src=data/django/ro/certs/tokens.crt
	local dst=data/registry/ro/certs/tokens.crt
	if [ -f "$src" ] && [ -d "`dirname "$dst"`" ] && [[ "$TODO" =~ dev-django|dev-registry ]] ; then
		echo "Install tokens certificate as $dst/tokens.crt"
		cp "$src" "$dst"
		docker-compose restart dev-registry dev-django
	fi

	# install the controller token into the controller external volume
	local src=data/django/ro/controller_token
	local dst=data/controller/ro/config.yml
	if [ -f "$src" ] && [ -f "$dst" ] && [[ "$TODO" =~ dev-django|dev-controller ]] ; then
		echo "Install controller token into $dst"
		# copy the file with a docker command because it is owned by root
		docker run --rm -i -v "$PWD/$dst:/config.yml" busybox sh -c "cat >>/config.yml" <<EOF
registry_auth: {"username": "token", "password": "`cat "$src"`"}
EOF
		docker-compose restart dev-controller
	fi
}

# remove container and its data
purge_container()
{
	local name="$1"

	(set -x ; docker-compose rm -f "$name")

	# ensure $name is well formatted (to avoid disasters with rm -rf)
	[[ "$name" =~ ^dev-[a-z][a-z0-9-]*$ ]] || die "bad container name: $name"
	local data_dir="data/${name/dev-/}"

	if [ -e "$data_dir" ] ; then
		if [ -z "$FORCE" ] ; then
			echo "warning: data dir '$data_dir' already exists"
			echo -n "remove it [y/N]? "
			read confirm
			[ "$confirm" = "y" ] || die "aborted"
		fi

		(set -x ; rm -rf -- "$data_dir")
	fi
}

# init a container
init_container()
{
	local name="$1"
	local data_dir="data/${name/dev-/}"

	(set -x

	# create the directory before running the container
	# so that it is owned by the current user (not by root)
	mkdir -p -- "$data_dir"


	# run the /dk/container_init script if present
	#
	# FIXME: the "sleep 1" is because of a race condition in
	# docker-compose (if the command finishes too quickly, then it
	# is run twice)
	docker-compose run --rm "$name" sh -e -c \
	  'if [ -f /dk/container_init ] ; then sleep 1; /dk/container_init ; fi'

	# start the container
	docker-compose up -d "$name"
	)
}

# seed the db & registry
seed()
{
	if [ -z "$NOSEED" ] ; then
		if [[ "$TODO" =~ "dev-django" ]] ; then
			(set -x ; django/tools/seed-dev.sh --django dev localhost:5000)
		fi
		if [[ "$TODO" =~ "dev-registry" ]] ; then
			(set -x ; django/tools/seed-dev.sh --registry dev localhost:5000)
		fi
	fi
}

##################################################################


if [ ! -f docker-compose.yml ] || [ ! -f bootstrap ] ; then
	die "the 'bootstrap' script must be run from the root of the allgo repository"
fi

if [ "$1" == "-h" ] ; then
	cat <<EOF
usage: $0 [-n|--nobuild|--noseed] [CONTAINER ...]

The bootstrap script initialises the environment and the selected containers
(or by default all the containers).

If a container is already initialised, then the script asks for confirmation
before purging its data (to bootstrap it again).

Options:
  -n,--nobuild	do not rebuild the images
  --noseed	do not seed the db & registry
  --nostart	do not start the containers at the end (actually this stops the
  		containers at the end of the process because they need to be
		started to be initialised)
  --force	do not ask for confirmation before deleting data (removing
  		external volumes when bootstraping a container over an existing one)

EOF
fi

NOBUILD=
NOSEED=
NOSTART=
FORCE=
while true ; do
	case "$1" in
	-n|--nobuild)
		NOBUILD=1
		shift;;
	--noseed)
		NOSEED=1
		shift;;
	--nostart)
		NOSTART=1
		shift;;
	--force)
		FORCE=1
		shift;;
	*)
		break;;
	esac
done

# selection of the containers to be generated
if [ -n "$*" ] ; then
	TODO="$*"
else
	docker-compose down
	TODO="$CONTAINERS"
fi

set -e

# purge containers (container + external volumes) that will be bootstraped
for name in $TODO
do
	purge_container "$name"
done

generate_env_file

if [ -z "$NOBUILD" ] ; then
	# build base image (if not present)
	(set -x ; make base-debian)

	# build the requested images images
	docker-compose build $TODO
fi

# initialise the containers
for name in $TODO
do
	init_container "$name"
done

install_secrets

# force restarting the nginx frontend because the IP address of
# dev-django/dev-registry may have changed (and nginx does not support IP
# address changes in upstream servers)
docker-compose restart dev-nginx

seed

if [ -n "$NOSTART" ] ; then
	docker-compose down
fi

# display running containers
docker-compose ps
