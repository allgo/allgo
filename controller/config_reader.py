#!/usr/bin/python3

import collections
import logging
import types

import yaml

class ConfigError(Exception):
    pass

class _Error(Exception):
    @property
    def key(self):
        return self.args[0]

    @property
    def msg(self):
        return self.args[1]

class _KeyError(_Error, KeyError):
    pass

class _UNSET:
    pass

class _Container:

    def _set_parent(self, obj, key):
        assert "_parent" not in self
        self._parent = obj, key

    def _read_value(self, value, type, cast, key=_UNSET):
        # check type
        if not ( isinstance(value, type) or
                (isinstance(value, Sequence) and (type is list)) or
                (isinstance(value, Mapping)  and (type is dict))):
            raise _Error(self.path(key), "type error: value %r is not a %s" % (value, type.__name__))

        # convert value
        try:
            return cast(value)
        except Exception as e:
            raise _Error(self.path(key), str(e))

    def path(self, key=_UNSET):
        """Return the path of entry relative to the root of the tree
        
        if `key` is provided, then the function displays the path of the given
        item, otherwise it returnt the path to `self`
        
        """
        if key is _UNSET:
            obj, key = getattr(self, "_parent", (None, None))
        else:
            obj = self

        lst=[]
        while obj is not None:
            lst.append(obj._path_elem(key))
            obj, key = getattr(obj, "_parent", (None, None))

        return "".join(reversed(lst))

class Mapping(_Container):
    """A read-only dict-like structure that tracks unused keys

    This class provides the collections.abc.Mutable API plus .path() and extra
    args in .get()
    """

    class _Enumerator:
        def __init__(self, dct, func):
            self._dict = dct
            self._func = func

        def __len__(self):
            return len(self._dict)

        def __iter__(self):
            for key, val in self._dict._iter_items():
                yield self._func(key, val)

    def __init__(self, values=()):
        self._dict   = types.MappingProxyType(collections.OrderedDict(values))
        self._unused = set(self._dict)
        for k, v in self._dict.items():
            if isinstance(v, _Container):
                v._set_parent(self, k)

    def __len__(self):
        return len(self._dict)

    def __contains__(self, key):
        return key in self._dict

    def __getitem__(self, key):
        return self.get(key, required=True)

    def get(self, key, default=None, type=object, *, cast=lambda x:x, required=False):
        """Get an elemment of the Mapping

        This function gets the value of entry `key` in the Mapping
        - if found :
          - ensure that the value is an instance of `type` or raise _Error
          - return the result of `cast`(value) or raise _Error in case `cast`
            return an exception
        - if not found :
          - if `required` is true, then raise _KeyError
          - otherwise return the value of `default` 

        NOTE: if `default` is a dict then it is implicitely converted to a Mapping
        """
        self._unused.discard(key)
        result = self._dict.get(key, _UNSET)
        if result is not _UNSET:
            return self._read_value(result, type, cast, key)
        elif required:
            raise _KeyError(self.path(key), "config key is missing")
        elif isinstance(default, dict):
            # implicitely create a mapping
            dct = Mapping(default)
            dct._set_parent(self, key)
            return dct
        else:
            return default

    def __iter__(self):
        return iter(self._dict)

    def keys(self):
        return self._dict.keys()

    def _iter_items(self):
        for key, val in self._dict.items():
            self._unused.discard(key)
            yield key, val

    def values(self):
        return self._Enumerator(self, lambda k,v: v)

    def items(self):
        return self._Enumerator(self, lambda k,v: (k,v))

    def _path_elem(self, key):
        return ".%s" % key

    def _log_warnings(self, logger):
        for key, val in self._dict.items():
            if key in self._unused:
                logger.warning("unused config key %s", self.path(key))
            elif isinstance(val, _Container):
                val._log_warnings(logger)

class Sequence(tuple, _Container):
    """A read-only list-like structure

    This class provides the collections.abc.Sequence API plus .path()
    """

    def __init__(self, values=()):
        super().__init__()
        for k, v in enumerate(self):
            if isinstance(v, _Container):
                v._set_parent(self, k)

    def _path_elem(self, key):
        return "[%d]" % key

    def _log_warnings(self, logger):
        for val in self:
            if isinstance(val, _Container):
                val._log_warnings(logger)


class _Loader(yaml.SafeLoader):
    pass
yaml.add_constructor(
        yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
        lambda loader, node: Mapping(loader.construct_pairs(node)),
        _Loader)
yaml.add_constructor(
        yaml.resolver.BaseResolver.DEFAULT_SEQUENCE_TAG,
        lambda loader, node: Sequence(loader.construct_sequence(node)),
        _Loader)

class ConfigReader:
    """A class for reading a YAML config file

    Usage:
        with ConfigReader(open(filename), logger) as cfg:
            val1 = cfg["key1"]
            ...
    
    When entering the context, this class parses the provided yaml source and
    returns a read-only object to access its content.
    
    Within the context the class tracks which config keys are accessed.
    
    When leaving the context and in the absence of any error, the class warns
    about unused config keys. In case of error (any exception) it logs the
    error and raise ConfigError.

    The read-only object (returned when entering the context) is a tree where
    all instances of dict and list are replaced with Mapping and Sequence. Also
    the order of dicts is preserved.

    All warning and error messages are reported to the logger, with the path to
    the relevant entry within the tree.
    """

    def __init__(self, source, logger=logging.root):
        self._log = logger
        self._source = source

    def _report_error(self, exc_value):
        if isinstance(exc_value, _Error):
            msg = "%s: %s" % (exc_value.key, exc_value.msg)
            self._log.error("%s", msg)
        elif isinstance(exc_value, yaml.YAMLError):
            msg = "yaml error: %s" % str(exc_value)
            self._log.error("%s", msg)
        else:
            msg = "exception in config reader"
            self._log.exception("%s", msg)
        raise ConfigError(msg)

    def __enter__(self):
        assert not hasattr(self, "value")
        name = getattr(self._source, "name", None)
        if name:
            self._log.info("reading config file %s", name)
        try:
            self.value = yaml.load(self._source, _Loader)
        except yaml.YAMLError as e:
            self._report_error(e)
        finally:
            if hasattr(self._source, "close"):
                self._source.close()
        return self.value

    def __exit__(self, exc, val, tb):
        assert hasattr(self, "value")
        v = self.value
        del self.value
        if exc is not None:
            self._report_error(val)
        elif isinstance(v, _Container):
            v._log_warnings(self._log)


