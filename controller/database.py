#!/usr/bin/python3 -i
import datetime
import enum

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, Table, Text, Boolean, DateTime
from sqlalchemy.orm import relationship, sessionmaker

__all__ = "Webapp", "WebappVersion", "DockerOs", "Job", "connect_db", "SandboxState", "VersionState", "JobState", "JobQueue", "JobResult"

Base = declarative_base()


class SandboxState(enum.IntEnum):
    IDLE        = 0
    RUNNING     = 1
    STARTING    = 2
    START_ERROR = 3
    STOPPING    = 4
    STOP_ERROR  = 5

class VersionState(enum.IntEnum):
    SANDBOX     = 0
    COMMITTED   = 1
    READY       = 2
    ERROR       = 3
    DELETED     = 4
    USER        = 5
    IMPORT      = 6


class JobState(enum.IntEnum):
    NEW         = 0
    WAITING     = 1
    RUNNING     = 2
    DONE        = 3
    ARCHIVED    = 4
    DELETED     = 5
    ABORTING    = 6

class JobResult(enum.IntEnum):
    NONE        = 0
    SUCCESS     = 1
    ERROR       = 2
    ABORTED     = 3
    TIMEOUT     = 4


class Webapp(Base):
    __tablename__ = "dj_webapps"

    id              = Column(Integer, primary_key=True)
    docker_name     = Column(String)
    sandbox_state   = Column(Integer)
    sandbox_version_id = Column(Integer, ForeignKey('dj_webapp_versions.id'))
    docker_os_id    = Column(Integer, ForeignKey('dj_docker_os.id'))
    exec_time       = Column(Integer)
    entrypoint      = Column(String)
    memory_limit    = Column(Integer)

    versions        = relationship("WebappVersion", foreign_keys="[WebappVersion.webapp_id]")
    sandbox_version = relationship("WebappVersion", foreign_keys=[sandbox_version_id])
    docker_os = relationship("DockerOs")

    def __repr__(self):
        return "%s(%s)" % (type(self).__name__, ", ".join((
            "%s=%r" % (k, getattr(self, k)) for k in (
                "id", "docker_name", "sandbox_state", "docker_os_id"))))

    def __setattr__(self, key, value):
        if key == "sandbox_state" and isinstance(value, SandboxState):
            value = int(value)
        super().__setattr__(key, value)

class WebappVersion(Base):
    __tablename__ = "dj_webapp_versions"

    id              = Column(Integer, primary_key=True)
    webapp_id       = Column(Integer, ForeignKey('dj_webapps.id'))
    created_at      = Column(DateTime, default=datetime.datetime.now)
    updated_at      = Column(DateTime, default=datetime.datetime.now)
    deleted_at      = Column(DateTime, nullable=True)
    number          = Column(String)
    description     = Column(String)
    published       = Column(Boolean)
    state           = Column(Integer)

    webapp = relationship("Webapp", foreign_keys=[webapp_id])

    def __setattr__(self, key, value):
        if key == "state" and isinstance(value, VersionState):
            value = int(value)
        super().__setattr__(key, value)

class DockerOs(Base):
    __tablename__ = "dj_docker_os"

    id              = Column(Integer, primary_key=True)
    docker_name     = Column(String)
    version         = Column(String)

    def __repr__(self):
        return "%s(%s)" % (type(self).__name__, ", ".join((
            "%s=%r" % (k, getattr(self, k)) for k in (
                "id", "docker_name", "version"))))

class JobQueue(Base):
    __tablename__ = "dj_job_queues"

    id              = Column(Integer, primary_key=True)
    name            = Column(String)
    timeout         = Column(Integer)

class Job(Base):
    __tablename__ = "dj_jobs"

    id              = Column(Integer, primary_key=True)
    webapp_id       = Column(Integer, ForeignKey('dj_webapps.id'))
    user_id         = Column(Integer)
    state           = Column(Integer)
    result          = Column(Integer)
    param           = Column(String)
    version         = Column(String)
    exec_time       = Column(Integer)
    access_token    = Column(String)
    container_id    = Column(String(64))
    queue_id        = Column(Integer, ForeignKey('dj_job_queues.id'))
    files           = Column(Text)


    webapp = relationship("Webapp")
    queue  = relationship("JobQueue")

    def __repr__(self):
        return "%s(%s)" % (type(self).__name__, ", ".join((
            "%s=%r" % (k, getattr(self, k)) for k in (
                "id", "state"))))

def connect_db(host, *, echo=False):
    # NOTE: max connexion lifetime set to 1 hour to avoid 'mysql server gone away' errors
    return sessionmaker(create_engine("mysql://allgo:allgo@%s/allgo" % host, echo=echo,
        pool_size=20, pool_recycle=3600), autocommit=True)


if __name__ == "__main__":
    session = ses = connect_db("dev-mysql", echo=True)()
