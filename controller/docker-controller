#!/usr/bin/python3

import argparse
import contextlib
import logging.handlers
import re
import os
import sys

import fasteners

import config_reader
import controller

# TODO: deploy to prod

log = controller.log

def die(msg, *k):
    print("error:", msg % k, file=sys.stderr)
    sys.exit(1)


def init_logging(log_level):
    logging.basicConfig(
            level   = min(log_level, logging.INFO),
            format  = "%(asctime)s %(levelname)-8s %(name)-24s %(message)s",
            datefmt = "%Y-%b-%2d %H:%M:%S",
            )

    console_handler = logging.root.handlers[0]
    def add_handler(level, hnd):
        hnd.setLevel(level)
        hnd.setFormatter(console_handler.formatter)
        logging.root.addHandler(hnd)

    os.makedirs("/vol/log", exist_ok=True)

    # normal logs
    add_handler(logging.INFO, logging.handlers.TimedRotatingFileHandler(
        "/vol/log/controller.log",
        when = "W6",        # rotate every sunday
        backupCount = 52))  # keep 1 year of logs

    # debug logs
    if log_level <= logging.DEBUG:
        add_handler(logging.DEBUG, logging.handlers.TimedRotatingFileHandler(
            "/vol/log/debug.log",
            when = "D",         # rotate every day
            backupCount = 7))   # keep a week of logs

    return console_handler

def main():
    @contextlib.contextmanager
    def get_envvar(name):
        value = os.environ.get(name, "")
        try:
            yield value
        except:
            logging.exception("")
            die("invalid environment variable %s=%r", name, value)

    def parse_path(path):
        if not path:
            raise ValueError("path is empty")
        if not os.path.isabs(path):
            raise ValueError("path is not absolute")
        return "/" + path.strip("/")

    def log_var(desc, val):
        log.info("%-21s %r", desc, val)

    def cfg_positive_int(name, desc):
        with get_envvar(name) as val:
            result = int(val)
            if result <= 0:
                raise ValueError("must provide a positive value")
            log_var(desc, result)
            return result

    with get_envvar("DEBUG") as val:
        log_level = logging.DEBUG if bool(int(val or 0)) else logging.WARNING

    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--debug",   action="store_true")
    parser.add_argument("-v", "--verbose", action="store_true")
    args = parser.parse_args()
    if args.debug:
        log_level = logging.DEBUG
    if args.verbose:
        log_level = min(log_level, logging.INFO)

    console_handler = init_logging(log_level)
    console_handler.setLevel(min(log_level, logging.INFO))
    log.info("---- docker controller started ----")
    log.info("log level:            %s", logging.getLevelName(log_level))


    with get_envvar("ENV") as env:
        re.match("[a-z][a-z0-9_-]*\Z", env).groups()
        log.info("environment:          %s", env)

    with get_envvar("REGISTRY") as val:
        re.match("[a-z0-9_.-]+:\d+(/[a-z0-9_./-]+)?\Z", val, re.I).groups()
        registry = val
        log.info("registry:             %s", registry)

    with get_envvar("DATASTORE_PATH") as val:
        datastore_path = parse_path(val.format(ENV=env))
        log.info("datastore path        %s", datastore_path)

    with get_envvar("SANDBOX_PATH") as val:
        sandbox_path = parse_path(val.format(ENV=env))
        log.info("sandbox path          %s", sandbox_path)

    with get_envvar("TOOLBOX_PATH") as val:
        toolbox_path = parse_path(val.format(ENV=env))
        log.info("toolbox path          %s", toolbox_path)

    with get_envvar("SANDBOX_NETWORK") as val:
        re.match(r"\A[\w]+[\w. _-]*[\w]+\Z", val)
        sandbox_network = val
        log.info("sandbox network       %s", sandbox_network)

    with get_envvar("JOB_USER") as val:
        re.match(r"\d+:\d+\Z", val).groups()
        job_user = val
        log.info("run jobs as user      %s", job_user)

    docker_host = os.environ.get("DOCKER_HOST")
    swarm_host  = os.environ.get("SWARM_HOST")
    log.info("docker host           %s", docker_host)
    log.info("swarm host            %s", swarm_host)

    with get_envvar("MYSQL_HOST") as val:
        mysql_host = val.format(ENV=env)
        log.info("mysql host            %s", mysql_host)

    with get_envvar("ALLGO_REDIS_HOST") as val:
        redis_host = val.format(ENV=env)
        log.info("redis host            %s", redis_host)

    with get_envvar("ALLGO_IMPORT_REGISTRY") as val:
        import_registry = val.format(ENV=env)
        log.info("import registry       %s", import_registry)

    os.makedirs("/vol/cache", exist_ok=True)
    lockfile = "/vol/cache/controller.lock"
    lock = fasteners.InterProcessLock(lockfile)
    lock.acquire(timeout=10) or die("unable to lock %r (there is another running controller)", lockfile)
    try:
        log.info("---- docker controller ready ----")
        console_handler.setLevel(logging.DEBUG if args.debug else (
            logging.INFO if args.verbose else logging.WARNING))

        return controller.DockerController(docker_host, swarm_host, mysql_host,
                registry, env, datastore_path, sandbox_path,
                toolbox_path, sandbox_network, redis_host,
                job_user, import_registry).run()
    except config_reader.ConfigError:
        log.critical("bad config")
        sys.exit(1)
    finally:
        lock.release()

        console_handler.setLevel(logging.INFO)
        log.info("---- docker controller terminated ----")

        # NOTE: this is a crude hack to terminate the process immediatly,
        # without joining the background threads (which may still be running
        # because of the PushManager/PullManager implementation)
        import concurrent.futures.thread
        concurrent.futures.thread._threads_queues.clear()

main()
