#!/usr/bin/python3

# TODO: documentation
#
# TODO: implement the slave client
#

import asyncio
import collections
import concurrent.futures
import contextlib
import itertools
import json
import logging
import re
import socket
import threading
import time

import docker
import requests
import yaml

import swarm_abstraction

_UNITS = collections.OrderedDict([
    (" ",  1),
    ("K", 1024),
    ("M", 1024**2),
    ("G", 1024**3),
    ("T", 1024**4),
])

log=logging.getLogger("shared_swarm")

AnyDockerError = docker.errors.APIError, requests.RequestException, requests.urllib3.exceptions.HTTPError

def _parse_exitcode(txt):
    if txt is not None:
        return int(txt)

def _parse_sockaddr(txt, *, default_host):
    mo = re.match(r"(?:([^:]+):)?(\d+)\Z", str(txt))
    if mo is None:
        raise ValueError("invalid socket address: %r" % (txt,))
    host = mo.group(1) or default_host
    port = int(mo.group(2))
    return host, port


class _InitReprMixin:
    def __init__(self, **kw):
        assert all(key in self.__slots__ for key in kw), "unknown attribute"
        for name in self.__slots__:
            setattr(self, name, kw.get(name, None))

    def __repr__(self):
        return "%s(%s)" % (self.__class__.__name__, ",".join(
            "%s=%r" % (k, getattr(self, k)) for k in self.__slots__))

class Error(Exception):
    pass

class ShuttingDown(Error):
    pass

class SlotTooBig(Error):
    pass

class AlreadyRequested(Error):
    pass

def rate_limit(period, func=time.sleep):
    """Generator for rate limiting
    
    This function ensures we spend at least `period` seconds for each iteration
    """
    assert period > 0

    t0 = time.monotonic()
    while True:
        yield
        t1 = time.monotonic()
        delay = t0 - t1 + period
        if delay > 0:
            log.debug("rate_limit: sleep %f seconds", delay)
            func(delay)
            t0 = t1 + delay
        else:
            t0 = t1


def fmt_value(num):
    ref_value= num if num >= 0 else (-num*10)
    for unit in reversed(_UNITS):
        unit_value = _UNITS[unit]
        if ref_value >= unit_value:
            break
    v = str(num / unit_value)
    i = v.index(".")
    if i > 4 or "e" in v:
        return "%.0e" % num
    v = v[:(3 if i==3 else 4)]

    return "%4s%s" % (v, unit)

def fmt_requested_resource(number, percent):
    if number is None and percent is None:
        return "---------"
    return "%s %02d%%" % (fmt_value(number), percent*100)

def _start_thread(target, **kw):
    """create and synchronously start a thread
    
    target  function to be run inside the thread
    **kw    additional parameters for threading.Thread()
    
    This function creates a thread that calls target(*args, ready, **kwargs)
    where:
     - ready is a threading.Event object
     - *args, **kwargs are the optional extra arguments given in **kw (see
       threading.Thread())

    Then it waits until either:
     - the ready event is set
     - the target function terminates its execution

    If the target function raise an exception before setting the ready event,
    then the exception is propagated into the main thread, otherwise the newly
    created thread is returned.
    """

    ready = threading.Event()
    exc   = None
    def wrapper(*args, **kwargs):
        nonlocal exc
        try:
            return target(*args, ready=ready, **kwargs)
        except BaseException as e:
            log.exception("unhandled exception in thread %r" % th.name)
            if not ready.is_set():
                exc = e
        finally:
            ready.set()

    th = threading.Thread(target=wrapper, **kw)
    th.start()
    ready.wait()
    if exc is not None:
        raise exc
    return th

def _enable_tcp_keepalive(sock):
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
    sock.setsockopt(socket.SOL_TCP, socket.TCP_KEEPIDLE, 60)
    sock.setsockopt(socket.SOL_TCP, socket.TCP_KEEPCNT,   9)
    sock.setsockopt(socket.SOL_TCP, socket.TCP_KEEPINTVL, 1)


class _ResourceManager:

    def __init__(self, client, *, events=[], alias=None):
        self.client    = client
        self.log       = logging.getLogger(log.name if alias is None else
                "%s[%s]" % (log.name, alias))
        self._shutdown = threading.Event()

        self._events_thread = _start_thread(self._events_thread_func,
                kwargs={"events": events}, daemon=True)

    def _sleep(self, seconds):
        """Sleep function

        raise ShuttingDown if .shutdown() is called before the seconds are elapsed
        """
        if self._shutdown.wait(seconds):
            raise ShuttingDown()

    def shutdown(self):
        self._shutdown.set()

    def wait_shutdown(self):
        # NOTE: we do not wait for the termination of self._event_thread
        #       because there is no easy way of interrupting the generator
        #       (receiving the next event from the HTTP response), but this
        #       is ok since
        #       - self._event_thread is a daemon thread
        #       - once self._shutdown is set the thread won't do anything
        #TODO: wait for the termination of the refresh_thread ?
        assert self._shutdown.is_set(), "shutdown was not requested"

    def _events_thread_func(self, ready, events):
        self.log.debug("docker event monitor started")
        assert isinstance(events, list)

        try:
            # limit docker client requests to one per minute
            # (in case the docker daemon has errors)
            limiter = rate_limit(60, self._sleep)

            while True:
                next(limiter)
                try:
                    generator = self.client.events(filters={"event": events})
                    ready.set()

                    for event_bytes in generator:
                        if self._shutdown.is_set():
                            return

                        self.log.debug("docker event %r", event_bytes)
                        self._dispatch_event(json.loads(event_bytes.decode()))
                        self.log.debug("docker event dispatched")
                except AnyDockerError as e:
                    self.log.error("docker event monitor error (%s)", e)
                except Exception:
                    self.log.exception("docker event monitor exception")

        except ShuttingDown:
            pass
        finally:
            self.log.debug("docker event monitor terminated")


    def _dispatch_event(self, event):
        raise NotImplementedError()     #pragma: nocover

    @contextlib.contextmanager
    def request_slot(self, name, cpu, mem):
        raise NotImplementedError()     #pragma: nocover

    @asyncio.coroutine
    def wait_slot(self, name):
        raise NotImplementedError()     #pragma: nocover

    def container_created(self, container_id, name):
        raise NotImplementedError()     #pragma: nocover


    @asyncio.coroutine
    def wait(self, container_id):
        """Wait for the termination of a container

        Notes:
         - `container_id` *must* be the full container id (64 digits)
         - the class support only one concurrent waiter for each container id

        """
        raise NotImplementedError()     #pragma: nocover


class _SlaveResourceManager(_ResourceManager):

    def __init__(self, *k, config={}, **kw):
        # lock protecting the internal state variables
        # (shall never be locked for long periods)
        self._lock = threading.Lock()

        # {container_id: asyncio.Future}
        self._waiters = {}
        self._loop = asyncio.get_event_loop()

        # {name, client task}
        self._tasks = {}

        super().__init__(*k, events=["die"], **kw)

        self._read_config(config)

    def _read_config(self, cfg):
        self.log.debug("reading config")
        self._master_addr = cfg.get("connect", cast=lambda x:
                _parse_sockaddr(str(x), default_host="127.0.0.1"))
        self.log.info("using master swarm controller at %s:%s", *self._master_addr)


    @contextlib.contextmanager
    def request_slot(self, name, cpu, mem):

        # future that will store the reply from the master
        fut = asyncio.Future()

        @asyncio.coroutine
        def manage_request():
            writer = None
            try:
                self.log.debug("%s: connect to master", name)
                while True:
                    try:
                        reader, writer = yield from asyncio.open_connection(*self._master_addr)
                    except IOError as e:
                        log.error("unable to connect to master for %s (%s)", name, e) 
                        yield from asyncio.sleep(60)
                    else:
                        break

                self.log.debug("%s: connected to master", name)

                _enable_tcp_keepalive(writer.transport.get_extra_info("socket"))

                writer.write(("%s %d %d\n" % (name, cpu, mem)).encode())

                result = (yield from reader.readline()).decode()
                if not result:
                    raise RuntimeError("EOF from master")
                self.log.debug("%s: reply from master: %r", name, result)
                mo = re.match("(id|error)=(.*)$", result)
                if not mo:
                    raise RuntimeError("Malformatted reply from the master: %r" % result)
                key, val = mo.groups()
                if key == "error":
                    if val in ("ShuttingDown", "SlotTooBig", "AlreadyRequested"):
                        fut.set_exception(globals()[val]())
                    else:
                        fut.set_exception(Error(val))
                else:
                    assert key == "id"
                    fut.set_result(val)

                # wait until the coroutine is cancelled
                yield from asyncio.Future()

            except asyncio.CancelledError as e:
                if not fut.done():
                    fut.set_exception(ShuttingDown() if self._shutdown.is_set() else e)
            except Exception as e:
                if not fut.done():
                    fut.set_exception(e)
            finally:
                if writer is not None:
                    writer.close()
                    self.log.debug("%s: disconnected from master", name)

        task = None
        try:
            with self._lock:
                if name in self._tasks:
                    raise AlreadyRequested(name)

                task = self._tasks[name] = self._loop.create_task(manage_request())
                task.__future = fut
            yield
        finally:
            if task is not None:
                with self._lock:
                    task.cancel()
                    del self._tasks[name]


    @asyncio.coroutine
    def wait_slot(self, name):
        task = self._tasks[name]
        return (yield from asyncio.shield(task.__future))

    def container_created(self, cid, name):
        pass

    def shutdown(self):
        super().shutdown()
 
        @self._loop.call_soon_threadsafe
        def stop():
            # terminate all pending tasks
            with self._lock:
                for task in self._tasks.values():
                    task.cancel()

            # terminate all pending waiters
            for fut in self._waiters.values():
                if not fut.done():
                    fut.set_exception(ShuttingDown())


    def _dispatch_event(self, event):
        if event.get("status") == "die":
            cid = event["id"]
            code = _parse_exitcode(event["Actor"]["Attributes"].get("exitCode"))
            fut = self._waiters.get(cid)
            if fut is not None and not fut.done():
                self._loop.call_soon_threadsafe(fut.set_result, code)

    @asyncio.coroutine
    def wait(self, container_id):
        assert re.match(r"[0-9a-f]{64}\Z|<<ID ", container_id), "bad container id"
        assert asyncio.get_event_loop() == self._loop

        if container_id in self._waiters:
            raise RuntimeError("there is already a watch for this container")

        if self._shutdown.is_set():
            raise ShuttingDown()

        self._waiters[container_id] = fut = asyncio.Future()
        try:
            ctr = self.client.inspect_container(container_id)
            if ctr["State"]["Running"]:
                self.log.debug("wait for container:   %s", container_id)
                returncode = yield from fut
            else:
                returncode = ctr["State"]["ExitCode"]
            self.log.debug("container terminated: %s", container_id)
            return returncode
        except docker.errors.NotFound:
            self.log.debug("container not found: %s", container_id)
            raise KeyError(container_id)
        finally:
            del self._waiters[container_id]


class _MasterResourceManager(_ResourceManager):


    def __init__(self, *k, config={}, **kw):
        self._loop = asyncio.get_event_loop()

        # lock protecting the internal state variables
        # (shall never be locked for long periods)
        self._lock = threading.Lock()

        # lock for the events feed from the docker daemon
        # (may be locked for extended time -> when refresh is in progress)
        self._lock_events = threading.Lock()

        # groups:       [Group]
        self._groups = []

        # nodes:        [Node]
        self._nodes = collections.OrderedDict()


        ##########  execution slots  ##########
        # requested slots (within .request_slot())
        # {name: Slot}
        self._requested = collections.OrderedDict()

        # unallocated slots (i.e: containers created by a 3rd party and that we
        # have not yet inspected)
        # {Slot}
        self._unallocated = set()

        # created slots
        # {container_id: Slot}
        self._created = {}

        #######################################
        
        self.log = log     # because super().__init__() is not yet called
        self._read_config(config)

        super().__init__(*k, events=["create", "die", "destroy"], **kw)

        # start the refresh thread
        self._refresh_thread = _start_thread(self._refresh_thread_func, daemon=True)

        # start the tcp server (to accept slave requests)
        self._server = None if self._listen is None else self._create_server(*self._listen)

    # top of the top
    # - groups
    # - config parsing

    class Group(_InitReprMixin):
        __slots__ = "name_regexes", "cpu", "cpu_percent", \
                    "slots",        "mem", "mem_percent", \

        def match(self, slot):
            for reg in self.name_regexes:
                if reg.search(slot.name):
                    return True
            return False


    def _read_config(self, cfg):
        self.log.debug("reading config")

        def parse_resource(txt):
            number = percent = 0
            for item in (txt or "").split():
                mo=re.match(r"\A(\d+(?:\.\d+)?)([TGMK%]?)\Z", item, re.I)
                if not mo:
                    self.log.warning("ignored unknown resource spec %r", item)
                    continue
                val = float(mo.group(1))
                mul = mo.group(2).upper() or " "
                if mul != "%":
                    if number:
                        raise ValueError("multiple absolute values in %r" % txt)
                    number = int(val * _UNITS[mul])
                else:
                    if percent:
                        raise ValueError("multiple percent values in %r" % txt)
                    percent = val / 100

            return number, percent


        self._listen = cfg.get("listen", None, cast=lambda x:
                _parse_sockaddr(str(x), default_host="127.0.0.1"))

        self._groups.clear()
        for i, item in enumerate(cfg.get("reserve", [], list)):
            group = self.Group(name_regexes = [], slots=[])
            for match in item["match"]:
                group.name_regexes.append(re.compile(match["name"]))

            group.cpu, group.cpu_percent = item.get("cpu", (0,0), str, cast=parse_resource)
            group.mem, group.mem_percent = item.get("mem", (0,0), str, cast=parse_resource)
            self._groups.append(group)

            self.log.info("  group %2d  CPU[%s] MEM[%s]  match: %s", i,
                    fmt_requested_resource(group.cpu, group.cpu_percent),
                    fmt_requested_resource(group.mem, group.mem_percent),
                    " ".join(repr(x.pattern) for x in group.name_regexes))
        # append default group
        self._groups.append(self.Group(name_regexes=[re.compile("")], slots=[]))


    # swarm discovery
    # - nodes
    # - group slots

    class Node(_InitReprMixin):
        __slots__ = "id", "name", "healthy", "cpu", "mem"

    class GroupSlot(_InitReprMixin):
        __slots__ = "gid", "node", "cpu", "mem", "cpu_free", "mem_free"


    def _discover_swarm_nodes(self):
        """discover the swarm nodes and save their status
        
        We save the node id, name, cpu, memory and healthy status into
        self._nodes

        If any change happens, then the groups are reallocated to adapt to the
        new swarm layout
        """
        info = self.client.info()

        nodes  = []
        dirty  = False
        node   = None
        unseen = set(self._nodes)
        def flush_node(*,final=False):
            nonlocal node, dirty
            if node is None:
                return
            try:
                tgt = self._nodes[node.id]
            except KeyError:
                # the node is new
                nodes.append(node)
                dirty = True
                self.log.info("new swarm node: %s (%s)", node.id, node.name)
            else:
                # the node already exists
                unseen.remove(node.id)
                nodes.append(tgt)
                for key in node.__slots__:
                    val = getattr(node, key)
                    if getattr(tgt, key) != val:
                        setattr(tgt, key, val)
                        dirty = True

            if final:
                for node_id in unseen:
                    # the node has just disappeared
                    dirty = True
                    self.log.info("swarm node disappeared: %s (%s)", node_id, self._nodes[node_id])

                nodes.sort(key = lambda n: (n.name, n.id))
                self._nodes.clear()
                self._nodes.update((n.id, n) for n in nodes)
            node = None
                        

        for key, val in info["SystemStatus"]:
            if re.match(r" \S", key) and re.fullmatch(
                    r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d+", val):
                flush_node()
                node = self.Node()
                node.name = key[1:]
                node.healthy = True
            elif node is not None:
                mo = re.fullmatch(r"  \S (.*)", key)
                if not mo:
                    self.log.warning("failed to parse swarm info SystemStatus entry: %r", key)
                else:
                    key = mo.group(1)
                    if key == "ID":
                        node.id = val
                    elif key == "Status" and val != "Healthy":
                        node.healthy = False
                    elif key == "Error":
                        # NOTE : it seems that swarm does not mark immediately
                        # as "non healthy" a node that goes down but it
                        # displays an error immediately (FIXME the error seems
                        # to stay there a long time after the node goes up)
                        node.healthy = False
                    elif key == "Reserved CPUs":
                        node.cpu = int(re.search("/\s*(\d+)\Z", val).group(1))
                    elif key == "Reserved Memory":
                        num, unit = re.search("/\s*(\d[0-9.]*)\s*([A-Z]?)i?B\Z", val).groups()
                        node.mem = int(float(num) * _UNITS[unit or " "])
        flush_node(final=True)


        if dirty:
            self.log.info("Swarm layout updated:")
            for node in self._nodes.values():
                self.log.info("  node  %-10s CPU[%s] MEM[%s]  %s", node.name,
                        fmt_value(node.cpu), fmt_value(node.mem),
                        ("healthy" if node.healthy else "not healthy"))

            self._allocate_groups()


    # TODO: give a better name
    def _allocate_groups(self):
        healthy_nodes = [n for n in self._nodes.values() if n.healthy]
        cpu_free = {n: n.cpu for n in healthy_nodes}
        mem_free = {n: n.mem for n in healthy_nodes}
        cpu_total = sum(cpu_free.values())
        mem_total = sum(mem_free.values())

        self.log.info("reallocate swarm resources")
        default_group = self._groups[-1]
        for gid, group in enumerate(self._groups):
            group.slots = []

            if group is default_group:
                # default group (catches everything else)
                assert group.name_regexes[0].pattern == ""
                for node in healthy_nodes:
                    if cpu_free[node] and mem_free[node]:
                        group.slots.append(self.GroupSlot(gid=gid, node=node,
                            cpu     =cpu_free[node], mem     =mem_free[node],
                            cpu_free=cpu_free[node], mem_free=mem_free[node]))
                        cpu_free[node] = mem_free[node] = 0
            else:
                # normal groups
                wanted_cpu = max(group.cpu, int(group.cpu_percent * cpu_total))
                wanted_mem = max(group.mem, int(group.mem_percent * mem_total))

                ratio = wanted_mem / wanted_cpu

                for node in healthy_nodes:
                    # compute a slot on this node
                    #  - try to use the maximum number of cpus
                    #  - but have the same memory/cpu ratio as the whole group
                    # FIXME: should have a minimum allocation unit
                    cpu = min(wanted_cpu, cpu_free[node],
                            int(min(wanted_mem, mem_free[node]) / ratio))
                    if cpu:
                        mem = round(cpu * ratio)
                        cpu_free[node] -= cpu
                        mem_free[node] -= mem
                        wanted_cpu -= cpu
                        wanted_mem -= mem

                        if not cpu_free[node]:
                            # if all cpus are allocated on this node, then use
                            # all the remaining memory (but do not decrease wanted_mem)
                            mem += mem_free[node]
                            mem_free[node] = 0

                        group.slots.append(self.GroupSlot(gid=gid, node=node,
                            cpu=cpu, mem=mem, cpu_free=cpu, mem_free=mem))

        self._dump_groups()

        unallocated = [n for n in healthy_nodes if cpu_free[n] or mem_free[n]]
        if unallocated:
            self.log.warning("unallocated ressources: %s", " ".join(
                "%s(cpu:%s mem:%s)" % (n.name, fmt_value(cpu_free[n]), fmt_value(mem_free[n]))
                for n in unallocated))

    def _dump_groups(self):
        self.log.info("Swarm groups status                            free CPU    free MEM")
        for gid, group in enumerate(self._groups):
            tot = {attr: sum(getattr(s, attr) for s in group.slots)
                    for attr in ("cpu", "mem", "cpu_free", "mem_free")}

            self.log.info("  group %2d  CPU[%s] MEM[%s] -> %s/%s %s/%s  match: %s", gid,
                        fmt_requested_resource(group.cpu, group.cpu_percent),
                        fmt_requested_resource(group.mem, group.mem_percent),
                        fmt_value(tot["cpu_free"]), fmt_value(tot["cpu"]),
                        fmt_value(tot["mem_free"]), fmt_value(tot["mem"]),
                        "".join(repr(x.pattern) for x in group.name_regexes))
            for slot in group.slots:
                self.log.info("    node %-35s %s/%s %s/%s", slot.node.name, 
                        fmt_value(tot["cpu_free"]), fmt_value(tot["cpu"]),
                        fmt_value(tot["mem_free"]), fmt_value(tot["mem"]))


    # process management
    # - slots

    class Slot(_InitReprMixin):
        # fut_XXXXXX are asynci futures
        __slots__ = "id", "name", "cpu", "mem", "fut_allocated", "fut_terminated", "gslot"

    def _allocate(self):
        """Process the _requested dict and try to allocate a GroupSlot for each entry"""
        # FIXME: the implementation is time consuming (O(n)) -> this may cause
        #        delays if the number of waiting jobs is huge

        assert self._lock.locked()

        for slot in self._requested.values():
            if slot.gslot is None:
                self._allocate_single_slot(slot)

    def _allocate_single_slot(self, slot, *, force: Node = None):
        assert self._lock.locked()
        assert slot.gslot is None

        def do_allocate(slot, gslot):
            gslot.cpu_free -= slot.cpu
            gslot.mem_free -= slot.mem
            slot.gslot = gslot
            self._set_futures(slot.fut_allocated, result=slot.gslot)
            self.log.info("slot   allocated: name=%r(cpu=%s, mem=%s) group=%d node=%r (cpu_free=%s, mem_free=%s)",
                    slot.name, slot.cpu, fmt_value(slot.mem), gslot.gid,
                    gslot.node.name, gslot.cpu_free, fmt_value(gslot.mem_free))

        has_candidate = False

        for group in self._groups:
            if group.match(slot):
                for gslot in group.slots:
                    if force is None or force is gslot.node:
                        if slot.cpu <= gslot.cpu_free and slot.mem <= gslot.mem_free:
                            do_allocate(slot, gslot)
                            return
                        elif slot.cpu <= gslot.cpu and slot.mem <= gslot.mem:
                            has_candidate = True

        # allocation failed
        if force is None:
            if not has_candidate:
                # allocation will always fail
                raise SlotTooBig()
        else:
            # forced allocation (post-creation)

            # try to allocate this slot in any GroupSlot of the node
            for group in reversed(self._groups):
                for gslot in group.slots:
                    if gslot.node is force:
                        if slot.cpu <= gslot.cpu_free and slot.mem <= gslot.mem_free:
                            do_allocate(slot, gslot)
                            return

            # fall back to the last group of the node
            # NOTE: this can lead to negative values for cpu_free/mem_free and
            #       make create_container() fail. Perhaps it will be wiser to
            #       just terminate these jobs.
            #    Swarm groups status                            free CPU    free MEM
            #      group  0  CPU[ 1.0  25%] MEM[ 1.0G 25%] ->  1.0 / 1.0   1.0G/ 1.0G  match: '^h''/H'
            #        node n1                                   1.0 / 1.0   1.0G/ 1.0G
            #      group  1  CPU[ 1.0  25%] MEM[ 1.0G 25%] ->  1.0 / 1.0   1.0G/ 1.0G  match: '^[hHn]'
            #        node n1                                   1.0 / 1.0   1.0G/ 1.0G
            #      group  2  CPU[---------] MEM[---------] -> -2.0 / 2.0  -2.0G/ 2.0G  match: ''
            #        node n1                                  -2.0 / 2.0  -2.0G/ 2.0G
            for group in reversed(self._groups):
                for gslot in group.slots:
                    if gslot.node is force:
                        do_allocate(slot, gslot)
                        return

            self.log.warning("forced slot allocated failed for name=%r(cpu=%s, mem=%s, node=%r)",
                    slot.name, slot.cpu, fmt_value(slot.mem), force.name)


    def _deallocate(self, slot):
        """Deallocate the given slot"""

        assert self._lock.locked()

        gslot = slot.gslot
        if gslot is not None:
            gslot.cpu_free += slot.cpu or 0
            gslot.mem_free += slot.mem or 0
            slot.gslot = None

            self.log.info("slot deallocated: name=%r(cpu=%s, mem=%s) group=%d node=%r (cpu_free=%s, mem_free=%s)",
                    slot.name, slot.cpu, fmt_value(slot.mem), gslot.gid,
                    gslot.node.name, gslot.cpu_free, fmt_value(gslot.mem_free))

            # try to launch another job
            self._allocate()


    def _delayed_allocation(self, cid="all"):
        self.log.debug("delayed allocation (%s)", cid)
        if self._unallocated:
            for slot in tuple(self._unallocated):
                # inspect containers that were created by another party
                # and allocate slots for them
                try:
                    ctr = self.client.inspect_container(slot.id)
                except docker.errors.NotFound:
                    self._unallocated.discard(slot)
                    continue

                with self._lock:
                    if slot in self._unallocated:   # because it may already be deleted
                        # find node
                        node_id = ctr["Node"]["ID"]
                        node = self._nodes.get(node_id)
                        if node is None:
                            self.log.warning("ignored container creation %r on unknown node id %r", slot.name, node_id)
                        else:
                            assert node.id == node_id
                            slot.cpu = ctr["HostConfig"]["CpuShares"] or 0
                            slot.mem = ctr["HostConfig"]["Memory"] or 0
                            self._allocate_single_slot(slot, force=node)

                        self._unallocated.remove(slot)


    def _refresh_group_slots(self):
        """recompute cpu_free/mem_free for every GroupSlot"""
        with self._lock:
            # list of all allocated slots (requested or running)
            slots = set(s for s in itertools.chain(self._requested.values(), self._created.values())
                    if s.gslot is not None)

            class Dirty(Exception):
                pass

            try:
                # check the consistency of the group slots (cpu/mem free)

                # compute the used cpu/mem for each group
                cpu={}
                mem={}
                for group in self._groups:
                    for gslot in group.slots:
                        cpu[gslot] = gslot.cpu - gslot.cpu_free
                        mem[gslot] = gslot.mem - gslot.mem_free

                # decrease them with the known processes
                for slot in slots:
                    try:
                        cpu[slot.gslot] -= slot.cpu or 0
                        mem[slot.gslot] -= slot.mem or 0    # pragma: nocover
                    except KeyError:
                        # container refers to an unknown group slot
                        self.log.debug("slot allocated in an unknown GroupSlot: %s", slot)
                        raise Dirty("group slots were redefined")

                # we should have 0 everywhere
                for gslot in cpu:
                    if cpu[gslot] or mem[gslot]:
                       raise Dirty("cpu/mem inconsistency in slot for group %d node %r"
                            % (gslot.gid, gslot.node.name))

                self.log.debug("group slots are consistent")
                return

            except Dirty as e:
                self.log.info("%s", str(e))
            except Exception:   # pragma: nocover
                self.log.exception("unhandled exception when checking group slots consistency")


            # recompute the group slots
            # (this normally happens when the swarm layout is updated
            #  or in case of error)
            self.log.info("recomputing cpu/mem free for all group slots")

            for group in self._groups:
                for gslot in group.slots:
                    gslot.cpu_free = gslot.cpu
                    gslot.mem_free = gslot.mem

            for slot in slots:
                node = slot.gslot.node
                slot.gslot = None
                self._allocate_single_slot(slot, force=node)

    def _refresh_job_list(self):
    
        def get_node_and_name(ctr):
            for name in ctr["Names"]:
                mo = re.match(r"/([^/]+)/([^/]+)\Z", name)
                if mo:
                    return mo.groups()
            raise RuntimeError("docker container id %s has no name" % ctr["Id"])    # pragma: no cover
 
        # detect if containers were created/deleted
        unseen  = set(self._created)
        for ctr in self.client.containers(all=True):
            cid = ctr["Id"]
            node_name, name = get_node_and_name(ctr)

            try:
                slot = unseen.remove(cid)
            except KeyError:
                # this container was just created
                self.container_created(cid, name)
            else:
                # detect terminated containers
                if ctr["State"] not in ("running", "created"):
                    mo = re.match("Exited \((\d+)\)", ctr["Status"])
                    self._container_cleanup(cid, removed=False,
                            returncode=(mo and int(mo.group(1))))


        # flush removed containers
        # FIXME: ignore disappearring containers if their node is unhealthy
        for cid in unseen:
            self._container_cleanup(cid, None, removed=True)

        self._refresh_group_slots()

        # handle unallocated containers
        self._delayed_allocation()


    @contextlib.contextmanager
    def request_slot(self, name, cpu, mem):
        slot = None
        try:
            with self._lock:
                if name in self._requested:
                    raise AlreadyRequested(name)

                # FIXME: should ensure that it is not in self._created too ???

                # create new slot
                self._requested[name] = slot = self.Slot(name=name, cpu=cpu, mem=mem)

                # trigger allocation
                self._allocate()
            yield
        finally:
            if slot is not None:
                with self._lock:
                    assert slot.fut_allocated is None
                    del self._requested[name]
                    if slot.id is None:
                        self.log.info(".request_slot(): container %r was not created", slot.name)
                        # slot was allocated, but container was not created
                        self._deallocate(slot)


    # TODO: should detect if called more than once (so as to trigger a refresh)
    @asyncio.coroutine
    def wait_slot(self, name):
        self.log.debug("enter wait slot")

        with self._lock:
            slot = self._requested[name]

            if slot.gslot is not None:
                # already allocated
                return slot.gslot.node.id

            if slot.fut_allocated is not None:
                raise RuntimeError("slot %r has an active waiter" % name)
            slot.fut_allocated = asyncio.Future()

        try:
            return (yield from slot.fut_allocated).node.id
        finally:
            self.log.debug("leave wait slot")
            slot.fut_allocated = None

    
    def _create_server(self, host, port):
        self.log.info("listening on %s:%s", host, port)
        server = self._loop.run_until_complete(
                asyncio.start_server(self.slave_connection_handler, host, port))

        # send keep-alive messages to guarantee that resources are released
        # immediately when the slave goes down
        for sock in server.sockets:
            _enable_tcp_keepalive(sock)
        return server

    @asyncio.coroutine
    def slave_connection_handler(self, reader, writer):
        try:
            peer = "%s:%s" % writer.transport.get_extra_info("peername")

            request= (yield from reader.readline()).decode()

            mo=re.match("(\S+) (\d+) (\d+)$", request)
            if mo is None:
                self.log.error("slave connection error for %s (malformatted request %r)", peer, request)
                return

            name = mo.group(1)
            cpu  = int(mo.group(2))
            mem  = int(mo.group(3))

            self.log.debug("slot request from slave %s  name=%r cpu=%d mem=%s",
                    peer, name, cpu, fmt_value(mem))

            with self.request_slot(name, cpu, mem):
                node_id = yield from self.wait_slot(name)
                writer.write(("id=%s\n" % node_id).encode())

                yield from reader.read(1)

        except Exception as e:
            self.log.exception("exception in slave handler for %s", peer)
            writer.write(("error=%s\n" % e.__class__.__name__).encode())

        finally:
            self.log.debug("closing connection for slave %s", peer)
            writer.close()


    def shutdown(self):
        super().shutdown()

        # stop the server
        if self._server is not None:
            self._server.close()

        with self._lock:
            futures = set( s.fut_terminated for s in self._created.values())
            futures.update(s.fut_allocated  for s in self._requested.values())
            futures.discard(None)

            self._set_futures(*futures, exception=ShuttingDown())


    def container_created(self, container_id, name):
        self.log.debug("container event: created %s %s", container_id, name)
        assert container_id
    
        with self._lock:
            if container_id in self._created:
                # happens if the "create" is already received
                return

            slot = self._requested.get(name)
            if slot is None:
                # created without calling request_slot()
                slot = self.Slot(id=container_id, name=name)
                self._unallocated.add(slot)
            else:
                slot.id = container_id
            self._created[container_id] = slot

    def _container_cleanup(self, container_id, returncode, *, removed):
        self.log.debug("container event: %s %s", ("removed" if removed else "terminated"), container_id)
        assert container_id

        with self._lock:
            slot = self._created.get(container_id)
            if slot is not None:
                # report container terminated
                self._set_futures(slot.fut_terminated, result=returncode)

                if removed:
                    # container removed

                    self._deallocate(slot)

                    self._unallocated.discard(slot)
                    self._set_futures(slot.fut_allocated, exception=RuntimeError("ContainerRemoved"))
                    del self._created[container_id]


    #
    # Utility functions
    #

    def _set_futures(self, *futures, result=None, exception=None):
        assert self._lock.locked()
        assert result is None or exception is None

        @self._loop.call_soon_threadsafe
        def _():
            for fut in futures:
                if fut is not None and not fut.done():
                    if exception is None:
                        fut.set_result(result)
                    else:
                        fut.set_exception(exception)

    #
    # Refresh thread
    #

    def _next_refresh(self):
        self._sleep(60)     #pragma: nocover

    def _refresh_thread_func(self, ready):
        self.log.debug("docker refresh thread started")
        try:
            while True:
                try:
                    with self._lock_events:
                        self.log.debug("docker refresh")
                        self._discover_swarm_nodes()
                        self._refresh_job_list()
                        with self._lock:
                            self._allocate()

                    ready.set()

                except AnyDockerError as e:
                    self.log.error("docker refresh thread error (%s)", e)
                except Exception:
                    self.log.exception("docker refresh thread exception")

                self._next_refresh()

        except ShuttingDown:
            pass
        finally:
            self.log.debug("docker refresh thread terminated")



    #
    # Events thread
    #

    def _dispatch_event(self, event):
        status = event.get("status")
        if status is None:
            self.log.debug("_dispatch_event(): discarded event: %r", event)
            return
        cid    = event["id"]
        name   = event["Actor"]["Attributes"]["name"]
        code   = _parse_exitcode(event["Actor"]["Attributes"].get("exitCode"))

        with self._lock_events:

            # container created
            if status == "create":
                self.container_created(cid, name)
                self._delayed_allocation(cid)

            # container terminated
            elif status == "die":
                self._container_cleanup(cid, code, removed=False)

            # container removed
            elif status == "destroy":
                self._container_cleanup(cid, code, removed=True)

            #TODO: monitor "update" events too ? (in case cpu/ram changes)







    @asyncio.coroutine
    def wait(self, container_id):
        assert re.match(r"[0-9a-f]{64}\Z|<<ID ", container_id), "bad container id"
        assert asyncio.get_event_loop() == self._loop

        with self._lock:
            slot = self._created[container_id]  #NOTE: raise KeyError

            if slot.fut_terminated is not None:
                raise RuntimeError("there is already a watch for this container")

            if self._shutdown.is_set():
                raise ShuttingDown()

            slot.fut_terminated = asyncio.Future()

        try:
            # FIXME: should track 'start' events too (to avoid inspecting the container here)
            ctr = self.client.inspect_container(container_id)
            if ctr["State"]["Running"]:
                self.log.debug("wait for container:   %s", container_id)
                returncode = yield from slot.fut_terminated
            else:
                returncode = ctr["State"]["ExitCode"]
            self.log.debug("container terminated: %s", container_id)
            return returncode
        except docker.errors.NotFound:
            self.log.debug("container not found: %s", container_id)
            raise KeyError(container_id)
        finally:
            slot.fut_terminated = None


class SharedSwarmClient(swarm_abstraction.APIClient):
    """Wrapper for docker.APIClient aimed at sharing the resources of a swarm between multilple independent clients"""

    def __init__(self, docker_host=None, *k, config={}, alias=None, **kw):
        if docker_host is None:
            docker_host = config.get("docker_host", None, str)
        
        super().__init__(docker_host, *k, **kw)

        def error(msg, *k):
            raise RuntimeError("config error: %s" % (msg % k))

        role    = config.get("role", "master", str)
        if role == "master":
            self.__manager = _MasterResourceManager(self, config=config, alias=alias)

        elif role == "slave":
            self.__manager = _SlaveResourceManager(self, config=config, alias=alias)
        else:
            error("invalid role %r", role)


    # context manager
    def request_slot(self, name, *k, **kw):
        return self.__manager.request_slot(name, *k, **kw)
        
    # coroutine
    def wait_slot(self, name):
        return self.__manager.wait_slot(name)

    def shutdown(self, *, wait=False):
        self.__manager.log.debug("shutdown")
        self.__manager.shutdown()
        if wait:
            self.__manager.wait_shutdown()
            self.__manager.log.debug("shutdown complete")


    # FIXME: shoud accept container name or partial id
    # FIXME: may have a race condition if called just at startup (before refresh() is completed)
    # return the exit code of the container or None if unknown (eg. because
    # container does not exist)
    @asyncio.coroutine
    def wait_async(self, cid): 
        self.__manager.log.debug("wait_async %s", cid)
        try:
            returncode = yield from self.__manager.wait(cid)
            self.__manager.log.debug("wait_async %s -> done (exit %r)", cid, returncode)
            return returncode
        except BaseException as e:
            self.__manager.log.debug("wait_async %s -> %r", cid, e)
            raise


    def create_container(self, *k, name=None, **kw):
        # TODO: ensure that the caller uses the right node and does not overallocate resources
        result = super().create_container(*k, name=name, **kw)
        if name is not None:
            self.__manager.container_created(result["Id"], name)
        return result

# we use "SharedSwarmClient" as class name so that name mangling is useful
APIClient = SharedSwarmClient



