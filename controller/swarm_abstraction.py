import json
import logging

import docker

#TODO patch .containers()/.inspect_container() to prepend node name in the container names

_LOCAL_ID   = '0000:0000:0000:0000:0000:0000:0000:0000:0000:0000:0000:0000'
_LOCAL_NAME = "local"

log = logging.getLogger("swarm_abstraction")

class SwarmAbstractionClient(docker.APIClient):
    """Wrapper for docker.APIClient to make a single docker engine look like a swarm with a single node
 
    The swarm API is an extension of the docker remote API, but with some
    incompatibilities:
    https://docs.docker.com/v1.9/swarm/api/swarm-api/#endpoints-which-behave-differently
    
    The purpose of this class is to normalise the value of the CpuShare
    parameter to (1 cpu share == 1 CPU) so that the user of the class does not
    have to care whether he's talking to a swarm or a daemon.
    """


    def __getattr__(self, name):
        if name == "_SwarmAbstractionClient__cpu_shares_multiple":
            # lazily discover the number of CpuShares per CPU
            #  - 1 if server is a swarm
            #  - 1024/NCPU if server is a docker engine

            info = super().info()
            if info["ServerVersion"].startswith("swarm/"):
                log.info("docker host %r is a swarm", self.base_url)
                value = 1
            else:
                log.info("docker host %r is a docker engine", self.base_url)
                value = 1024 // info["NCPU"]
            setattr(self, name, value)
            return value
        else:
            s = super()
            return getattr(s, "__getattr__", s.__getattribute__)(name)


    def inspect_container(self, *k, **kw):
        result = super().inspect_container(*k, **kw)

        if self.__cpu_shares_multiple != 1:
            # docker engine case

            # add dummy Node info
            result["Node"]={"ID": _LOCAL_ID}

            # normalise CpuShares
            result["HostConfig"]["CpuShares"] //= self.__cpu_shares_multiple
        else:
            # swarm case

            # normalise CpuShares (yeah, swarm does not do it... )
            result["HostConfig"]["CpuShares"] //= (1024 // result["Node"]["Cpus"])

        return result


    def create_container(self, *k, **kw):
        if self.__cpu_shares_multiple != 1:
            # normalise CpuShares
            hc = kw.get("host_config")
            if hc and "CpuShares" in hc:
                hc["CpuShares"] *= self.__cpu_shares_multiple

            # check and remove node constraint (if any)
            env = kw.get("environment")
            if isinstance(env, list):
                for i in range(len(env)-1, -1, -1):
                    item = env[i]
                    if item.startswith("constraint:node=="):
                        if item != "constraint:node==" + _LOCAL_ID:
                            raise ValueError(item)
                        env.pop(i)

        return super().create_container(*k, **kw)


    def update_container(self, *k, cpu_shares=None, **kw):
        # normalise CpuShares
        if cpu_shares is not None:
            cpu_shares *= self.__cpu_shares_multiple
        return super().update_container(*k, cpu_shares=cpu_shares, **kw)

    def containers(self, *k, **kw):
        lst = super().containers(*k, **kw)
        if self.__cpu_shares_multiple != 1:
            # normalise container names
            for ctr in lst:
                ctr["Names"] = ["/%s%s" % (_LOCAL_NAME, x) for x in ctr["Names"]]
        return lst

    def info(self, *k, **kw):
        info = super().info(*k, **kw)

        if self.__cpu_shares_multiple != 1:
            # show local node
            if info.get("SystemStatus") is None:
                info["SystemStatus"] = []
            info["SystemStatus"] += [
                [f' {_LOCAL_NAME}', '127.0.0.1:0'],
                ['  └ ID', _LOCAL_ID],
                ['  └ Status', 'Healthy'],
                ['  └ Reserved CPUs', '0 / %d' % info["NCPU"]],
                ['  └ Reserved Memory', '0 KiB / %d KiB' % (info["MemTotal"] // 1024)]]

        return info

APIClient = SwarmAbstractionClient
