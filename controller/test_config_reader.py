import  contextlib
import  io
import  logging
import  unittest
from    unittest    import mock

import config_reader
from   config_reader import ConfigReader, ConfigError

log = logging.getLogger("test")

class ConfigReaderTestCase(unittest.TestCase):
    def test_config_ok(self):
        with mock.patch.object(log, "warning") as m, \
             ConfigReader("""---
            scalar_read:    42
            scalar_unused:  24
            dict_read:
                dscalar_read:       14
                dscalar_unused:     15
                404:                missing

            list_read:
              - lrscalar_read:      16
                lrscalar_unused:    17
              - unused_val4: 40
                unused_val5: 50

            list_unused:
              - unused_val1:   10
                unused_val2:   20
              - unused_val3:   30
            """, log) as cfg:

            self.assertEqual(cfg["scalar_read"], 42)
            self.assertEqual(cfg["scalar_read"], 42)
            self.assertEqual(cfg["dict_read"]["dscalar_read"], 14)
            self.assertEqual(cfg["list_read"][0]["lrscalar_read"], 16)

            # read a default value
            self.assertIs(cfg["dict_read"].get("unknown"), None)
            self.assertEqual(cfg["dict_read"].get("unknown", "bar"), "bar")

            # check type
            self.assertEqual(cfg.get("scalar_read", int), 42)

            # cast value
            self.assertEqual     (cfg.get("scalar_read", cast=str), "42")
            self.assertEqual     (cfg.get("scalar_read", cast=lambda x: x//2), 21)


            # check paths
            self.assertEqual(cfg.path("scalar_read"), ".scalar_read")
            self.assertEqual(cfg["dict_read"].path(), ".dict_read")
            self.assertEqual(cfg["dict_read"].path("dscalar_read"), ".dict_read.dscalar_read")
            self.assertEqual(cfg["list_read"].path(), ".list_read")
            self.assertEqual(cfg["list_read"].path(0), ".list_read[0]")
            self.assertEqual(cfg["list_read"][0].path(), ".list_read[0]")
            self.assertEqual(cfg["list_read"][0].path("lrscalar_read"), ".list_read[0].lrscalar_read")


        m.assert_has_calls([
            mock.call('unused config key %s', '.scalar_unused'),
            mock.call('unused config key %s', '.dict_read.dscalar_unused'),
            mock.call('unused config key %s', '.dict_read.404'),
            mock.call('unused config key %s', '.list_read[0].lrscalar_unused'),
            mock.call('unused config key %s', '.list_read[1].unused_val4'),
            mock.call('unused config key %s', '.list_read[1].unused_val5'),
            mock.call('unused config key %s', '.list_unused'),
            ])

    def test_enumerators(self):
        with ConfigReader("""---
            list: [4,4,3,1,5,7,9,8]
            dict:
                foo:    bar
                26:     14
                hello:  world
            """, log) as cfg:

            self.assertSequenceEqual(tuple(cfg["list"]), (4,4,3,1,5,7,9,8))
            self.assertSequenceEqual(tuple(cfg["dict"]), ("foo", 26, "hello"))
            self.assertSequenceEqual(tuple(cfg["dict"].keys()), ("foo", 26, "hello"))
            self.assertSequenceEqual(tuple(cfg["dict"].values()), ("bar", 14, "world"))
            self.assertSequenceEqual(tuple(cfg["dict"].items()), (("foo", "bar"), (26, 14), ("hello", "world")))

    def test_implicit_mapping(self):
        with self.check_error("\.mapping\.submapping\.missing: config key is missing"):
            with ConfigReader("""---
                scalar: 42
                """, log) as cfg:

                mapping = cfg.get("mapping", {})
                submapping = mapping.get("submapping", {"foo": "bar"})

                self.assertIsInstance(mapping, config_reader.Mapping)
                self.assertIsInstance(submapping, config_reader.Mapping)
                self.assertEqual(submapping["foo"], "bar")
                
                submapping["missing"]

    def test_config_file(self):

        fp = io.StringIO("---\n[4, 2]")
        fp.name="dummmy_file.yml"

        with mock.patch.object(log, "info") as m:
            with ConfigReader(fp, log) as cfg:
                self.assertTrue(fp.closed)
                self.assertSequenceEqual(cfg, (4, 2))
            m.assert_called_with('reading config file %s', 'dummmy_file.yml')


    @contextlib.contextmanager
    def check_error(self, regex, method="error"):
        assert method in ("error", "exception")
        with    mock.patch.object(log, "error"    ) as m_err,   \
                mock.patch.object(log, "exception") as m_exc:
            m = m_err if (method == "error") else m_exc
                
            with self.assertRaisesRegex(ConfigError, regex):
                yield

            self.assertEqual(m.call_count, 1)
            call = m.call_args[0]
            msg = call[0] % call[1:]
            self.assertRegex(msg, regex)


    def test_parse_error(self):
        with self.check_error("yaml error: while scanning "):
            with ConfigReader(""""foo""", log) as cfg:
                raise ValueError()

    def test_key_error(self):
        with self.check_error(r"\.a\.b\.missing: config key is missing"):
            with ConfigReader("""---
            a: { b: {c: 42}}
            """, log) as cfg:
                cfg["a"]["b"]["missing"]

        with self.check_error(r"exception in config reader", method="exception"):
            with ConfigReader("""---
            a: { b: [ 42 ] }
            """, log) as cfg:
                cfg["a"]["b"][2]

    def test_type_error(self):
        with self.check_error(r"\.bad\.value: type error: value 42 is not a str"):
            with ConfigReader("""---
            bad: { value: 42 }
            """, log) as cfg:
                cfg["bad"].get("value", type=str)

        # type check happens before cast
        with self.check_error(r"\.bad\.value: type error: value 42 is not a str"):
            with ConfigReader("""---
            bad: { value: 42 }
            """, log) as cfg:
                cfg["bad"].get("value", type=str, cast=lambda x:x/0)


    def test_cast_error(self):
        # ValueError
        with self.check_error(r"\.bad\.value: invalid literal for int\(\) with base 10: 'foo'"):
            with ConfigReader("""---
            bad: { value: "foo" }
            """, log) as cfg:
                cfg["bad"].get("value", cast=int)

        # TypeError
        with self.check_error(r"\.bad\.value: unsupported operand type\(s\) for /: 'str' and 'int'"):
            with ConfigReader("""---
            bad: { value: "foo" }
            """, log) as cfg:
                cfg["bad"].get("value", cast=lambda x: x/2)
