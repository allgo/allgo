#!/usr/bin/python3


import asyncio
import concurrent.futures
from   contextlib import contextmanager, suppress
import io
import itertools
import json
import logging
import os
import queue
import random
import re
import shlex
import shutil
import socket
import threading
import time
import unittest
from   unittest import mock

import controller
from   controller import docker_check_error
import docker
from   docker.errors import NotFound
from   database import *

# WARNING: all images whose tag starst with REGISTRY are removed at tear down
REGISTRY    = "localhost:8002/allgo/dev/test"
ENV         = "test"
MYSQL_HOST  = "dev-mysql"
DOCKER_HOST = "unix:///vol/test-daemon/docker.sock"
#DOCKER_HOST = "unix:///run/docker.sock"
PORT        = 6789
DATASTORE_HOST_PATH = "/data/dev/controller/tmp/test/datastore/"
DATASTORE_PATH      =                 "/vol/tmp/test/datastore/"

#FIXME this will fail if the ssh container was not run previously
SANDBOX_HOST_PATH   = "/data/dev/ssh/cache/sandbox"
SANDBOX_NETWORK     = "allgo_sandboxes"

#FIXME this will fail if the toolbox container was not run previously
TOOLBOX_HOST_PATH   = "/data/dev/toolbox/cache"

S = SandboxState
V = VersionState
J = JobState

log = logging.getLogger("test_controller")


BAD_SANDBOX_NAMES = "", ".", "..", "foo/bar", "../blah", "/root", "foo/"

def update_default(dct, **kw):
    for k, v in kw.items():
        dct.setdefault(k, v)

def wait_until(func, *, period=.05, timeout=1, msg=None):
    limit = time.time() + timeout
    while time.time() < limit:
        if func():
            return
        time.sleep(period)

    txt = "timeout expired"
    if msg:
        txt += " (%s)" % msg
    raise AssertionError(txt)

@contextmanager
def preamble():
    try:
        yield
    except Exception as e:
        log.exception("test preamble failed")
        raise

@contextmanager
def part(descr):
    try:
        yield
    except Exception as e:
        log.exception("test part %r failed", descr)
        raise

@contextmanager
def lock_loop(loop, *, timeout=1):
    cond = threading.Condition()
    def lock():
        with cond:
            cond.notify()
            log.debug("loop locked")
            cond.wait()
            log.debug("loop unlocked")

    with cond:
        loop.call_soon_threadsafe(lock)
        if not cond.wait(timeout):
            raise Exception("timeout")
    try:
        yield
    finally:
        with cond:
            cond.notify()


def as_coroutine(func):
    """decorator for test thats are implemented as a coroutine"""
    func = asyncio.coroutine(func)
    def wrapper(*k, **kw):
        return asyncio.get_event_loop().run_until_complete(func(*k, **kw))
    wrapper.__name__ = func.__name__
    return wrapper

class DockerEventsWatcher:
    def __init__(self, client):
        self.stream = client.events()

        self._lock = threading.Lock()
        self._queues = {}

        self._thread = threading.Thread(target=self._listener)
        self._thread.daemon = True
        self._thread.start()

    def _listener(self):
        try:
            for event in self.stream:
                js = json.loads(event.decode())
                log.debug("event %r", js)
                with self._lock:
                    for q in self._queues.values():
                        q.append(js)
        finally:
            self._queues = None

    @contextmanager
    def __call__(self, container):
        queue = []
        try:
            with self._lock:
                if self._queues is None:
                    raise Exception("listener is dead")
                self._queues[id(queue)] = queue
            yield queue
        finally:
            with self._lock:
                del self._queues[id(queue)]


class DockerError(docker.errors.APIError):
    def __init__(self, *k):
        Exception.__init__(self, *k)
    def __str__(self):
        return Exception.__str__(self)

class _UNSET:
    pass

class ControllerTestCase(unittest.TestCase):

    @classmethod
    def create_controller(cls):
        return controller.DockerController(
                sandbox_host = DOCKER_HOST,
                swarm_host   = DOCKER_HOST,
                mysql_host   = MYSQL_HOST,
                port         = PORT,
                registry     = REGISTRY,
                env          = ENV,
                datastore_path = DATASTORE_HOST_PATH,
                sandbox_path   = SANDBOX_HOST_PATH,
                sandbox_network= SANDBOX_NETWORK,
                toolbox_path   = TOOLBOX_HOST_PATH,
                )

    @classmethod
    def setUpClass(cls):

        cls.dk = docker.APIClient(DOCKER_HOST)
        cls.dk_events_watcher = DockerEventsWatcher(cls.dk)
        cls.session = connect_db(MYSQL_HOST)()

        # create busybox:latest factory
        deb_version  = "latest"
        deb_official = "busybox:"+ deb_version
        deb_factory  = REGISTRY + "/factory/test-busybox"
        
        try:
            cls.dk.inspect_image(deb_official)
        except NotFound:
            cls.dk.pull(deb_official)

        cls.dk.tag(deb_official, deb_factory, deb_version)

        # not necessary, but useful to ensure that the registry is up
        # (and abort immediately)
        cls.dk.push("%s:%s" % (deb_factory, deb_version))


    @classmethod
    def reset(cls, *, full=False):

        # remove all user containers
        for ctr in cls.dk.containers(all=True, filters={"name": "^/%s-" % ENV}):
            cls.dk.remove_container(ctr["Id"], force=True)

        # remove all images
        # TODO: clean the registry too
        assert not REGISTRY.endswith("/")
        pfx = REGISTRY + "/"
        pfx_factory = pfx + "factory/"
        for img in cls.dk.images():
            for repotag in img["RepoTags"] or ():
                if repotag.startswith(pfx):
                    if full or not repotag.startswith(pfx_factory):
                        cls.dk.remove_image(repotag)

        with cls.session.begin():
            # clean the db
            for app in cls.session.query(Webapp):
                dn = app.docker_name or ""
                if dn.startswith("test-") or dn in BAD_SANDBOX_NAMES:
                    cls.session.query(Job).filter_by(webapp_id=app.id).delete()
                    cls.session.query(WebappVersion).filter_by(webapp_id=app.id).delete()
                    cls.session.delete(app)

            for img in cls.session.query(DockerOs):
                if (img.docker_name or "").startswith("test-"):
                    cls.session.delete(img)

            for queue in cls.session.query(JobQueue):
                if queue.name.startswith("test_"):
                    cls.session.delete(queue)

            if not full:
                # seed the db
                deb = DockerOs(docker_name="test-busybox", version="latest")
                app = Webapp(
                    docker_name   = "test-app",
                    sandbox_state = SandboxState.IDLE,
                    docker_os     = deb,
                    entrypoint    = "/bin/sh",
                    )
                cls.session.add(app)

        # clear the tmp files
        with suppress(FileNotFoundError):
            shutil.rmtree("/vol/tmp/test")
        if not full:
            mapped_path = "/vol/host" + DATASTORE_HOST_PATH
            assert not os.path.exists(mapped_path)
            os.makedirs(DATASTORE_PATH)
            assert os.path.exists(mapped_path), "'/vol/host' must be mapped from the host '/'"


    @classmethod
    def tearDownClass(cls):
        cls.reset(full=True)

        cls.dk.close()
        del cls.dk
        del cls.session


    @classmethod
    def dk_exec(cls, *k, **kw):
        eid  = cls.dk.exec_create(*k, **kw)["Id"]
        out  = cls.dk.exec_start(eid)
        code = cls.dk.exec_inspect(eid)["ExitCode"]
        return code, out

    def setUp(self):
        self.reset()

        # run the controller in a separate thread
        def run():
            self._loop = asyncio.new_event_loop()
            self._loop.add_signal_handler = lambda *k: None
            asyncio.set_event_loop(self._loop)
            self.ctrl = self.create_controller()
            self.ctrl.run()


        # create the thread and wait for the controller to be fully initialised
        # (i.e. until the first call to .check_db())
        with mock.patch("controller.DockerController.check_db") as m:
            self._thread = threading.Thread(target=run)
            self._thread.start()

            while not m.called and self._thread.is_alive():
                time.sleep(0.01)

    def lock_loop(self, **kw):
        return lock_loop(self._loop, **kw)

    def tearDown(self):
        self._loop.call_soon_threadsafe(self.ctrl.shutdown)
        self._thread.join()
        self._loop.close()


    def find(self, cls, docker_name=None, **kw):
        if docker_name is not None:
            kw["docker_name"] = docker_name
        return self.session.query(cls).filter_by(**kw).one()

    def with_db(func):
        def wrapper(self, *k):
            with self.session.begin():
                app = self.find(Webapp, "test-app")
            return func(self, self.session, app, *k)
        wrapper.__name__ = func.__name__
        return wrapper

    def notify(self, *, startup=False):
        self._loop.call_soon_threadsafe(lambda: self.ctrl.check_db(startup=startup))


    @contextmanager
    def check_sandbox_transition(self, app, prev_state, next_state, *, timeout=10):
        def get_state(app):
            self.session.refresh(app)
            return S(app.sandbox_state)

        self.assertEqual(get_state(app), prev_state)
        yield

        wait_until(lambda: get_state(app) != prev_state,
                timeout=timeout, msg = "expected state change to %r" % next_state)

        self.assertEqual(get_state(app), next_state)


    @contextmanager
    def check_version_transition(self, ver, prev_state, next_state, *, ignore_state=_UNSET, timeout=10):
        def get_state(ver):
            self.session.refresh(ver)
            return V(ver.state)

        self.assertEqual(get_state(ver), prev_state)
        yield

        wait_until(lambda: get_state(ver) not in (prev_state, ignore_state),
                timeout=timeout, msg = "expected state change to %r)" % next_state)

        self.assertEqual(get_state(ver), next_state)

    @contextmanager
    def check_job_transition(self, job, prev_state, next_state, *, ignore_state=_UNSET, timeout=10):
        def get_state(job):
            self.session.refresh(job)
            return J(job.state)

        self.assertEqual(get_state(job), prev_state)
        yield

        wait_until(lambda: get_state(job) not in (prev_state, ignore_state),
                timeout=timeout, msg = "expected state change to %r)" % next_state)

        self.assertEqual(get_state(job), next_state)



    def check_sandbox_running(self, dct):
        for app, image in dct.items():
            ctr = "%s-sandbox-%s" % (ENV, app.docker_name)
            try:
                status = self.dk.inspect_container(ctr)
            except docker.errors.NotFound:
                status = None

            if not image:
                self.assertFalse(bool(status), msg="container %r must not be running"  % ctr)
            elif image is True:
                self.assertTrue(bool(status), msg="container %r must be running" % ctr)
            else:
                iid = self.dk.inspect_image("%s/%s" % (REGISTRY, image))
                self.assertEqual(status["Image"], iid["Id"], msg="container %r: bad image" % ctr)

    @contextmanager
    def check_sandbox_events(self, app, txt):
        expected = txt.split()
        container = self.ctrl.gen_sandbox_name(app)

        with self.dk_events_watcher(container) as queue:
            yield

        seen = [e["Action"] for e in queue if e["Actor"]["Attributes"].get("name") == container]
        self.assertListEqual(seen, expected, msg="unexpected events sequence")

    @contextmanager
    def check_log(self, level, regex):
        levelno = getattr(logging, level.upper())
        found = False
        class Hnd(logging.Handler):
            def emit(self, record):
                nonlocal found
                txt = record.msg % record.args
                #print((txt, record.levelno, regex, levelno, re.search(regex, txt)))
                if record.levelno == levelno and re.search(regex, txt):
                    found = True
        hnd = Hnd()
        try:
            controller.log.addHandler(hnd)
            yield
            if not found:
                self.fail("log not matched: level=%r msg=%r" % (level, regex))

        finally:
            controller.log.removeHandler(hnd)

    def add_dummy_version(self, app, number, *, append="", **kw):
        update_default(kw,
                state     = V.READY,
                description = "dummy version",
                published = False)

        ses = self.session
        with ses.begin():
            ver = WebappVersion(number=number, webapp=app, **kw)
            ses.add(ver)

            image = self.ctrl.gen_image_name(app)

            if ver.state in (V.COMMITTED, V.READY):
                docker_check_error(self.dk.build, tag="%s:%s" % (image, number), rm=True,
                    fileobj=io.BytesIO(("""
                        FROM busybox:latest
                        LABEL version=%r
                        %s
                    """ % (("%s:%s" % (app.docker_name, number)), append)).encode()))

            if ver.state == V.READY:
                docker_check_error(self.dk.push, image, number)

        return ver


    def start_sandbox(self, app, version=None):

        with self.session.begin():
            app.sandbox_state   = S.STARTING
            if isinstance(version, str):
                version = self.session.query(WebappVersion).filter_by(
                    webapp_id=app.id, number=version, state=int(V.READY)).one()
            app.sandbox_version = version

        with self.check_sandbox_transition(app, S.STARTING, S.RUNNING):
            self.notify()

        self.check_sandbox_running({app: True})

    def stop_sandbox(self, app):

        with self.session.begin():
            app.sandbox_state = S.STOPPING

        with self.check_sandbox_transition(app, S.STOPPING, S.IDLE):
            self.notify()

        self.check_sandbox_running({app: False})


    def kill_sandbox(self, app):
        with suppress(docker.errors.NotFound):
            self.dk.remove_container(self.ctrl.gen_sandbox_name(app), force=True)
        with self.session.begin():
            app.sandbox_state = S.IDLE

    def commit_sandbox(self, app, number, *, wait=True, stop=False, published=False, description="commit-sandbox", error=None, recovery=True, **kw):
        ses = self.session

        with ses.begin():
            ver = WebappVersion(number=number, published=published, state=V.SANDBOX, description=description, **kw)
            app.versions.append(ver)
            if stop:
                app.sandbox_state = S.STOPPING

        if wait:
            if error:
                with self.check_version_transition(ver, V.SANDBOX,
                        V.ERROR,
                        timeout=10):
                    self.notify()

                self.assertEqual(ver.description, "%s [%s]" % (description, error))

                # find the recovery version
                with ses.begin():
                    q = ses.query(WebappVersion).filter(WebappVersion.number.startswith("recovery-"))
                    if not recovery:
                        self.assertEqual(q.count(), 0)
                        return ver
                    
                    rec = q.one()

                # FIXME: possible race condition
                with self.check_version_transition(rec, V.SANDBOX,
                        V.READY, ignore_state=V.COMMITTED,
                        timeout=10):
                    pass

            else:
                with self.check_version_transition(ver, V.SANDBOX,
                        V.READY, ignore_state=V.COMMITTED,
                        timeout=10):
                    self.notify()

                self.assertEqual(ver.description, description)

        return ver

    def check_recovery_version(self, orig, ver, *, error):
        self.assertRegex(ver.number, r"\Arecovery-\d{8}-\d{6}\Z")
        self.assertNotEqual(ver.number,    orig.number)

        self.assertEqual(ver.description, error)

        self.assertFalse(ver.published)
        self.assertIn(ver.state, (V.COMMITTED, V.READY))

    @staticmethod
    def job_dir(job):
        return os.path.join(DATASTORE_PATH, str(job.user_id),
                str(job.webapp_id), job.access_token)

    def create_job(self, app, version, command="", *, state=J.WAITING,
            user_id=0, access_token="0123456789", files={}, queue_id=1):
        ses = self.session
        with ses.begin():
            job = Job(webapp=app, user_id=user_id, state=int(state),
                    param="-c " + shlex.quote(command), version=version, exec_time=None,
                    access_token=access_token, queue_id=queue_id,
                    result=int(JobResult.NONE))
            ses.add(job)

        path = self.job_dir(job)
        os.makedirs(path)
        for name, content in files.items():
            mode = "w" if isinstance(content, str) else "wb"
            with open(os.path.join(path, name), mode) as fp:
                fp.write(content)
        return job


    def check_job_output(self, job, expect, *, filename="allgo.log"):

        path = os.path.join(self.job_dir(job), filename)

        if expect is None:
            self.assertFalse(os.path.exists(path))
            return

        mode = "r" if isinstance(expect, str) else "rb"
        with open(path, mode) as fp:
            output = fp.read()

        self.assertEqual(output, expect)

    def wait_created(self, container, timeout=5):
        limit = time.time() + timeout
        while time.time() < limit:
            try:
                self.dk.inspect_container(container)
                return
            except docker.errors.NotFound:
                pass
            time.sleep(.2)
        raise Exception("timeout")


    # ---------------------- tests -------------------------


    def test_docker_check_error(self):
        self.assertRaisesRegex(controller.Error, "image does not exist locally",
                controller.docker_check_error,
                self.dk.push, REGISTRY + "test-unknown-image")

    @with_db
    def test_common_functions(self, ses, app):
        ctrl = self.ctrl

        self.assertEqual(ctrl.gen_sandbox_name(app), ENV + "-sandbox-test-app")
        self.assertEqual(ctrl.gen_image_name(app), REGISTRY + "/webapp/test-app")

        # session is a thread-local session object
        sid = id(ctrl.session)
        for i in range(10):
            self.assertEqual(id(ctrl.session), sid)

        def func():
            sid2 = id(ctrl.session)
            self.assertNotEqual(sid2, sid)
            for i in range(10):
                self.assertEqual(id(ctrl.session), sid2)

        th = threading.Thread(target = func)
        th.start()
        th.join()


    def test_check_host_path(self):

        good = lambda *k: self.assertIsNone(self.ctrl.check_host_path(*k))
        bad  = lambda reg, *k: self.assertRaisesRegex(controller.Error, reg, self.ctrl.check_host_path, *k)

        good(                           "isfile",   "/bin/ls")

        bad("path .* is not canonical", "isfile",   "/bin//ls")
        bad("path .* is not canonical", "isfile",   "/etc/../bin/ls")
        bad("path .* is not absolute",  "isfile",   "bin/ls")
        bad("path .* not found",        "isdir",    "/bin/ls")

        good(                                       "isdir", "/proc/1/fd")
        bad("path .* contains a symbolic link",     "isdir", "/proc/self/fd")

    def test_sock_callback(self):
        def connect(nb):
            for s in range(nb):
                s = socket.socket()
                s.connect(("127.0.0.1", PORT))
                s.close()
                time.sleep(.01)

        with mock.patch.object(self.ctrl, "check_db") as m:
            with self.lock_loop():
                connect(10)
            time.sleep(.1)
            m.assert_called_once_with()

            m.reset_mock()
            with self.lock_loop():
                connect(10)
            time.sleep(.1)

            m.reset_mock()
            connect(10)
            self.assertGreater(m.call_count, 1)

    @with_db
    def test_sandbox_start_dockeros(self, ses, app):
        with ses.begin():
            app.sandbox_state = S.STARTING

        with self.check_sandbox_transition(app, S.STARTING, S.RUNNING):
            self.notify()

        self.check_sandbox_running({app: "factory/test-busybox:latest"})
  

    @with_db
    def test_sandbox_start_latest(self, ses, app):
        
        # one version
        with preamble():
            ver = self.add_dummy_version(app, "1.0")
            self.add_dummy_version(app, "1.0", state=V.ERROR)
            self.add_dummy_version(app, "1.1", state=V.ERROR)

        self.start_sandbox(app, ver)
        self.check_sandbox_running({app: "webapp/test-app:1.0"})
        self.kill_sandbox(app)

        # multiple versions
        with preamble():
            v1  = self.add_dummy_version(app, "1.1")
            v2  = self.add_dummy_version(app, "0.9")

        self.start_sandbox(app, v1)
        self.check_sandbox_running({app: "webapp/test-app:1.1"})
        self.kill_sandbox(app)

        self.start_sandbox(app, v2)
        self.check_sandbox_running({app: "webapp/test-app:0.9"})
        self.kill_sandbox(app)
        

        # freshly committed version
        with preamble():
            ver = self.add_dummy_version(app, "0.8", state=V.COMMITTED)

        with mock.patch.object(self.ctrl.sandbox, "pull", mock.Mock()) as m:
            self.start_sandbox(app, ver)
            self.check_sandbox_running({app: "webapp/test-app:0.8"})
            self.assertFalse(m.called)


    @with_db
    def test_sandbox_start_bad_parameters(self, ses, app):
        # bad docker_name
        with part("case 1: bad docker_name"):
            for docker_name in BAD_SANDBOX_NAMES:
                with part(docker_name):
                    with ses.begin():
                        w = Webapp(docker_name=docker_name, sandbox_state = S.STARTING, docker_os=app.docker_os)
                        ses.add(w)

                    with    self.check_log("error", "start error .* malformatted docker_name"), \
                            self.check_sandbox_transition(w, S.STARTING, S.START_ERROR):
                        self.notify()

                    self.check_sandbox_running({w: None})

        # bad id
        with part("case 2: bad webapp id"):
            with ses.begin():
                w = Webapp(id=-1, docker_name="test-bad-id", sandbox_state = S.STARTING, docker_os=app.docker_os)
                ses.add(w)
            with    self.check_log("error", "start error .* bad webapp id"), \
                    self.check_sandbox_transition(w, S.STARTING, S.START_ERROR):
                self.notify()

            self.check_sandbox_running({w: None})

        # bad version
        with part("case 3: bad version state"):
            with preamble():
                ver = self.add_dummy_version(app, "0.8", state=V.ERROR)
                with ses.begin():
                    app.sandbox_state   = S.STARTING
                    app.sandbox_version = ver

            with    self.check_log("error", "start error .* bad version state"), \
                    self.check_sandbox_transition(app, S.STARTING, S.START_ERROR):
                self.notify()

        # bad version app id
        with part("case 4: bad version webapp id"):
            with preamble():
                with ses.begin():
                    other = Webapp(docker_name=ENV+"-other-app", docker_os=app.docker_os, sandbox_state=S.IDLE)
                ver = self.add_dummy_version(other, "0.9")
                with ses.begin():
                    app.sandbox_state   = S.STARTING
                    app.sandbox_version = ver

            with    self.check_log("error", "start error .* invalid version id %d .*belongs to webapp %d"
                    % (ver.id, ver.webapp_id)), \
                    self.check_sandbox_transition(app, S.STARTING, S.START_ERROR):
                self.notify()

    @with_db
    def test_sandbox_start_prepare_fail(self, ses, app):
        with ses.begin():
            app.sandbox_state = S.STARTING

        with    mock.patch("docker.APIClient.wait", return_value=42), \
                self.check_log("error", "sandbox preparation failed"), \
                self.check_sandbox_transition(app, S.STARTING, S.START_ERROR):
            self.notify()

        self.check_sandbox_running({app: None})

    @with_db
    def test_sandbox_start_docker_fail(self, ses, app):
        # create error
        with ses.begin():
            app.sandbox_state = S.STARTING
        
        with    mock.patch("docker.APIClient.create_container", side_effect = DockerError), \
                self.check_log("error", "DockerError"), \
                self.check_sandbox_transition(app, S.STARTING, S.START_ERROR):
            self.notify()

        self.check_sandbox_running({app: None})

        # start error
        with ses.begin():
            app.sandbox_state = S.STARTING
        
        with    mock.patch("docker.APIClient.start", side_effect = DockerError), \
                self.check_log("error", "DockerError"), \
                self.check_sandbox_transition(app, S.STARTING, S.START_ERROR):
            self.notify()

        self.check_sandbox_running({app: None})

        # cleanup error
        with ses.begin():
            app.sandbox_state = S.STARTING
        
        with    mock.patch("docker.APIClient.start", side_effect = DockerError("start-error")), \
                mock.patch("docker.APIClient.remove_container", side_effect = DockerError("rm-error")), \
                self.check_log("error", "start-error"), \
                self.check_log("warning", "cleanup error: unable to remove container .*rm-error"), \
                self.check_sandbox_transition(app, S.STARTING, S.START_ERROR):
            self.notify()


    @with_db
    def test_sandbox_commit(self, ses, app):

        with preamble():
            self.start_sandbox(app)
            self.check_sandbox_running({app: "factory/test-busybox:latest"})
            self.assertFalse(app.versions)

        with self.check_sandbox_events(app, """
                pause commit unpause
                """):
            self.commit_sandbox(app, "1.0", description="pouet pouet")

        self.check_sandbox_running({app: "factory/test-busybox:latest"})

        with ses.begin():
            ses.refresh(app)

            self.assertEqual(len(app.versions), 1)
            ver = app.versions[0]
            self.assertEqual(ver.number, "1.0")
            self.assertEqual(ver.description, "pouet pouet")
            self.assertFalse(ver.published)
            self.assertIn(ver.state, (V.COMMITTED, V.READY))

    @with_db
    def test_sandbox_commit_stop(self, ses, app):

        with preamble():
            self.start_sandbox(app)
            self.check_sandbox_running({app: "factory/test-busybox:latest"})
            self.assertFalse(app.versions)
            with ses.begin():
                app.sandbox_state = S.STOPPING

        with self.check_sandbox_events(app, """
                kill die stop commit destroy
                """):
            self.commit_sandbox(app, "1.1", description="blah", published=True)

        self.check_sandbox_running({app: False})

        with ses.begin():
            ses.refresh(app)

            self.assertEqual(len(app.versions), 1)
            ver = app.versions[0]
            self.assertEqual(ver.number, "1.1")
            self.assertEqual(ver.description, "blah")
            self.assertTrue(ver.published)
            self.assertIn(ver.state, (V.COMMITTED, V.READY))

    @with_db
    def test_sandbox_commit_start(self, ses, app):

        with preamble():
            self.start_sandbox(app)
            self.check_sandbox_running({app: "factory/test-busybox:latest"})
            self.assertFalse(app.versions)
            with ses.begin():
                app.sandbox_state = S.STARTING

        with self.check_sandbox_events(app, 
            "kill die stop commit destroy "             # previous version committed
            "create start die destroy create start"):   # new sandbox started) 

            self.commit_sandbox(app, "1.1", description="blah", published=True)
            time.sleep(1)

        self.check_sandbox_running({app: True})

        with ses.begin():
            ses.refresh(app)

            self.assertEqual(len(app.versions), 1)
            ver = app.versions[0]
            self.assertEqual(ver.number, "1.1")
            self.assertEqual(ver.description, "blah")
            self.assertTrue(ver.published)
            self.assertIn(ver.state, (V.COMMITTED, V.READY))


    @with_db
    def test_sandbox_commit_dangling(self, ses, app):

        with preamble():
            self.start_sandbox(app)
            with ses.begin():
                ses.refresh(app)
                app.sandbox_state = S.IDLE
                self.assertFalse(app.versions)

        with self.check_sandbox_events(app, """
                kill die stop commit destroy
                create start die destroy create start
                """):
            self.start_sandbox(app)
            self.check_sandbox_running({app: "factory/test-busybox:latest"})

        with ses.begin():
            ses.refresh(app)
            self.assertEqual(len(app.versions), 1)

            ver = app.versions[0]
            self.assertRegex(ver.number, r"\Arecovery-\d{8}-\d{6}\Z")
            self.assertEqual(ver.description, "pre-commit error: dangling sandbox")
            self.assertFalse(ver.published)
            self.assertIn(ver.state, (V.COMMITTED, V.READY))



    @with_db
    def test_sandbox_commit_error_empty(self, ses, app):

        with preamble():
            self.start_sandbox(app)
            self.assertFalse(app.versions)

        error = "pre-commit error: empty version number"

        with self.check_sandbox_events(app, "pause commit unpause"):
            self.commit_sandbox(app, "", error=error)

        with ses.begin():
            self.assertEqual(len(app.versions), 2)
            self.check_recovery_version(*app.versions, error=error)
    
    @with_db
    def test_sandbox_commit_error_multiple_candidates(self, ses, app):

        with preamble():
            self.start_sandbox(app)
            self.assertFalse(app.versions)
            

        with ses.begin():
            # add bogus versions
            ses.add(WebappVersion(webapp=app, number="1.1", description="foo", published=True,  state=V.SANDBOX))
            ses.add(WebappVersion(webapp=app, number="1.2", description="bar", published=False, state=V.SANDBOX))
            ses.add(WebappVersion(webapp=app, number="1.2", description="baz", published=True,  state=V.SANDBOX))

        error = "pre-commit error: multiple candidate versions ('1.0', '1.1', '1.2', '1.2')"

        with self.check_sandbox_events(app, "pause commit unpause"):
            self.commit_sandbox(app, "1.0", error=error)

        with ses.begin():
            self.assertEqual(len(app.versions), 5)
            rec = app.versions[-1]
            for orig in app.versions[:-1]:
                self.check_recovery_version(orig, rec, error=error)

    @with_db
    def test_sandbox_commit_error_sandbox_down(self, ses, app):

        with preamble():
            self.start_sandbox(app)
            self.assertFalse(app.versions)
         
        self.dk.remove_container(self.ctrl.gen_sandbox_name(app), force=True)

        error = "commit error: sandbox is down"

        self.commit_sandbox(app, "1.0", error=error, recovery=False)

        with ses.begin():
            # no recovery version
            self.assertEqual(len(app.versions), 1)

    @with_db
    def test_sandbox_commit_error_unexpected(self, ses, app):

        with preamble():
            self.start_sandbox(app)
            self.assertFalse(app.versions)


        with    self.check_sandbox_events(app, ""), \
                mock.patch("docker.APIClient.commit", side_effect=DockerError):
            self.commit_sandbox(app, "1.0", description="unchanged", wait=False)
            self.notify()
            time.sleep(1)

        self.check_sandbox_running({app: True})

        with ses.begin():
            self.assertEqual(app.sandbox_state, S.RUNNING)

            # no recovery version
            self.assertEqual(len(app.versions), 1)
            ver = app.versions[0]

            # sandbox kept in same state
            self.assertEqual(ver.number,    "1.0")
            self.assertEqual(ver.description, "unchanged")
            self.assertEqual(ver.state,     V.SANDBOX)

        # try again
        with    self.check_sandbox_events(app, "pause commit unpause"), \
                self.check_version_transition(ver, V.SANDBOX, V.READY, ignore_state=V.COMMITTED):
            self.notify()


    @with_db
    def test_sandbox_commit_error_unexpected_stop(self, ses, app):

        with preamble():
            self.start_sandbox(app)
            self.assertFalse(app.versions)

        with    self.check_sandbox_events(app, "kill die stop"), \
                mock.patch("docker.APIClient.commit", side_effect=DockerError):
            self.commit_sandbox(app, "1.0", description="unchanged", wait=False, stop=True)
            self.notify()
            time.sleep(1)

        self.check_sandbox_running({app: True})

        with ses.begin():
            self.assertEqual(app.sandbox_state, S.STOP_ERROR)

            # no recovery version
            self.assertEqual(len(app.versions), 1)
            ver = app.versions[0]

            # sandbox kept in same state
            self.assertEqual(ver.number,    "1.0")
            self.assertEqual(ver.description, "unchanged")
            self.assertEqual(ver.state,     V.SANDBOX)

        # try again
            app.sandbox_state = S.STOPPING

        with    self.check_sandbox_events(app, "commit destroy"), \
                self.check_version_transition(ver, V.SANDBOX, V.READY, ignore_state=V.COMMITTED):
            self.notify()

    @with_db
    def test_sandbox_commit_error_sandbox_down_restart(self, ses, app):

        with preamble():
            self.start_sandbox(app)
            self.assertFalse(app.versions)
         
        with ses.begin():
            app.sandbox_state = S.STARTING
        self.dk.remove_container(self.ctrl.gen_sandbox_name(app), force=True)
        self.check_sandbox_running({app: False})

        error = "commit error: sandbox is down"

        with    self.check_sandbox_events(app, 
                    "create start die destroy create start"), \
                self.check_sandbox_transition(app, S.STARTING, S.RUNNING):
            self.commit_sandbox(app, "1.0", error=error, recovery=False)

        self.check_sandbox_running({app: True})

        with ses.begin():
            # no recovery version
            self.assertEqual(len(app.versions), 1)



    @with_db
    def test_sandbox_stop(self, ses, app):

        with preamble():
            self.start_sandbox(app)

        with self.check_sandbox_events(app, "kill die destroy"):
            self.stop_sandbox(app)

        self.check_sandbox_running({app: False})
        with ses.begin():
            # no commit
            self.assertFalse(app.versions)

    @with_db
    def test_sandbox_stop_already_down(self, ses, app):

        with preamble():
            self.start_sandbox(app)

        self.dk.remove_container(self.ctrl.gen_sandbox_name(app), force=True)
        self.check_sandbox_running({app: False})

        with self.check_sandbox_events(app, ""):
            self.stop_sandbox(app)

        self.check_sandbox_running({app: False})
        with ses.begin():
            # no commit
            self.assertFalse(app.versions)

    @with_db
    def test_sandbox_init_entrypoint(self, ses, app):

        ctr = "%s-sandbox-%s" % (ENV, app.docker_name)
        entrypoint = "/home/allgo/entrypoint"

        # create sandbox from scratch
        with preamble():
            with ses.begin():
                app.entrypoint = entrypoint
            self.start_sandbox(app)
            self.check_sandbox_running({app: "factory/test-busybox:latest"})
            self.assertFalse(app.versions)


        # ensure the dummy entrypoint is installed
        code, out = self.dk_exec(ctr, [entrypoint, "foo", "bar", "1", "2", "3"])
        self.assertIn(b"This is app 'test-app' called with parameters 'foo bar 1 2 3'", out)
        self.assertEqual(code, 0)

        # overwrite the entrypoint
        code, out = self.dk_exec(ctr, ["/bin/sh", "-c", "(echo '#!/bin/sh' ; echo 'echo pouet') >%r" % entrypoint])
        self.assertEqual(code, 0)

        code, out = self.dk_exec(ctr, [entrypoint, "foo", "bar", "1", "2", "3"])
        self.assertEqual((code, out), (0, b"pouet\n"))

        # commit
        self.commit_sandbox(app, "1.0", stop=True)
        self.check_sandbox_running({app: None})

        # start the sandbox (again)
        self.start_sandbox(app, "1.0")

        # ensure the entrypoint is not reset
        code, out = self.dk_exec(ctr, [entrypoint, "foo", "bar", "1", "2", "3"])
        self.assertEqual((code, out), (0, b"pouet\n"))

    @with_db
    def test_sandbox_process_shutdown(self, ses, app):
        with preamble():
            ver = self.add_dummy_version(app, "1.0")
        
        with ses.begin():
            app.sandbox_state = S.STARTING
            app.sandbox_version = ver

        with mock.patch("controller.ImageManager.pull") as m, \
                self.check_log("info", "sandbox 'test-app' start aborted \(controller shutdown\)"):
            m.side_effect = controller.ShuttingDown()
            self.notify()
            time.sleep(.1)

        with ses.begin():
            self.assertEqual(app.sandbox_state, S.STARTING)

        self.check_sandbox_running({app: False})


    @with_db
    def test_job_new(self, ses, app):
        with preamble():
            self.add_dummy_version(app, "1.0")

        job = self.create_job(app, "1.0", state=J.NEW)
        self.notify()
        time.sleep(.1)
        self.assertEqual(job.state, J.NEW)

    @with_db
    def test_job_basic(self, ses, app):
        with preamble():
            self.add_dummy_version(app, "1.0")

        job = self.create_job(app, "1.0", 'echo "coin coin" ; echo "Hello World!" >hello ; cat foo > bar',
                files = {"foo": "this is foo\n"})
        with self.check_job_transition(job, J.WAITING, J.DONE, ignore_state=J.RUNNING):
            self.notify()

        self.check_job_output(job, "coin coin\n\n====  ALLGO JOB SUCCESS  ====\n")
        self.check_job_output(job, "Hello World!\n",    filename="hello")
        self.check_job_output(job, "this is foo\n",     filename="bar")

        self.assertEqual(job.exec_time, 0)

    @with_db
    def test_job_exit_code(self, ses, app):
        with preamble():
            self.add_dummy_version(app, "1.0")

        with part("case 1: exit(0)"):
            job = self.create_job(app, "1.0", 'echo ok')
            with self.check_job_transition(job, J.WAITING, J.DONE, ignore_state=J.RUNNING):
                self.notify()

            self.check_job_output(job, "ok\n\n====  ALLGO JOB SUCCESS  ====\n")
            self.assertEqual(job.result, JobResult.SUCCESS)

        with part("case 1: exit(24)"):
            job = self.create_job(app, "1.0", 'echo bad; exit 24', access_token="4567")
            with self.check_job_transition(job, J.WAITING, J.DONE, ignore_state=J.RUNNING):
                self.notify()

            self.check_job_output(job, "bad\n\n====  ALLGO JOB ERROR  ====\nprocess exited with code 24\n")
            self.assertEqual(job.result, JobResult.ERROR)

    @with_db
    def test_job_alt_queue(self, ses, app):
        with preamble():
            self.add_dummy_version(app, "1.0")
            with ses.begin():
                queue = JobQueue(name="test_alt_queue", timeout=42)
                ses.add(queue)


        job1 = self.create_job(app, "1.0")
        job2 = self.create_job(app, "1.0", queue_id=queue.id, access_token="123456")
        
        self.assertEqual(self.ctrl.gen_job_name(job1), "test-job-default-%d-test-app" % job1.id)
        self.assertEqual(self.ctrl.gen_job_name(job2), "test-job-test_alt_queue-%d-test-app" % job2.id)


    #TODO: remove after migration (now container id is stored as Job.container_id at creation time)
    @with_db
    def test_job_with_no_container_id(self, ses, app):
        with preamble():
            self.add_dummy_version(app, "1.0")

        with part("case 1: container still present"):
            job = self.create_job(app, "1.0", '', state=J.RUNNING)

            # create the container manually
            cid = self.dk.create_container("%s/webapp/%s:1.0" % (REGISTRY, app.docker_name),
                    command="/bin/sh -c 'sleep 1;echo coin coin'",
                    name="%s-job-%d-%s" % (ENV, job.id, app.docker_name))
            self.dk.start(cid)

            with self.check_job_transition(job, J.RUNNING, J.DONE):
                self.notify()

            self.assertEqual(job.exec_time, 1)
            self.assertEqual(job.result, JobResult.SUCCESS)

        with part("case 2: container already removed"):
            job = self.create_job(app, "1.0", '', state=J.RUNNING, access_token="456789")

            # do not create the container
            with self.check_job_transition(job, J.RUNNING, J.DONE):
                self.notify()

            self.assertEqual(job.exec_time, 0)
            self.assertEqual(job.result, JobResult.ERROR)

    @with_db
    def test_job_start_error(self, ses, app):
        with preamble():
            self.add_dummy_version(app, "1.0")

        with mock.patch("docker.APIClient.start", side_effect = Exception("STARTERROR")):
            job = self.create_job(app, "1.0", 'echo "Hello World!"')
            with self.check_job_transition(job, J.WAITING, J.DONE, ignore_state=J.RUNNING):
                self.notify()

        self.assertRaises(docker.errors.NotFound, self.dk.inspect_container, self.ctrl.gen_job_name(job))
        self.assertEqual(job.exec_time, 0)
        self.assertEqual(job.result, JobResult.ERROR)


    @with_db
    def test_job_start_unknown_webapp(self, ses, app):
        with preamble():
            self.add_dummy_version(app, "1.0")

        job = self.create_job(app, "1.0", 'echo "Hello World!"')
        with ses.begin():
            ses.delete(app)
        with    self.check_log("error", r"webapp id \d+ not found"),    \
                self.check_job_transition(job, J.WAITING, J.DONE, timeout=1):
            self.notify()
        self.assertEqual(job.result, JobResult.ERROR)

    @with_db
    def test_job_start_unknown_version(self, ses, app):
        with preamble():
            self.add_dummy_version(app, "1.0")

        job = self.create_job(app, "1.1", 'echo "Hello World!"')
        with    self.check_log("error", "webapp 'test-app' version '1.1' not found"),    \
                self.check_job_transition(job, J.WAITING, J.DONE, timeout=1):
            self.notify()
        self.assertEqual(job.result, JobResult.ERROR)

    @with_db
    def test_job_remove_error_already_removed(self, ses, app):
        with preamble():
            self.add_dummy_version(app, "1.0")

        def dummy_start(client, cid, *k, **kw):
            client.remove_container(cid)
            raise Exception("STARTERROR")
        with mock.patch("docker.APIClient.start", dummy_start):
            job = self.create_job(app, "1.0", 'echo "Hello World!"')
            with self.check_job_transition(job, J.WAITING, J.DONE, ignore_state=J.RUNNING):
                self.notify()

        self.assertRaises(docker.errors.NotFound, self.dk.inspect_container, self.ctrl.gen_job_name(job))
        self.assertEqual(job.exec_time, 0)
        self.assertEqual(job.result, JobResult.ERROR)


    @with_db
    def test_job_abort(self, ses, app):
        with preamble():
            self.add_dummy_version(app, "1.0")

        with part("case 1: graceful exit (SIGTERM) works)"):
            job = self.create_job(app, "1.0", 'echo foo ; hnd() { echo exiting ; sleep 1 ; kill %1 ; exit ; } ; trap hnd TERM ; sleep 3600 & wait ; echo bar')

            with self.check_job_transition(job, J.WAITING, J.RUNNING,):
                self.notify()

            with ses.begin():
                job.state = int(J.ABORTING)
            with self.check_job_transition(job, J.ABORTING, J.DONE):
                self.notify()

            self.check_job_output(job, "foo\n\n====  ALLGO JOB ABORT  ====\nexiting\n")
            self.assertEqual(job.exec_time, 1)
            self.assertEqual(job.result, JobResult.ABORTED)

        with part("case 2: graceful exit fails -> fallback to SIGKILL"):
            job = self.create_job(app, "1.0", 'echo foo ; hnd() { echo not exiting ; sleep 1 ; } ; trap hnd TERM ; sleep 3600 & wait ; wait ; echo bar', access_token="456789")

            with self.check_job_transition(job, J.WAITING, J.RUNNING,):
                self.notify()

            with ses.begin():
                job.state = int(J.ABORTING)
            with self.check_job_transition(job, J.ABORTING, J.DONE):
                self.notify()

            self.check_job_output(job, "foo\n\n====  ALLGO JOB ABORT  ====\nnot exiting\n")
            self.assertEqual(job.exec_time, 5)
            self.assertEqual(job.result, JobResult.ABORTED)

    @with_db
    def test_job_timeout(self, ses, app):
        with preamble():
            self.add_dummy_version(app, "1.0")
            with ses.begin():
                queue = JobQueue(name="test_short_queue", timeout=2)
                ses.add(queue)

        with part("case 1: graceful exit (SIGALRM) works)"):
            job = self.create_job(app, "1.0", 'echo foo ; hnd() { echo exiting ; sleep 1 ; kill %1 ; exit ; } ; trap hnd ALRM ; sleep 3600 & wait ; echo bar',
                    queue_id=queue.id)

            with self.check_job_transition(job, J.WAITING, J.DONE, ignore_state=J.RUNNING):
                self.notify()

            self.check_job_output(job, "foo\n\n====  ALLGO JOB TIMEOUT  ====\nexiting\n")
            self.assertEqual(job.exec_time, 3)
            self.assertEqual(job.result, JobResult.TIMEOUT)

        with part("case 2: graceful exit fails -> fallback to SIGKILL"):
            job = self.create_job(app, "1.0", 'echo foo ; hnd() { echo not exiting ; } ; trap hnd ALRM ; sleep 3600 & wait ; wait ; echo bar',
                    queue_id=queue.id, access_token="456789")

            with self.check_job_transition(job, J.WAITING, J.DONE, ignore_state=J.RUNNING):
                self.notify()

            self.check_job_output(job, "foo\n\n====  ALLGO JOB TIMEOUT  ====\nnot exiting\n")
            self.assertEqual(job.exec_time, 7)
            self.assertEqual(job.result, JobResult.TIMEOUT)

    @with_db
    def test_job_controller_interrupted(self, ses, app):
        with preamble():
            self.add_dummy_version(app, "1.0")
            with ses.begin():
                queue = JobQueue(name="test_short_queue", timeout=3)
                ses.add(queue)


        def test(remove=False, sema_lock=False, abort=False, timeout=False, error=False):
            token = str("".join(map(str, (12345,  remove, sema_lock, abort, timeout, error))))

            if abort or timeout:
                append = " ; trap 'kill $pid' ALRM TERM ; sleep 10 & pid=$! ; while ! wait ; do true ; done"
            elif error:
                append = " ; exit 17"
            else:
                append = ""
            queue_id = queue.id if timeout else 1
            job = self.create_job(app, "1.0", 'sleep .4 ; echo "Hello World!"' + append,
                        access_token = token, queue_id=queue_id)

            with mock.patch("shared_swarm.APIClient.wait_async", side_effect=controller.ShuttingDown):
                with self.check_job_transition(job, J.WAITING, J.RUNNING):
                    self.notify()

                ctr = "%s-job-%s-%d-test-app" % (ENV, job.queue.name, job.id)
                self.wait_created(ctr)
                if remove:
                    self.dk.remove_container(ctr, force=True)
                if sema_lock:
                    self.ctrl.job_manager._semaphore._value = -10 # negative value to avoid race condition
                if abort:
                    with ses.begin():
                        job.state = int(J.ABORTING)
            
                if timeout:
                    time.sleep(1)
                else:
                    time.sleep(.1) # because JobManager reschedules the task

            with self.check_job_transition(job, job.state, J.DONE):
                self.notify()

            if abort:
                self.check_job_output(job, "\n====  ALLGO JOB ABORT  ====\n")
                self.assertEqual(job.exec_time, 0)
                self.assertEqual(job.result, JobResult.ABORTED)
            elif timeout:
                self.check_job_output(job, "Hello World!\n\n====  ALLGO JOB TIMEOUT  ====\n")
                self.assertEqual(job.exec_time, 3)
                self.assertEqual(job.result, JobResult.TIMEOUT)
            elif remove:
                self.assertEqual(job.result, JobResult.ERROR)
            elif error:
                self.check_job_output(job, "Hello World!\n\n====  ALLGO JOB ERROR  ====\nprocess exited with code 17\n")
                self.assertEqual(job.exec_time, 0)
                self.assertEqual(job.result, JobResult.ERROR)
            else:
                self.check_job_output(job, "Hello World!\n\n====  ALLGO JOB SUCCESS  ====\n")
                self.assertEqual(job.exec_time, 0)
                self.assertEqual(job.result, JobResult.SUCCESS)
            self.assertRaises(docker.errors.NotFound, self.dk.inspect_container, ctr)

        
        with part("case 1: normal"):
            test()

        with part("case 2: non-zero exit"):
            test(error=True)

        with part("case 3: container removed"):
            test(remove=True)

        with part("case 4: JobManager semaphore locked"):
            test(sema_lock=True)

        with part("case 5: job aborted"):
            test(abort=True)

        with part("case 6: job timeout"):
            test(timeout=True)

    @with_db
    def test_job_from_sandbox(self, ses, app):
        with preamble():
            with ses.begin():
                app.entrypoint = "/bin/entrypoint"
            self.add_dummy_version(app, "1.0",
                    append = """
                    RUN echo "echo foo" > /bin/entrypoint && chmod 755 /bin/entrypoint
                    """)
            self.start_sandbox(app, "1.0")
            self.check_sandbox_running({app: "webapp/test-app:1.0"})

        # modify entrypoint
        self.dk_exec("%s-sandbox-%s" % (ENV, app.docker_name), ["/bin/sh", "-c", """
            echo "echo bar" > /bin/entrypoint
        """])

        images = self.dk.images()
        
        with part("case 1: swarm job"):
            job = self.create_job(app, "1.0")
            with self.check_job_transition(job, J.WAITING, J.DONE, ignore_state=J.RUNNING):
                self.notify()

            self.check_job_output(job, "foo\n\n====  ALLGO JOB SUCCESS  ====\n")
            self.assertEqual(job.result, JobResult.SUCCESS)

        self.assertListEqual(images, self.dk.images(), msg="tmp img must not be created")

        with part("case 2: sandbox job"):
            job = self.create_job(app, "sandbox", access_token="456789")
            with self.check_job_transition(job, J.WAITING, J.DONE, ignore_state=J.RUNNING):
                self.notify()

            self.check_job_output(job, "bar\n\n====  ALLGO JOB SUCCESS  ====\n")
            self.assertEqual(job.result, JobResult.SUCCESS)

        self.assertListEqual(images, self.dk.images(), msg="tmp img must be removed")

        with part("case 3: sandbox job create error"):
            job = self.create_job(app, "sandbox", access_token="789789")
            with mock.patch("docker.APIClient.create_container", side_effect=DockerError("CREATEERROR")),\
                    self.check_job_transition(job, J.WAITING, J.DONE, ignore_state=J.RUNNING):
                self.notify()

            self.check_job_output(job, None)
            self.assertEqual(job.result, JobResult.ERROR)

        self.assertListEqual(images, self.dk.images(), msg="tmp img must be removed")

        with part("case 4: sandbox job start error"):
            job = self.create_job(app, "sandbox", access_token="789903")
            with mock.patch("docker.APIClient.start", side_effect=DockerError("STARTERROR")),\
                    self.check_job_transition(job, J.WAITING, J.DONE, ignore_state=J.RUNNING):
                self.notify()

            self.check_job_output(job, None)
            self.assertEqual(job.result, JobResult.ERROR)

        self.assertListEqual(images, self.dk.images(), msg="tmp img must be removed")

        # modify entrypoint (again)
        self.dk_exec("%s-sandbox-%s" % (ENV, app.docker_name), ["/bin/sh", "-c", """
            echo "echo hello" > /bin/entrypoint
        """])

        with part("case 5: sandbox job interrupted"):
            job = self.create_job(app, "sandbox", access_token="785469")

            with mock.patch("shared_swarm.APIClient.wait_async", side_effect=controller.ShuttingDown):
                with self.check_job_transition(job, J.WAITING, J.RUNNING):
                    self.notify()

                ctr = "%s-job-default-%d-test-app" % (ENV, job.id)
                self.wait_created(ctr)
                time.sleep(.1) # because JobManager reschedules the task

            with self.check_job_transition(job, J.RUNNING, J.DONE):
                self.notify()

            self.check_job_output(job, "hello\n\n====  ALLGO JOB SUCCESS  ====\n")
            self.assertEqual(job.result, JobResult.SUCCESS)

        self.assertListEqual(images, self.dk.images(), msg="tmp img must be removed")
        
    @with_db
    def test_job_from_sandbox_to_swarm(self, ses, app):

        with preamble():
            # random token to ensure we are not using old images from the
            # registry
            rnd = '%08x.' % random.randint(1,0x100000000)

            with ses.begin():
                app.entrypoint = "/bin/entrypoint"
            self.add_dummy_version(app, "1.0",
                    append = """
                    RUN echo "echo %sfoo" > /bin/entrypoint && chmod 755 /bin/entrypoint
                    """ % rnd)
            self.start_sandbox(app, "1.0")
            self.check_sandbox_running({app: "webapp/test-app:1.0"})

        def update_entrypoint(txt):
            self.dk_exec("%s-sandbox-%s" % (ENV, app.docker_name), ["/bin/sh", "-c", """
                echo "echo %s" > /bin/entrypoint
            """ % txt])

    
        @contextmanager
        def interleave(target, *, timeout=10):

            def replacement(*k, **kw):
                # run in the thread running the target function

                barrier.wait(timeout)
                barrier.wait(timeout)
                log.debug("########## enter %s()", target)
                try:
                    return original(*k, **kw)
                finally:
                    log.debug("########## leave %s()", target)

            @contextmanager
            def inner_context():
                barrier.wait(timeout)
                log.debug("########## interleave: %s() invoked --> run inner context", target)
                try:
                    yield
                finally:
                    log.debug("########## interleave: inner context done --> call %s()", target)
                    barrier.wait(timeout)

            barrier  = threading.Barrier(2)
            patch    = mock.patch(target, replacement)
            original = patch.get_original()[0]

            with patch:
                log.debug("########## interleave: wait until %s() is called", target)
                yield inner_context()
                log.debug("########## interleave: outer context done")

        
        def get_version(ver_id):
            with ses.begin():
                return ses.query(WebappVersion).filter_by(id=ver_id).first()

        def ensure_version_ready():
            time.sleep(2)
            with ses.begin():
                self.assertFalse(
                        ses.query(WebappVersion.id, WebappVersion.state)
                            .filter_by(webapp_id=app.id)
                            .filter(WebappVersion.state.notin_((int(V.READY), int(V.DELETED)))).all())

        with part("case 1: image replaced but not yet committed -> use previous image"), \
                interleave("docker.APIClient.commit") as commit_interleave:
            update_entrypoint(rnd+"bar1")

            ver_id = self.commit_sandbox(app, "1.0", wait=False).id
            self.notify()

            with commit_interleave:
                job = self.create_job(app, "1.0")
                with self.check_job_transition(job, J.WAITING, J.DONE, ignore_state=J.RUNNING):
                    self.notify()

                self.check_job_output(job, rnd+"foo\n\n====  ALLGO JOB SUCCESS  ====\n")

            # wait until the image is replaced
            wait_until(lambda: get_version(ver_id).state == V.DELETED, timeout=10)

            ensure_version_ready()


        with part("case 2: image replaced, but not yet pushed -> use previous image"), \
                interleave("docker.APIClient.push") as push_interleave:
            update_entrypoint(rnd+"bar2")

            ver_id = self.commit_sandbox(app, "1.0", wait=False).id
            self.notify()

            with push_interleave:
                job = self.create_job(app, "1.0", access_token="76541")
                with self.check_job_transition(job, J.WAITING, J.DONE, ignore_state=J.RUNNING):
                    self.notify()

                self.check_job_output(job, rnd+"bar1\n\n====  ALLGO JOB SUCCESS  ====\n")

            # wait until the image is replaced
            wait_until(lambda: get_version(ver_id).state == V.DELETED, timeout=10)

            ensure_version_ready()

        # TODO: case 3:  image replaced, but not yet pushed -> use next image
        # (to be implemented when Job links to WebappVersion.id)

        with part("case 4: new version -> wait next image"), \
                interleave("docker.APIClient.push") as push_interleave:
            update_entrypoint(rnd+"bar3")

            ver_id = self.commit_sandbox(app, "1.1", wait=False).id
            self.notify()

            with push_interleave:
                job = self.create_job(app, "1.1", access_token="76234541")
                self.notify()
                time.sleep(1)

            with self.check_job_transition(job, J.WAITING, J.DONE, ignore_state=J.RUNNING):
                pass

            self.check_job_output(job, rnd+"bar3\n\n====  ALLGO JOB SUCCESS  ====\n")

            # wait until the image is replaced
            wait_until(lambda: get_version(ver_id).state == V.READY, timeout=10)

            ensure_version_ready()

    @with_db
    def test_check_db_startup(self, ses, app):

        with preamble():
            committed = (
                self.add_dummy_version(app, "1.0", state=V.COMMITTED).id,
                self.add_dummy_version(app, "1.1", state=V.COMMITTED).id,
                )
            self.add_dummy_version(app, "2.0", state=V.READY)
            self.add_dummy_version(app, "2.1", state=V.READY)
            replaced = (
                self.add_dummy_version(app, "3.0", state=V.DELETED).id,
                self.add_dummy_version(app, "3.1", state=V.DELETED).id,
                )

        with mock.patch("controller.ImageManager.push") as m_push:
            self.notify(startup=True)
            time.sleep(.1)

            for version_id in committed:
                m_push.assert_any_call(version_id)
            self.assertEqual(m_push.call_count, 2)

            with ses.begin():
                self.assertFalse(
                        ses.query(WebappVersion.id).filter(WebappVersion.id.in_(replaced)).all())



class ManagerTestCase(unittest.TestCase):

    def check_manager(self, nb_threads, events, *, cleanup_dead_conditions=False):
        """Test the manager class with a sequence of events

        expect is a list of events:
            "process N" call Manager.process(N)     (stimulus)
            "finish N"  exit task N                 (stimulus)
            "reset N"   reset task N                (stimulus)
            "shutdown"  call Manager.shutdown()     (stimulus)

            "start N"   task id N started           (response)
            "stop  N"   task id N terminated        (response)
            "exc   N"   task id N terminated with ShuttingDown() (response)

        """
        expect_lst = [re.match(r"\s*(\S+)(?:\s+(\S+))?\s*$", line).groups()
                for line in events.splitlines()
                if re.search(r"\S", line)]
        
        lock = threading.Lock()
            
        # {id: Condition}
        conditions = {}
        dead_conditions = []
        resets = {}

        def worker(id):
            with lock:
                self.assertIn(id, conditions)
                actual_lst.append(("start", id))
                conditions[id].wait()
                actual_lst.append(("stop", id))

        @asyncio.coroutine
        def _process(id, reset, rescheduled):
            with lock:
                self.assertNotIn(id, conditions)
                conditions[id] = threading.Condition(lock)
                resets[id] = reset

            try:
                yield from manager.run_in_executor(worker, id)
            except controller.ShuttingDown:
                actual_lst.append(("exc", id))


            with lock:
                self.assertIn(id, conditions)
                dead_conditions.append(conditions.pop(id))
                del resets[id]

        def finish(id):
            with lock:
                self.assertIn(id, conditions)
                conditions[id].notify()

        @asyncio.coroutine
        def shutdown():
            yield from manager.shutdown()
            actual_lst.append(("terminated", None))

        manager = controller.Manager(nb_threads)
        manager._process = _process

        actual_lst = []

        @asyncio.coroutine
        def run():
            for event, id in expect_lst:
                if event in ("start", "stop", "exc", "terminated"):
                    continue

                yield from asyncio.sleep(0.01)

                actual_lst.append((event, id))

                if event == "process":
                    manager.process(id)
                elif event == "finish":
                    finish(id)
                elif event == "reset":
                    resets[id]()
                elif event == "shutdown":
                    asyncio.async(shutdown())
                else:
                    assert 0, "bad event"

            yield from asyncio.sleep(0.01)

        asyncio.get_event_loop().run_until_complete(run())

        with lock:
            for cond in conditions.values():
                cond.notify()
        manager.shutdown()

        self.assertListEqual(actual_lst, expect_lst)

        if cleanup_dead_conditions:
            with lock:
                for cond in dead_conditions:
                    cond.notify()

    def test_manager_single(self):

        # 1 call
        self.check_manager(5, """
            process 1
            start 1
            finish 1
            stop 1
        """)

        # 2 calls (non-overlapping)
        self.check_manager(5, """
            process 1
            start 1
            finish 1
            stop 1

            process 1
            start 1
            finish 1
            stop 1
        """)
        # 4 calls (non-overlapping)
        self.check_manager(5, """
            process 1
            start 1
            finish 1
            stop 1

            process 1
            start 1
            finish 1
            stop 1

            process 1
            start 1
            finish 1
            stop 1

            process 1
            start 1
            finish 1
            stop 1
        """)


        # 4 calls (overlapping)
        self.check_manager(5, """
            process 1
            start 1
            process 1
            process 1
            process 1
            finish 1
            stop 1

            start 1
            finish 1
            stop 1
        """)

    def test_manager_multiple_tasks(self):
        
        # one worker
        self.check_manager(1, """
            process 1
            start 1
            process 1
            process 2
            process 3
            finish 1
            stop 1
            start 2
            finish 2
            stop 2
            start 3
            finish 3
            stop 3
            start 1
            finish 1
            stop 1
        """)


        # two worker
        self.check_manager(2, """
            process 1
            start 1
            process 1
            process 2
            start 2
            process 3
            process 3
            process 2
            finish 1
            stop 1
            start 3
            finish 2
            stop 2
            start 1
            finish 3
            stop 3
            start 2
            finish 1
            stop 1
            start 3
            finish 3
            stop 3
            finish 2
            stop 2
        """)

    def test_manager_reset(self):

        # no reset
        self.check_manager(2, """
            process 1
            start 1
            process 1
            finish 1
            stop 1
            start 1
            finish 1
            stop 1
        """)

        # with reset
        self.check_manager(2, """
            process 1
            start 1
            process 1
            reset 1
            finish 1
            stop 1
        """)

        # with reset (twice)
        self.check_manager(2, """
            process 1
            start 1
            process 1
            reset 1
            reset 1
            finish 1
            stop 1
        """)

        # with reset + process again
        self.check_manager(2, """
            process 1
            start 1
            process 1
            reset 1
            process 1
            finish 1
            stop 1
            start 1
            finish 1
            stop 1
        """)

    def test_manager_shutdown(self):
        self.check_manager(2, """
            process 1
            start 1
            process 1
            process 2
            start 2
            process 3
            process 4
            shutdown
            finish 1
            stop 1
            exc 3
            exc 4
            process 1
            finish 2
            stop 2
            terminated
            process 2
        """)

        init = controller.Manager.__init__
        with mock.patch("controller.Manager.__init__", lambda *k: init(*k, interruptible=True)):
            self.check_manager(2, """
                process 1
                start 1
                process 1
                process 2
                start 2
                process 3
                process 4
                shutdown
                terminated
                exc 1
                exc 2
                exc 3
                exc 4
            """, cleanup_dead_conditions=True)


    def test_manager_notimpl(self):
        self.assertRaises(NotImplementedError,
            asyncio.get_event_loop().run_until_complete,
                controller.Manager(1)._process(1, None, None))


    @mock.patch("controller.log.exception")
    def test_manager_futures(self, mexc):
        futures = {}
        rescheduled = {}
        reset = {}

        def worker(id, rst, rsc_fut):
            assert id not in futures or futures[id].done()
            fut = futures[id] = concurrent.futures.Future()
            reset[id]         = rst
            rescheduled[id]   = rsc_fut
            return fut.result()

        manager = controller.Manager(2)
        manager._process = asyncio.coroutine(
                lambda *k: manager.run_in_executor(worker, *k))


        @asyncio.coroutine
        def run():
            ## (1) run a single task
            fut = manager.process(1)
            yield from asyncio.sleep(.1)
            self.assertFalse(fut.done())
            self.assertFalse(rescheduled[1].done())
            futures[1].set_result(42)
            self.assertEquals(42, (yield from fut))
            self.assertFalse(rescheduled[1].done())

            ## (2.a) run the same task twice in a row
            fut  = manager.process(2)
            yield from asyncio.sleep(.1)
            self.assertFalse(rescheduled[2].done())

            fut2 = manager.process(2)

            # -> fut2 should yield after fut (because the task need to be rescheduled)
            self.assertIsNot(fut, fut2)

            # -> rescheduled[2] must be done (to let the current task know that
            #    it is being rescheduled)
            self.assertIs(rescheduled[2].result(), None)

            yield from asyncio.sleep(.1)
            self.assertFalse(fut.done())
            self.assertFalse(fut2.done())

            ## (2.b) first execution done
            futures[2].set_result(43)
            # expect: fut terminated, fut2 still pending
            yield from asyncio.wait_for(fut, timeout=.1)
            self.assertEquals(fut.result(), 43)
            self.assertFalse(fut2.done())

            yield from asyncio.sleep(.1)
            # since the task is restarted, the 'rescheduled' is recreated
            self.assertFalse(rescheduled[2].done())


            ## (2.c) second execution raises an error
            mexc.reset_mock()
            exc = RuntimeError(44)
            futures[2].set_exception(exc)
            yield from asyncio.sleep(.1)
            mexc.assert_called_with('task %r %r unhandled exception', manager, 2)
            # expect: fut & fut2 terminated
            with self.assertRaises(RuntimeError):
                yield from asyncio.wait_for(fut2, timeout=.1)
            self.assertEquals(fut.result(),  43)
            self.assertIs(fut2.exception(), exc)

            ## (3) reset case
            ## (3.a) call .process() twice in a row
            fut  = manager.process(3)
            yield from asyncio.sleep(.1)
            self.assertFalse(rescheduled[3].done())

            fut2 = manager.process(3)
            self.assertIs(rescheduled[3].result(), None) # got notification

            ## (3.b) reset the task
            rescheduled[3] = reset[3]()
            yield from asyncio.sleep(.1)
            self.assertFalse(rescheduled[3].done())
            self.assertFalse(fut.done())
            self.assertFalse(fut2.done())

            ## (3.c) reschedule the task (again)
            fut3 = manager.process(3)
            self.assertIs(rescheduled[3].result(), None) # got notification

            ## (3.d) reset the task (again)
            rescheduled[3] = reset[3]()
            yield from asyncio.sleep(.1)
            self.assertFalse(rescheduled[3].done())
            self.assertFalse(fut.done())
            self.assertFalse(fut2.done())
            self.assertFalse(fut3.done())

            ## (3.e) finish the task
            futures[3].set_result(44)
            yield from asyncio.sleep(.1)
            self.assertEqual(fut.result(), 44)
            self.assertEqual(fut2.result(), 44)
            self.assertEqual(fut3.result(), 44)
            self.assertFalse(rescheduled[3].done())


            # (4) ShuttingDown case
            #   - start task 4 before shutdown
            #   - start task 5 after  shutdown
            mexc.reset_mock()
            fut4 = manager.process(4)
            yield from asyncio.sleep(.1)
            shutdown_task = asyncio.async(manager.shutdown())
            fut5 = manager.process(5)
            yield from asyncio.sleep(.1)
            self.assertFalse(shutdown_task.done())
            futures[4].set_result(45)
            # shutdown completed
            # -> task 4 completed
            # -> task 5 not started
            yield from asyncio.wait_for(shutdown_task, timeout=.1)
            self.assertEqual(fut4.result(), 45)
            self.assertRaises(controller.ShuttingDown, fut5.result)
            self.assertFalse(mexc.called)

        try:
            asyncio.get_event_loop().run_until_complete(run())
        finally:
            for fut in futures.values():
                if not fut.done():
                    fut.set_exception(Exception("CANCELLED"))



class UtilsTestCase(unittest.TestCase):

    @as_coroutine
    def test_cascade_future(self):
        # result
        src, dst = asyncio.Future(), asyncio.Future()
        controller.cascade_future(src, dst)
        self.assertFalse(src.done())
        self.assertFalse(dst.done())
        val = "7895"
        src.set_result(val)
        self.assertIs(val, (yield from asyncio.wait_for(dst, timeout=.1)))

        # exception
        src, dst = asyncio.Future(), asyncio.Future()
        controller.cascade_future(src, dst)
        self.assertFalse(src.done())
        self.assertFalse(dst.done())
        val = RuntimeError("pouet")
        src.set_exception(val)
        with self.assertRaises(RuntimeError):
            yield from asyncio.wait_for(dst, timeout=.1)
        self.assertIs(dst.exception(), val)

        # already done
        src, dst = asyncio.Future(), asyncio.Future()
        val = "7895"
        src.set_result(val)
        controller.cascade_future(src, dst)
        self.assertIs(val, (yield from asyncio.wait_for(dst, timeout=.1)))

    @as_coroutine
    def test_auto_create_task(self):

        @controller.auto_create_task
        @asyncio.coroutine
        def func(a, b=1, *, c):
            return (a, b, c)

        tsk = func(4, c=7)
        self.assertIsInstance(tsk, asyncio.Task)
        with mock.patch.object(tsk, "exception") as m:
            self.assertEqual((4,1,7), (yield from asyncio.wait_for(tsk, 0.1)))
            m.assert_called_with()


    @mock.patch("controller.log")
    def test_report_error(self, mlog):
        merr = mlog.error
        mexc = mlog.exception

        with controller.report_error("foo %d", 1):
            pass
        self.assertFalse(merr.called)
        self.assertFalse(mexc.called)

        with self.assertRaises(ValueError), \
                controller.report_error("foo %d", 2):
            raise ValueError("bar")

        mexc.assert_called_with('%s (%s)', 'foo 2', 'ValueError: bar')
        mexc.reset_mock()
        self.assertFalse(merr.called)

        with self.assertRaises(controller.Error), \
                controller.report_error("foo %d", 3):
            raise controller.Error("hello")

        merr.assert_called_with('%s (%s)', 'foo 3', 'controller.Error: hello')
        self.assertFalse(mexc.called)
