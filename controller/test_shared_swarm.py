import asyncio
import collections
import concurrent.futures
import contextlib
import itertools
import json
import numbers
import queue
import re
import sys
import tempfile
import threading
import time
import unittest
from   unittest import mock

import docker
from   termcolor import colored
import yaml

import config_reader
import shared_swarm
from   shared_swarm import APIClient

K=1024**1
M=1024**2
G=1024**3
T=1024**4

PERIOD = .01

generic_id = lambda name: "<<ID %s>>" % name
    
class UnexpectedCall(Exception):
    pass
class NotReachable(Exception):
    pass

class ContainerState(shared_swarm._InitReprMixin):
   __slots__ = "node", "cpu", "mem", "state", "code"

@contextlib.contextmanager
def temp_file(content):
    with tempfile.NamedTemporaryFile("w+") as tmp:
        tmp.write(content)
        tmp.flush()
        yield tmp.name

def raise_docker_NotFound():
    with mock.patch("docker.errors.APIError.__init__", return_value=None):
        raise docker.errors.NotFound()

@contextlib.contextmanager
def patch_bases(cls):
    """patch the base classes of cls with an autospec mock
    
    This context manager replaces the base class of cls with an ad-hoc class
    wrapping an autospec mock of cls
    """
    try:
        saved_bases = cls.__bases__

        dummy_base = type(cls.__name__, saved_bases, {})
        m = mock.create_autospec(dummy_base)

        # create a new base class
        new_base = type("BaseMock"+cls.__name__, (), {
            # populate it with each attribute from the mock
            attr: getattr(m, attr)
            for attr in dir(dummy_base)
                # if it exists in the mock
                if hasattr(m, attr)
                    # but keep 'object' methods as-is if not reimplemented by a base
                    # (we do not want a mock for __new__ and __init__)
                    and getattr(dummy_base, attr) is not getattr(object, attr, None)
            })

        cls.__bases__ = new_base,
        yield m
    finally:
        cls.__bases__ = saved_bases


class Scenario:
    def __init__(self, testcase, config_file):
        self.tc = testcase
        self.config_file = config_file

    def _run_loop(self, fut_or_delay, *, timeout=None, throw=None):
        fut = asyncio.sleep(fut_or_delay) if isinstance(fut_or_delay, numbers.Number) else fut_or_delay
        if timeout is not None:
            fut = asyncio.wait_for(fut, timeout)
        if throw is None:
            return self.loop.run_until_complete(fut)
        else:
            self.tc.assertRaises(throw, self.loop.run_until_complete, fut)

    def _create_task(self, name, container, coro):
        key = "%s-%s" % (name, container)
        if key in self.tasks:
            raise RuntimeError("task %r already exists" % key)
        self.tasks[key] = self.loop.create_task(coro)

    @asyncio.coroutine
    def _wait_task(self, name, container, *, ignore_missing=False):
        key = "%s-%s" % (name, container)
        try:
            task = self.tasks[key]
        except KeyError:
            if ignore_missing:
                return
            raise RuntimeError("task %r does not exist" % key)

        try:
            return (yield from task)
        finally:
            del self.tasks[key]

    def _ensure_tasks_running(self):
        """Ensure that all pending tasks are still running
        
        This function is called before every stimulus (to ensure that there are
        no unexpected responses)
        """
        self._run_loop(PERIOD)
        done = [k for k,v in self.tasks.items() if v.done()]
        assert not done, "tasks prematurely terminated: %s" % " ".join(done)
        

    def _send_event(self, status, name, **kw):
        kw.setdefault("id", generic_id(name))
        kw.setdefault("Actor", {})
        kw["Actor"].setdefault("Attributes", {})
        kw["Actor"]["Attributes"]["name"] = name
        kw["status"] = status
        self.event_queue.put(kw)


    def __enter__(self):
        # {container: ContainerState}
        self.containers = {}
        self.step_counter = itertools.count()

        self.stack = stack = contextlib.ExitStack()
        self.event_queue = queue.Queue()
        self.refresh_future = None
        self.tasks = {}

        try:
            stack.__enter__()

            # asyncio event loop
            self.loop = asyncio.new_event_loop()
            asyncio.set_event_loop(self.loop)
            stack.callback(asyncio.set_event_loop, None)
            stack.callback(self.loop.close)

            # patch the base class (docker.APIClient)
            stack.enter_context(patch_bases(APIClient))
            BaseClient = APIClient.__bases__[0]
            def patch_method(cls):
                return lambda func: stack.enter_context(mock.patch.object(
                        cls, func.__name__, func))

            # TODO: write integration tests to ensure that the behaviour of these functions
            #       is consistent with docker.APIClient()
            @patch_method(BaseClient)
            def events(client, *, filters):
                self.tc.assertEqual(filters, {"event": ["create", "die", "destroy"]})
                while True:
                    ev = self.event_queue.get()
                    if isinstance(ev, Exception):
                        raise ev
                    yield json.dumps(ev).encode()


            @patch_method(shared_swarm._MasterResourceManager)
            def _next_refresh(client):
                logging.debug("enter next_refresh")
                self.refresh_future = concurrent.futures.Future()
                self.refresh_future.result()
                logging.debug("leave next_refresh")

            @patch_method(BaseClient)
            def create_container(client, image, *, name, **kw):
                logging.debug("create_container (mock) %s %s %s", image, name, kw)
                assert name not in self.containers
                hc = kw.get("host_config")
                #print("%r"%kw["environment"][0])
                node = re.match(r"constraint:node==<<ID (.*)>>\Z", kw["environment"][0]).group(1)
                self.containers[name] = ContainerState(node=node, state="running",
                        cpu=hc.get("CpuShares"), mem=hc.get("MemLimit"))
                self._send_event("create", name)
                return {"Id": generic_id(name)}

            @patch_method(BaseClient)
            def containers(client, *, all=False):
                self.tc.assertTrue(all)
                return [{"Id": generic_id(name), "Names": ["/%s/%s" % (state.node, name)],
                    "State": state.state,
                    "Status": (("Exited (%d) FOO" % state.code) if state.state=="exited" else "FOO"),
                    } for name, state in self.containers.items()]

            @patch_method(BaseClient)
            def inspect_container(client, cid):
                mo = re.match("<<ID (.*)>>\Z", cid)
                assert mo, "test mock supports only query by container_id"
                name = mo.group(1)
                state = self.containers.get(name)
                if state is None:
                    raise_docker_NotFound()
                return dict(Id=cid, Name="/%s" % name, Node={"Name": state.node, "ID":
                    generic_id(state.node)},
                    State={"Running": state.state == "running",
                        # NOTE: ExitCode is always an int (0 if unset) but we force an invalid
                        #       value here to ensure that the ExitCode is used only when the
                        #       container has exited
                        "ExitCode": "<<TEST_UNSET>>" if state.code is None else state.code},
                    HostConfig=dict(CpuShares=state.cpu, Memory=state.mem))

            # override .info() at startup so that the swarm contains 0 nodes
            with mock.patch.object(APIClient, "info", return_value={"SystemStatus":()}):

                # create the client
                with config_reader.ConfigReader(open(self.config_file),
                        shared_swarm.log) as cfg:
                    self.client = APIClient(config=cfg)

            self.client.info.side_effect = UnexpectedCall()

            # shutdown
            @stack.callback
            def shutdown():
                exc = sys.exc_info()[1]
                if exc is not None:
                    logging.error(colored("[ABORT] unexpected exception %r", "red"), exc)

                self._ensure_tasks_running()

                self.client.shutdown()
                # send a dummy event because self.event_queue.get() is a blocking call
                self.event_queue.put({})
                self.refresh_future.set_exception(shared_swarm.ShuttingDown("SCENARIO END"))
                self.client.shutdown(wait=True)

                self._run_loop(.1)

                cancelled = {}
                for key, task in self.tasks.items():
                    if task.done():
                        if task.exception() is None:
                            self.tc.assertIsNone(task.result(), msg="unexpected result for task %s" % key)
                        else:
                            self.tc.assertIsInstance(task.exception(), shared_swarm.ShuttingDown,
                                msg="unexpected exception for task %s" % key)
                    else:
                        task.cancel()
                        cancelled[key] = task

                if cancelled:
                    self.loop.run_until_complete(asyncio.gather(*cancelled.values(), return_exceptions=True))
                    raise AssertionError("tasks not completed: %s" % (" ".join(cancelled)))

            self._run_loop(PERIOD)

            return self

        except Exception:
            self.stack.__exit__(*sys.exc_info())
            raise

    def __exit__(self, *exc_info):
        self.stack.__exit__(*exc_info)


    def _gen_info(self, nodes):
        lst= [["\x08Nodes", str(len(nodes))]]
        for name, node in nodes.items():
            id      = node.get("ID", generic_id(name))
            healthy = node.get("healthy", True)
            error   = node.get("error", None)
            cpu     = node.get("cpu", 4)
            mem     = node.get("mem", 4*G)
            lst.extend((
                [f" {name}", "255.0.0.0:2375"],
                ["  └ ID", id],
                ["  └ Status", ("Healthy" if healthy else "Not Healthy")],
                ["  └ Reserved CPUs", "0 / %d" % cpu],
                ["  └ Reserved Memory", "0 GiB / %.2f GiB" % (mem/G)],
                    ))
            # NOTE : it seems that swarm does not mark immediately as "non
            # healthy" a node that goes down but it displays an error
            # immediately (however the error seems to stay there a long time
            # after the node goes up)
            if error is not None:
                lst.append(["  └ Error", error])
            lst.extend(node.get("append", ()))

        return {"SystemStatus": lst}

       
    def __call__(self, event, *k, **kw):
        #################################################
        #                                               #
        #  stimuli                                      #
        #                                               #
        #################################################

        def event_create(name, *, cpu: int , mem: int, node: str =None):
            """Simulate the creation of a container
            
            - If node is None, then the container is created asynchronously 
              (after the ressurces are allocated with .request_slot() +
              .wait_slot()). The event "created" is issued once the container
              is created.

            - If node is given, then then container is created immediatly on
              the requested node, without any ressource allocation (used to
              simulate a creation by a 3rd-party client)

            """

            self._ensure_tasks_running()

            def do_create(node):
                cid = self.client.create_container("scratch", name=name,
                        host_config={"CpuShares": cpu, "MemLimit": mem},
                    environment=["constraint:node==%s" % generic_id(node)])["Id"]
                self.tc.assertEqual(cid, generic_id(name))

                state = self.containers[name]
                assert state.node == node
                assert state.cpu  == cpu
                assert state.mem  == mem
                return cid

            @asyncio.coroutine
            def create_coro():
                try:
                    with self.client.request_slot(name, cpu, mem):
                        # TODO: manage exceptions (not available, shutting down)
                        actual_node = yield from self.client.wait_slot(name)

                        cid = yield from self.loop.run_in_executor(None, do_create, actual_node)
                        # start the waiter
                        self._create_task("wait", name, self.client.wait_async(cid))
                        return actual_node
                except Exception as e:
                    if not isinstance(e, shared_swarm.Error):
                        logging.exception("create_coro() not terminated for %r", name)
                    raise

            if node is None:
                # created by us
                # (asynchronous -> will issue a "created" event when done)
                self._create_task("create", name, create_coro())
            else:
                # created by another client
                # (synchronous)
                do_create(node)
                self._ensure_tasks_running()

        def event_terminate(name, *, code=0):
            """Simulate the termination of a container
            
            code: exit code of the container
            """
            assert isinstance(code, int)

            self._ensure_tasks_running()

            self.containers[name].state = "exited"
            self.containers[name].code  = code
            self._send_event("die", name, Actor={"Attributes":{"exitCode": str(code)}})
            actual_code = self._run_loop(self._wait_task("wait", name, ignore_missing=False),
                    timeout=PERIOD)
            self.tc.assertEqual(actual_code, code)

        def event_destroy(name, *, code=None):
            """Simulate the removal of a container
            
            code is the expected exit code of the process
            (in case the wait task is still active (i.e. no terminated event
            were received))
            """

            self._ensure_tasks_running()

            del self.containers[name]
            self._send_event("destroy", name)
            actual_code = self._run_loop(self._wait_task("wait", name, ignore_missing=True),
                    timeout=PERIOD)
            self.tc.assertEqual(actual_code, code)

        def event_refresh(*, nodes):
            """Trigger a refresh event (in the refresh thread)"""

            with mock.patch.object(self.client, "info", return_value=self._gen_info(nodes)) as m:
                self.refresh_future.set_result(None)
                self._run_loop(PERIOD)
                m.assert_called_once_with()
                
        def event_nop():
            """No operation

            to flush all pending operations
            """
            self._run_loop(PERIOD)

        #################################################
        #                                               #
        #  responses                                    #
        #                                               #
        #################################################
        def event_created(name, *, node=None, throw=None):
            """Expect the creation of a container
            
            This event is observed containers created with event_create(.., node=None)
            """
            actual_node_id = self._run_loop(self._wait_task("create", name), timeout=PERIOD, throw=throw)
            if throw is None:
                self.tc.assertEqual(actual_node_id, generic_id(node))

        def event_terminated(name, *, code=None):
            """Expect the termination of a container
            
            This event is observed when a container terminates and when this
            termination is not caused by a event_terminate() stimulus.
            """
            actual_code = self._run_loop(self._wait_task("wait", name, ignore_missing=False),
                    timeout=PERIOD)
            self.tc.assertEqual(actual_code, code)


        func = locals().get("event_" + event)
        if func is None:
            raise NotImplementedError("event %r" % event)

        def key(x):
            # sort the function params according to the function prototype
            try:
                return func.__code__.co_varnames.index(x), x
            except ValueError:
                return 999, x

        logging.info(colored("[STEP %2d]  %-10s %-10s%s%s", attrs=("bold",)),  
                next(self.step_counter), event, (k[0] if k else ""),
                "".join(" %r" % x for x in k[1:]),
                "".join(" %s=%r" % (x, kw[x]) for x in sorted(kw, key=key)))

        func(*k, **kw)

        logging.info("## step done ##")
        self.client._SharedSwarmClient__manager._dump_groups()
        
class SharedSwarmClientTestCase(unittest.TestCase):

    @contextlib.contextmanager
    def check_duration(self, expect):
        try:
            t0 = time.time()
            yield
        finally:
            t1 = time.time()
            self.assertAlmostEqual(t1-t0, expect, places=2)

    def new_swarm_client(self, **kw):
        with mock.patch.object(APIClient, "info", spec=["__call__"]) as m:
            client = APIClient(**kw)
            m.return_value = {
                'ServerVersion': 'swarm/1.2.6',
            }
            return client


    def test_config_ok(self):
        with temp_file("""---
            reserve:
              - match: [{name: foo}, {name: bar}]
                cpu:    "17 20%"
                mem:    "4M 10%"
              - match: [{name: "12_cpu"}]
                cpu:    "12"
              - match: [{name: "21%_cpu"}]
                cpu:    "21%"
              - match: [{name: "14k_mem"}]
                mem:    "14k"
              - match: [{name: "19m_mem"}]
                mem:    "19m"
              - match: [{name: "27g_mem"}]
                mem:    "27g"
              - match: [{name: "9t_mem"}]
                mem:    "9t"
              - match: [{name: "4.3%_mem"}]
                mem:    "4.3%"
        """) as tmp:
            with Scenario(self, tmp) as sc:
                def check(group, patterns, *, cpu=0, mem=0, cpu_percent=0, mem_percent=0):
                    self.assertListEqual([x.pattern for x in group.name_regexes], patterns)
                    self.assertEqual(group.cpu, cpu)
                    self.assertEqual(group.cpu_percent, cpu_percent)
                    self.assertEqual(group.mem, mem)
                    self.assertEqual(group.mem_percent, mem_percent)


                groups = sc.client._SharedSwarmClient__manager._groups
                check(groups[0], ["foo", "bar"], cpu=17, cpu_percent=0.20, mem=4*M, mem_percent=.10)
                check(groups[1], ["12_cpu"], cpu=12)
                check(groups[2], ["21%_cpu"], cpu_percent=.21)
                check(groups[3], ["14k_mem"], mem=14*K)
                check(groups[4], ["19m_mem"], mem=19*M)
                check(groups[5], ["27g_mem"], mem=27*G)
                check(groups[6], ["9t_mem"],  mem=9*T)
                check(groups[7], ["4.3%_mem"], mem_percent=.043)

    def test_config_error(self):
        def check_bad_resource_value(value, msg):
            with temp_file("""---
                reserve:
                  - match: [{name: "foo"}]
                    cpu:   %s
            """ % json.dumps(value)) as tmp:
                with self.assertRaisesRegex(config_reader.ConfigError, msg):
                    with Scenario(self, tmp):
                        raise NotReachable()

        check_bad_resource_value("1 2", r"\.reserve\[0\]\.cpu: multiple absolute values in '1 2'")
        check_bad_resource_value("1% 2%", r"\.reserve\[0\]\.cpu: multiple percent values in '1% 2%'")

    def test_config_warning(self):
            with temp_file("""---
                reserve:
                  - match: [{name: "hello", foo: "bar"}]
                    cpu:   "Hello world!"
            """) as tmp:
                with mock.patch("shared_swarm.log.warning") as m:
                    with Scenario(self, tmp):
                        m.assert_has_calls([
                                mock.call('ignored unknown resource spec %r', 'Hello'),
                                mock.call('ignored unknown resource spec %r', 'world!'),
                                mock.call('unused config key %s', '.reserve[0].match[0].foo'),
                                ])


    def test_scenario_basic(self):
        with Scenario(self, "test-config.yml") as sc:
            sc("refresh",   nodes={"n1":{}})
            sc("create",    "h1", cpu=1, mem=1*G)
            sc("created",   "h1", node="n1")
            sc("refresh",   nodes={"n1":{"mem":5*G}, "n2":{"cpu":8}})
            sc("create",    "n2", cpu=1, mem=1*G)
            sc("created",   "n2", node="n1")
            sc("terminate", "n2", code=42)
            sc("destroy",   "n2")
            sc("destroy",   "h1")


    def test_scenario_container_not_created(self):
        with Scenario(self, "test-config.yml") as sc:
            sc("refresh",   nodes={"n1":{}})
            with sc.client.request_slot("l1", cpu=1, mem=0):
                sc("create","l2", cpu=2, mem=1*G)
            sc("created","l2", node="n1")


    def test_scenario_third_party_container(self):
        with Scenario(self, "test-config.yml") as sc:
            sc("refresh",    nodes={"n1":{}})

            # group 1 (1 cpu)  group 2 (2 cpu)

            # l0 created in group 2 (normal case)
            sc("create",    "l0", cpu=1, mem=0, node="n1")
            sc("create",    "l1", cpu=2, mem=0)
            # -> l1 created when group 2 is freed
            sc("destroy",   "l0")
            sc("created",   "l1", node="n1")

            # l2 created in group 1
            # (abnormal case: group 2 is already saturated by l1)
            sc("create",    "l2", cpu=1, mem=0, node="n1")
            sc("create",    "n3", cpu=1, mem=0)
            # -> n3 created when group 1 is freed
            sc("destroy",   "l2")
            sc("created",   "n3", node="n1")


            # l5 created in group 0
            # (because all other groups are saturated)
            sc("create",    "h4", cpu=1, mem=0)
            sc("created",   "h4", node="n1")
            sc("destroy",   "h4")
            sc("create",    "l5", cpu=1, mem=0, node="n1")
            sc("create",    "h6", cpu=1, mem=0)


            # n7 n8 created in group2
            # (because all other groups are saturated)
            sc("create",    "n7", cpu=1, mem=0, node="n1")
            sc("create",    "n8", cpu=1, mem=0, node="n1")
            # -> n9 created when l1+n7+n8(+h6 which was queued) are destroyed (free cpu is negative)
            sc("create",    "n9", cpu=2, mem=0)
            sc("destroy",   "l1")
            sc("destroy",   "n7")
            sc("created",   "h6", node="n1")
            sc("destroy",   "n8")
            sc("destroy",   "h6")
            sc("created",   "n9", node="n1")


            # n10 created in group 2 (oversized)
            sc("create",    "n10", cpu=27, mem=0, node="n1")
            sc("create",    "l11", cpu=1,  mem=0)
            sc("destroy",   "n9")
            sc("destroy",   "n10")
            sc("created",   "l11", node="n1")


    def test_scenario_slot_too_big(self):
        with Scenario(self, "test-config.yml") as sc:
            sc("refresh",    nodes={"n1":{}})

            # group 1 (1 cpu)  group 2 (2 cpu)

            sc("create",    "n0", cpu=3, mem=0)
            sc("created",   "n0", throw=shared_swarm.SlotTooBig)
            sc("create",    "n1", cpu=0, mem=2.1*G)
            sc("created",   "n1", throw=shared_swarm.SlotTooBig)
            sc("create",    "n2", cpu=2, mem=2*G)
            sc("created",   "n2", node="n1")


    def test_scenario_refresh_job_created(self):
        with Scenario(self, "test-config.yml") as sc:
            sc("refresh",   nodes={"n1":{}})

            sc("create",    "n0", cpu=1, mem=1*G)
            sc("created",   "n0", node="n1")

            # container n1 created but notification not received
            sc.containers["n1"] = ContainerState(node="n1", cpu=2, mem=2*G, state="running")
            sc("refresh",   nodes={"n1":{}})
    
            # n2 not created because all resources taken by n1
            sc("create",    "n2", cpu=1, mem=1*G) 

            sc("destroy",   "n1")
            sc("created",   "n2", node="n1") 

    def test_scenario_refresh_job_terminated(self):
        with Scenario(self, "test-config.yml") as sc:
            sc("refresh",   nodes={"n1":{}})

            sc("create",    "n0", cpu=1, mem=1*G)
            sc("created",   "n0", node="n1")

            # container n1 destroyed but notification not received
            sc.containers["n0"].state = "exited"
            sc.containers["n0"].code  = 43
            sc("refresh",   nodes={"n1":{}})
            sc("terminated","n0", code=43)
            sc("destroy",   "n0") 

    def test_scenario_refresh_job_destroyed(self):
        with Scenario(self, "test-config.yml") as sc:
            sc("refresh",   nodes={"n1":{}})

            sc("create",    "n0", cpu=1, mem=1*G)
            sc("created",   "n0", node="n1")
            sc("create",    "n1", cpu=2, mem=2*G)
            sc("created",   "n1", node="n1")

            # n2 not created because no resource available
            sc("create",    "n2", cpu=1, mem=1*G) 

            # container n1 destroyed but notification not received
            del sc.containers["n1"]
            sc("refresh",   nodes={"n1":{}})
            sc("terminated","n1")
            sc("created",   "n2", node="n1") 


    def test_scenario_double_allocation_request(self):
        with Scenario(self, "test-config.yml") as sc:
            sc("refresh",   nodes={"n1":{}})

            sc("create",    "l0", cpu=2, mem=2*G, node="n1")
            sc("create",    "l1", cpu=1, mem=1*G)
            sc("nop")
            with self.assertRaisesRegex(shared_swarm.AlreadyRequested, "l1"):
                with sc.client.request_slot("l1", cpu=1, mem=1*G):
                    raise ValueError()


    @contextlib.contextmanager
    def scenario_node_n2_fail(self):
        with Scenario(self, "test-config.yml") as sc:
            sc("refresh",   nodes={"n1":{}, "n2":{}})

            sc("create",    "l0", cpu=2, mem=2*G)
            sc("created",   "l0", node="n2")
            sc("create",    "l1", cpu=2, mem=2*G)
            sc("created",   "l1", node="n2")
            sc("create",    "l2", cpu=1, mem=1*G)
            sc("create",    "l3", cpu=1, mem=1*G)
            sc("create",    "l4", cpu=1, mem=1*G)
            sc("create",    "l5", cpu=2, mem=2*G)
            sc("create",    "l6", cpu=1, mem=1*G)
            sc("create",    "l7", cpu=1, mem=1*G)

            # node n2 displays an error
            for name in ([name for name, ctr in sc.containers.items() if ctr.node==generic_id("n2")]):
                del sc.containers[name]
            yield sc

            sc("terminated","l0")
            sc("terminated","l1")
            sc("created",   "l2", node="n1")
            sc("created",   "l3", node="n1")

            # node n2 goes back online
            sc("refresh",   nodes={"n1":{}, "n2": {}})
            sc("created",   "l4", node="n2")
            sc("created",   "l5", node="n2")
            sc("created",   "l6", node="n2")

            # l7 not created when l2/l3 terminate
            # (because they use a GroupSlot now allocated to another group)
            sc("destroy",   "l2")
            sc("destroy",   "l3")

            sc("destroy",   "l6")
            sc("created",   "l7", node="n2")


    def test_scenario_node_down(self):
        with self.scenario_node_n2_fail() as sc:
            sc("refresh",   nodes={"n1":{}})

    def test_scenario_node_unhealthy(self):
        with self.scenario_node_n2_fail() as sc:
            sc("refresh",   nodes={"n1":{}, "n2": {"healthy": False}})

    def test_scenario_node_error(self):
        with self.scenario_node_n2_fail() as sc:
            sc("refresh",   nodes={"n1":{}, "n2": {"error": "boom"}})

    def test_scenario_node_parse_warning(self):
        with self.scenario_node_n2_fail() as sc:
            with mock.patch("shared_swarm.log.warning") as m:
                sc("refresh",   nodes={"n1":{"append": [("blah", "blah")]}})
                m.assert_called_with('failed to parse swarm info SystemStatus entry: %r', 'blah')

    def test_scenario_node_parse_exception(self):
        with Scenario(self, "test-config.yml") as sc:
            with mock.patch("shared_swarm.log.exception") as m:
                sc("refresh",   nodes={"n1":{}, "n2":{"append": ["blah blah"]}})
                m.assert_called_with('docker refresh thread exception')

    def test_scenario_unallocated_resources_warning(self):
        with Scenario(self, "test-config.yml") as sc:
            with mock.patch("shared_swarm.log.warning") as m:
                sc("refresh",   nodes={"n1":{"cpu":4, "mem": 1*G}, "n2":{"cpu":5, "mem":1*G}})
                m.assert_called_with('unallocated ressources: %s', 'n1(cpu: 2.0  mem: 0.0 ) n2(cpu: 3.0  mem: 0.0 )')

    def test_scenario_forced_allocation_failed_warning(self):
        with Scenario(self, "test-config.yml") as sc:
            with mock.patch("shared_swarm.log.warning") as m:
                sc("refresh",   nodes={"n1":{}, "n2":{"cpu": 0}})
                sc("create",    "l0", cpu="1", mem=1*G, node="n2")
                m.assert_called_with('forced slot allocated failed for name=%r(cpu=%s, mem=%s, node=%r)', 'l0', '1', ' 1.0G', 'n2')

    def test_scenario_forced_allocation_unknown_node_warning(self):
        with Scenario(self, "test-config.yml") as sc:
            with mock.patch("shared_swarm.log.warning") as m:
                sc("refresh",   nodes={"n1":{}})
                sc("create",    "l0", cpu=1, mem=1*G, node="n2")
                m.assert_called_with('ignored container creation %r on unknown node id %r', 'l0', '<<ID n2>>')

    def test_scenario_wait_slot_called_twice(self):
        with Scenario(self, "test-config.yml") as sc:
            with mock.patch("shared_swarm.log.warning") as m:
                sc("refresh",   nodes={"n1":{}})
                sc("create",    "h0", cpu=4, mem=1*G, node="n1")
                sc("create",    "l0", cpu=1, mem=1*G)
                self.assertRaisesRegex(RuntimeError, "slot 'l0' has an active waiter",
                        sc.loop.run_until_complete, sc.client.wait_slot("l0"))

    def test_scenario_wait_called_twice(self):
        with Scenario(self, "test-config.yml") as sc:
            with mock.patch("shared_swarm.log.warning") as m:
                sc("refresh",   nodes={"n1":{}})
                sc("create",    "h0", cpu=1, mem=1*G)
                sc("created",   "h0", node="n1")
                self.assertRaisesRegex(RuntimeError, "there is already a watch for this container",
                        sc.loop.run_until_complete, sc.client.wait_async(generic_id("h0")))

    def test_scenario_wait_shutdown(self):
        with Scenario(self, "test-config.yml") as sc:
            with mock.patch("shared_swarm.log.warning") as m:
                sc("refresh",   nodes={"n1":{}})
                sc("create",    "h0", cpu=1, mem=1*G, node="n1")
                sc.client.shutdown()
                self.assertRaises(shared_swarm.ShuttingDown,
                        sc.loop.run_until_complete, sc.client.wait_async(generic_id("h0")))

    def test_scenario_wait_after_die(self):
        with Scenario(self, "test-config.yml") as sc:
            with mock.patch("shared_swarm.log.warning") as m:
                sc("refresh",   nodes={"n1":{}})

                sc("create",    "h0", cpu=1, mem=1*G)
                sc("created",   "h0", node="n1")
                sc("terminate", "h0", code=44)

                # recreate the wait task
                sc._create_task("wait", "h0", sc.client.wait_async(generic_id("h0")))
                # -> must return immediately
                sc("terminated","h0", code=44)



    def test_scenario_delayed_allocation_already_removed(self):
        with Scenario(self, "test-config.yml") as sc:
            sc("refresh",   nodes={"n1":{}})
            with mock.patch.object(sc.client, "inspect_container",
                    lambda ctr: raise_docker_NotFound()) as m:
                sc("create",    "h0", cpu=1, mem=1*G, node="n1")
            sc("destroy",    "h0")


    def test_scenario_sleep(self):
        with Scenario(self, "test-config.yml") as sc:
            _sleep = sc.client._SharedSwarmClient__manager._sleep

            # normal case (sleep)
            with self.check_duration(.042):
                _sleep(.042)

            # shutdown
            th=threading.Thread(target=lambda: (time.sleep(.17), sc.client.shutdown()))
            try:
                th.start()
                with self.check_duration(.17), self.assertRaises(shared_swarm.ShuttingDown):
                    _sleep(.68)
            finally:
                th.join()

    def test_scenario_event_thread_exception(self):
        with Scenario(self, "test-config.yml") as sc:
            with mock.patch("shared_swarm.log.exception") as m:
                sc.event_queue.put(ValueError("blah"))
                time.sleep(PERIOD)
                m.assert_called_with('docker event monitor exception')

    def test_scenario_recompute_group_slots(self):
        with Scenario(self, "test-config.yml") as sc:
            sc("refresh",   nodes={"n1":{}})
            sc("create",    "h0", cpu=1, mem=1*G, node="n1")
            sc.client._SharedSwarmClient__manager._groups[0].slots[0].cpu_free += 42
            with mock.patch("shared_swarm.log.info") as m:
                sc("refresh",   nodes={"n1":{}})
                m.assert_any_call("%s", "cpu/mem inconsistency in slot for group 0 node 'n1'")
                m.assert_any_call("recomputing cpu/mem free for all group slots")

# 
# scenarios
#
# stimuli
#   create  test-low-1  cpu:1   mem:1G
#   create  test-low-2  cpu:2   mem:2G  indep
#   refresh_begin
#   refresh_end
#   
#
# responses
#   created test-low-1   cpu:1   mem:1G
#
#

#   with scenario.refresh({
#               "test-1": {cpu:1, mem:1G},
#           }):
#       scenario.expect_terminated("test-2")
#       scenario.expect_destroyed("test-2")
#
#   scenario.create("test-1", cpu="1", mem="1G", direct=True)
#   scenario.created("test-1")
#   scenario.terminate("test-1")
#   scenario.destroy("test-1")


class UtilsTestCase(unittest.TestCase):

    def test_rate_limit(self):

        def assert_elapsed(expected, func, *k):
            t0 = time.monotonic()
            func(*k)
            t1 = time.monotonic()
            self.assertAlmostEqual(t1-t0, expected, 2)


        limiter=shared_swarm.rate_limit(0.1)

        assert_elapsed(0,   next, limiter)
        assert_elapsed(0.1, next, limiter)
        time.sleep(.02)
        assert_elapsed(0.08, next, limiter)
        time.sleep(.1)
        assert_elapsed(0,    next, limiter)
        time.sleep(.07)
        assert_elapsed(0.03, next, limiter)
        time.sleep(.12)
        assert_elapsed(0,    next, limiter)


    def test_fmt_value(self):
        fmt_value = shared_swarm.fmt_value

        self.assertEqual(" 0.0 ", fmt_value(0))
        self.assertEqual(" 1.0 ", fmt_value(1))
        self.assertEqual("1.71 ", fmt_value(1.712))
        self.assertEqual(" 999 ", fmt_value(999))
        self.assertEqual("1023 ", fmt_value(1023))
        self.assertEqual(" 1.0K", fmt_value(1024))
        self.assertEqual("85.4K", fmt_value(87451))
        self.assertEqual(" 964K", fmt_value(987654))
        self.assertEqual("1023K", fmt_value(1024**2-1))
        self.assertEqual(" 1.0M", fmt_value(1024**2))
        self.assertEqual("1023M", fmt_value(1024**3-1))
        self.assertEqual(" 1.0G", fmt_value(1024**3))
        self.assertEqual("1023G", fmt_value(1024**4-1))
        self.assertEqual(" 1.0T", fmt_value(1024**4))
        self.assertEqual("1023T", fmt_value(1024**5-1))
        self.assertEqual("1024T", fmt_value(1024**5))
        self.assertEqual("2e+16", fmt_value(1024**5 * 14))

        self.assertEqual("-1.0 ", fmt_value(-1))
        self.assertEqual("-1.7 ", fmt_value(-1.712))
        self.assertEqual("-0.9K", fmt_value(-999))
        self.assertEqual("-0.9K", fmt_value(-1023))
        self.assertEqual("-1.0K", fmt_value(-1024))
        self.assertEqual(" -85K", fmt_value(-87451))
        self.assertEqual("-0.1M", fmt_value(-123000))
        self.assertEqual("-0.9M", fmt_value(-987654))
        self.assertEqual("-1.0M", fmt_value(-1024**2-1))
        self.assertEqual("-1e+15", fmt_value(-1024**5))
        self.assertEqual("-2e+16", fmt_value(-1024**5 * 14))

    def test_init_repr_mixin(self):
        class A(shared_swarm._InitReprMixin):
            __slots__ = "foo", "bar"

        self.assertEquals(repr(A()),            "A(foo=None,bar=None)")
        self.assertEquals(repr(A(foo=21)),      "A(foo=21,bar=None)")
        self.assertEquals(repr(A(bar=42.42)),   "A(foo=None,bar=42.42)")
        self.assertEquals(repr(A(foo=0, bar=1)),"A(foo=0,bar=1)")
        self.assertRaises(TypeError, A, 1)
        self.assertRaisesRegex(AssertionError, "unknown attribute", lambda: A(foo2="bar"))


    def test_start_thread(self):
        # normal case
        args = 1, 38
        kwargs = dict(foo="bar", hello="world")
        end = threading.Event()
        def func_ok(*k, ready, **kw):
            self.assertEqual(k, args)
            self.assertEqual(kw, kwargs)
            ready.set()
            end.wait()

        th = shared_swarm._start_thread(func_ok, args=args, kwargs=kwargs)
        self.assertIsInstance(th, threading.Thread)
        time.sleep(PERIOD)
        self.assertTrue(th.is_alive())
        end.set()
        th.join()

        # exception during initialisation
        def func_bad(*k, ready, **kw):
            raise ValueError("INIT")
        with mock.patch("shared_swarm.log.exception") as m:
            self.assertRaisesRegex(ValueError, "INIT", shared_swarm._start_thread, func_bad)
            self.assertTrue(m.called)

        # finished without setting event
        th = shared_swarm._start_thread(lambda ready: None)
        self.assertIsInstance(th, threading.Thread)
        self.assertFalse(th.is_alive())




import logging
logging.basicConfig(level="DEBUG", format='%(relativeCreated)5dms  %(levelname)-8s %(name)s:%(message)s')
