import  json
import  queue
import  unittest
from    unittest    import mock

import  docker

import swarm_abstraction
from   swarm_abstraction import APIClient

_LOCAL_ID   = '0000:0000:0000:0000:0000:0000:0000:0000:0000:0000:0000:0000'
_LOCAL_NAME = "local"

def fpatch(*k, **kw):
    """mock patch a single function"""
    kw.setdefault("spec", ("__call__",))
    return mock.patch(*k, **kw)


class SwarmAbstractionClientTestCase(unittest.TestCase):

    def new_docker_client(self, ncpus=8, *, expect_cpu_shares_multiple=None, **kw):
        with mock.patch("docker.APIClient.info", spec=["__call__"]) as m:
            client = APIClient(**kw)
            m.return_value = {
                'ServerVersion': '1.13.0',
                'NCPU': ncpus,
            }
            if expect_cpu_shares_multiple is None:
                client._SwarmAbstractionClient__cpu_shares_multiple
            else:
                # test the discovery of the docker engine
                self.assertEqual(client._SwarmAbstractionClient__cpu_shares_multiple, expect_cpu_shares_multiple)
                self.assertTrue(m.called)

                # ensure it is a lazy discovery
                m.reset_mock()
                self.assertEqual(client._SwarmAbstractionClient__cpu_shares_multiple, expect_cpu_shares_multiple)
                self.assertFalse(m.called)
            return client

    def new_swarm_client(self, *, expect_cpu_shares_multiple=None, **kw):
        with mock.patch("docker.APIClient.info", spec=["__call__"]) as m:
            client = APIClient(**kw)
            m.return_value = {
                'ServerVersion': 'swarm/1.2.6',
            }

            if expect_cpu_shares_multiple is None:
                client._SwarmAbstractionClient__cpu_shares_multiple
            else:
                # test the discovery of the swarm engine
                self.assertEqual(client._SwarmAbstractionClient__cpu_shares_multiple, expect_cpu_shares_multiple)
                self.assertTrue(m.called)

                # ensure it is a lazy discovery
                m.reset_mock()
                self.assertEqual(client._SwarmAbstractionClient__cpu_shares_multiple, expect_cpu_shares_multiple)
                self.assertFalse(m.called)
            return client


    def test_swarm_discovery(self):

        client = self.new_docker_client(8, expect_cpu_shares_multiple=1024//8)
        self.new_docker_client(4, expect_cpu_shares_multiple=1024//4)

        self.new_swarm_client(expect_cpu_shares_multiple=1)
        self.new_swarm_client(expect_cpu_shares_multiple=1)

        self.assertRaisesRegex(AttributeError, "has no attribute 'INVALID'",
                lambda: client.INVALID)


    def test_containers(self):
        with fpatch("docker.APIClient.containers") as m:
            # docker engine
            client = self.new_docker_client(4)

            m.return_value = [  {"Id": "ID-FOO", "Names": ["/foo"]},
                                {"Id": "ID-BAR", "Names": ["/bar", "/foo/barlink"]}]

            self.assertListEqual(client.containers(), [
                                {"Id": "ID-FOO", "Names": ["/local/foo"]},
                                {"Id": "ID-BAR", "Names": ["/local/bar", "/local/foo/barlink"]}])

            # swarm engine
            client = self.new_swarm_client()

            m.return_value = [  {"Id": "ID-FOO", "Names": ["/node1/foo"]},
                                {"Id": "ID-BAR", "Names": ["/node2/bar", "/node1/foo/barlink"]}]
            self.assertListEqual(client.containers(), m.return_value)



    def test_create_container(self):
        with fpatch("docker.APIClient.create_container") as m:
            # docker engine
            client = self.new_docker_client(4)

            #   CpuShares normalisation
            m.reset_mock()
            client.create_container(  "test", host_config={"foo": "bar"})
            m.assert_called_once_with("test", host_config={"foo": "bar"})

            m.reset_mock()
            client.create_container(  "test", host_config={"CpuShares": 0, "foo": "bar"})
            m.assert_called_once_with("test", host_config={"CpuShares": 0, "foo": "bar"})

            m.reset_mock()
            client.create_container(  "test", host_config={"CpuShares": 3,   "foo": "bar"})
            m.assert_called_once_with("test", host_config={"CpuShares": 768, "foo": "bar"})

            #   remove node constraint (or raise ValueError)
            m.reset_mock()
            client.create_container(  "test", environment=["foo=bar", "constraint:node==0000:0000:0000:0000:0000:0000:0000:0000:0000:0000:0000:0000", "hello=world"])
            m.assert_called_once_with("test", environment=["foo=bar", "hello=world"],)

            m.reset_mock()
            client.create_container(  "test", environment={"foo": "bar", "hello": "world"})
            m.assert_called_once_with("test", environment={"foo": "bar", "hello": "world"})

            self.assertRaises(ValueError, client.create_container, "test", environment=["foo=bar", "constraint:node==GKDG:DG7B:DAEA:X2GT:EOIL:MH5T:LYXD:BAIX:3EXH:Z77N:UUKI:QYDM", "hello=world"])

            # swarm engine
            client = self.new_swarm_client()

            #   CpuShares normalisation
            m.reset_mock()
            client.create_container(  "test", host_config={"foo": "bar"})
            m.assert_called_once_with("test", host_config={"foo": "bar"})

            m.reset_mock()
            client.create_container(  "test", host_config={"CpuShares": 0, "foo": "bar"})
            m.assert_called_once_with("test", host_config={"CpuShares": 0, "foo": "bar"})

            m.reset_mock()
            client.create_container(  "test", host_config={"CpuShares": 3, "foo": "bar"})
            m.assert_called_once_with("test", host_config={"CpuShares": 3, "foo": "bar"})

            #   Ignore node constraint
            m.reset_mock()
            client.create_container(  "test", environment=["foo=bar", "constraint:node==0000:0000:0000:0000:0000:0000:0000:0000:0000:0000:0000:0000", "hello=world"])
            m.assert_called_once_with("test", environment=["foo=bar", "constraint:node==0000:0000:0000:0000:0000:0000:0000:0000:0000:0000:0000:0000", "hello=world"])


    def test_info(self):
        with fpatch("docker.APIClient.info") as m:
            # docker engine
            client = self.new_docker_client(4)
            m.return_value = {"NCPU": 4, "MemTotal": 1024**2}
            self.assertDictEqual(client.info(), {
                "NCPU": 4, "MemTotal": 1024**2,
                'SystemStatus': [
                    [' local', '127.0.0.1:0'],
                    ['  └ ID', '0000:0000:0000:0000:0000:0000:0000:0000:0000:0000:0000:0000'],
                    ['  └ Status', 'Healthy'],
                    ['  └ Reserved CPUs', '0 / 4'],
                    ['  └ Reserved Memory', '0 KiB / 1024 KiB']]})

            client = self.new_docker_client(8)
            m.return_value = {"NCPU": 8, "MemTotal": 42*1024, "SystemStatus": [["foo", "bar"]]}
            self.assertDictEqual(client.info(), {
                "NCPU": 8, "MemTotal": 42*1024,
                'SystemStatus': [["foo", "bar"],
                    [' local', '127.0.0.1:0'],
                    ['  └ ID', '0000:0000:0000:0000:0000:0000:0000:0000:0000:0000:0000:0000'],
                    ['  └ Status', 'Healthy'],
                    ['  └ Reserved CPUs', '0 / 8'],
                    ['  └ Reserved Memory', '0 KiB / 42 KiB']]})

            # swarm engine
            client = self.new_swarm_client()
            m.return_value = {"foo": "bar"}
            self.assertDictEqual(client.info(), m.return_value)



    @unittest.skipIf(not hasattr(docker.APIClient, "update_container"), "not supported in docker-py")
    def test_update_container(self):
        with fpatch("docker.APIClient.update_container") as m:
            # docker engine
            client = self.new_docker_client(4)

            m.reset_mock()
            client.update_container(  "test", foo="bar")
            m.assert_called_once_with("test", foo="bar", cpu_shares=None)

            m.reset_mock()
            client.update_container(  "test", cpu_shares=0, foo="bar")
            m.assert_called_once_with("test", cpu_shares=0, foo="bar")

            m.reset_mock()
            client.update_container(  "test", cpu_shares=3,   foo="bar")
            m.assert_called_once_with("test", cpu_shares=768, foo="bar")


            # swarm engine
            client = self.new_swarm_client()

            m.reset_mock()
            client.update_container(  "test", foo="bar")
            m.assert_called_once_with("test", foo="bar", cpu_shares=None)

            m.reset_mock()
            client.update_container(  "test", cpu_shares=0, foo="bar")
            m.assert_called_once_with("test", cpu_shares=0, foo="bar")

            m.reset_mock()
            client.update_container(  "test", cpu_shares=27,   foo="bar")
            m.assert_called_once_with("test", cpu_shares=27, foo="bar")


    def test_inspect_container(self):
        with fpatch("docker.APIClient.inspect_container") as m:
            # docker engine
            client = self.new_docker_client(8)

            m.return_value=  { "Id": "ID", "HostConfig" : { "CpuShares": 0, "foo": "bar"}}
            self.assertEqual({ "Id": "ID", "HostConfig" : { "CpuShares": 0, "foo": "bar"},
                "Node": {"ID": _LOCAL_ID}}, client.inspect_container("test"))

            m.return_value=  { "Id": "ID", "HostConfig" : { "CpuShares": 256, "foo": "bar"}}
            self.assertDictEqual({ "Id": "ID", "HostConfig" : { "CpuShares": 2, "foo": "bar"},
                "Node": {"ID": _LOCAL_ID}}, client.inspect_container("test"))

            # swarm engine
            client = self.new_swarm_client()

            m.return_value=  { "Id": "ID", "HostConfig" : { "CpuShares": 0, "foo": "bar"}, "Node": {"Cpus": 8}}
            self.assertEqual({ "Id": "ID", "HostConfig" : { "CpuShares": 0, "foo": "bar"}, "Node": {"Cpus": 8}},
                client.inspect_container("test"))

            m.return_value=  { "Id": "ID", "HostConfig" : { "CpuShares": 384, "foo": "bar"}, "Node": {"Cpus": 8}}
            self.assertEqual({ "Id": "ID", "HostConfig" : { "CpuShares":   3, "foo": "bar"}, "Node": {"Cpus": 8}},
                client.inspect_container("test"))

