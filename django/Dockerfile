FROM allgo/base-debian

# configure stretch backports
#COPY setup/backports/. /

RUN apt-getq update && apt-getq install	\
  default-libmysqlclient-dev	\
  default-mysql-client		\
  gcc			\
  gunicorn3		\
  nginx-light		\
  python3-aiohttp	\
  python3-aioredis	\
  python3-cryptography	\
  python3-dev		\
  python3-django	\
  python3-django-allauth	\
  python3-django-extensions	\
  python3-django-jsonfield	\
  python3-django-taggit	\
  python3-djangorestframework	\
  python3-ipy		\
  python3-iso8601	\
  python3-jwt		\
  python3-misaka	\
  python3-mysqldb	\
  python3-natsort	\
  python3-pip		\
  python3-pydotplus	\
  python3-pylint-django	\
  python3-redis		\
  python3-robot-detection	\
  python3-sqlparse	\
  python3-wheel		\
  supervisor		\
  zip

COPY requirements.txt /tmp/
RUN cd /tmp && pip3 install -r requirements.txt && rm requirements.txt

COPY	. /opt/allgo
RUN	sh /opt/allgo/setup/setup.sh

USER	allgo
WORKDIR /opt/allgo
LABEL   dk.migrate_always=1
ENV PYTHONUNBUFFERED 1
ENV PYLINTHOME /opt/allgo_metrics

# NOTE: we use SIGINT instead of SIGTERM because the django server does not
# catch SIGTERM (while gunicorn catches both SIGTERM & SIGINT)
STOPSIGNAL SIGINT
CMD ["run-allgo"]

HEALTHCHECK CMD healthcheck
