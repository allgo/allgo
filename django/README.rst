Allgo
=====

`Allgo`_ is a platform for building deploying apps that analyze massive data in
Linux containers, it has been specifically designed for use in scientific
applications. This documentation is related to its front-end. Please see the
:ref:`installation` to get started.

Features
--------

The current version aims to reproduce the basic features offered by the rails
front-end such as:

- authentication
- documentation of the API
- creation of an app
- launch of a processing job

Installation
------------

Please refer to :ref:`installation` in the `docs` directory for a detailed
explaination of the setup.

License
-------

This work is distributed under the terms of the AGPLv3 license.

Logo
----

The logo is a SVG file generated using Inkscape. It contains two examples in
white or black. In order to edit it without any issues, it requires the Orbitron
fonts available on `Google Fonts website`_

.. _Allgo: https://allgo.inria.fr/
.. _Google Fonts website: https://fonts.google.com/specimen/Orbitron
