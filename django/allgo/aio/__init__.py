#!/usr/bin/env python3
#
# This module implement an auxiliary HTTP server for serving asynchronous
# requests for allgo.
#
# There are two purposes:
# - implement server push (using long-lived HTTP requests) for:
#     - sending status updates for the jobs and sandboxes
#     - live-streaming of the job logs
# - have a really async implementation for pushing image manifests into
#   the registry (the preliminary implementation in
#   5451a6dfdb10a2d0875179d06b43c947ebcd37b4 was blocking)
#
# It is implemented with aiohttp.web (a lighweight HTTP framework,
# similar to what we can do with flask but asynchronously).
#
# The alternative would have been to use the django channels plugin, but:
# - it went through a major refactoring (v2) recently
# - it requires replacing unicorn with an ASGI server (daphne)
# - django-channels and daphne are not yet debian, and for the HTTP server
#   i would prefer an implementation for which we have stable security
#   updates
#
# (anyway this can be ported to django-channels later)
#
# The nginx config redirects the /aio/ path to this server (and the image
# manifests pushes too).
#
# The allgo.aio server interacts only with the nginx, reddis and django
# containers. It relies totally on the django server for authenticating
# the user and for accessing the mysql db (so there is no ORM).
#
# NOTE: in this design the django server has to trust blindly the requests
# coming from the allgo.aio server (for some endpoints). To prevent
# security issues, the nginx configuration is modified to set the header
# 'X-Origin: nginx'. Thus django knowns who he can trust.


import contextlib
import http
import itertools
import json
import logging
import os
import re
import signal
import ssl
import sys
import time
import weakref

import asyncio
import aiohttp
import aiohttp.web
from   aiohttp.web  import Response, StreamResponse
import aioredis

import config.env




# TODO: return error pages with some content


##################################################
# redis keys

# job log
REDIS_KEY_JOB_LOG       = "log:job:%d"
REDIS_KEY_JOB_STATE     = "state:job:%d"
REDIS_KEY_JOB_RESULT    = "result:job:%d"


# pubsub channels for waking up allgo.aio (frontend) and the controller
# (ourselves)
REDIS_CHANNEL_AIO        = "notify:aio"
REDIS_CHANNEL_CONTROLLER = "notify:controller"

# pubsub messages
REDIS_MESSAGE_JOB_UPDATED    = "job:%d"
REDIS_MESSAGE_WEBAPP_UPDATED = "webapp:%d"

##################################################


log = logging.getLogger("allgo-aio")

def log_future_exception(fut: asyncio.Future):
    @fut.add_done_callback
    def cb(fut):
        if not fut.cancelled() and (e := fut.exception()) is not None:
            log.error("unhandled exception %s in %s", e.__class__.__name__, fut)

def daemon_startup(fork: bool, pidfile: str):
    """Demonise the process

    - fork the process if 'fork' is true and wait until any of:
      - SIGUSR1 is received (daemon successfully started)
      - child terminated (daemon prematurely terminated)
    - write the pid into 'pidfile'

    NOTE: the child process must call `daemon_ready()` after initialisation
    """
    def write_pidfile(pid):
        with open(pidfile, "w") as fp:
            fp.write("%d\n" % pid)
    if fork:
        signal.signal(signal.SIGUSR1, lambda *k: sys.exit(0))
        pid = os.fork()
        if pid:
            write_pidfile(pid)
            code = os.waitstatus_to_exitcode(os.waitpid(pid, 0)[1])
            log.error("daemon terminated prematurely with exit code %d", code)
            sys.exit(1)
        else:
            signal.signal(signal.SIGUSR1, signal.SIG_DFL)
    else:
        write_pidfile(os.getpid())

def daemon_ready(fork: bool):
    """Signal the parent process that the daemon is ready"""
    if fork:
        # send SIGUSR1 to parent process
        os.kill(os.getppid(), signal.SIGUSR1)


def try_set_result(fut: asyncio.Future, result):
    """set a asyncio.Future result

    but do not raise an error if the result is already set
    """
    if fut.done():
        return False
    else:
        fut.set_result(result)
        return True

def prepare_headers(request: aiohttp.web.Request) -> dict:
    """Prepare the headers to be forwarded to django

    This function reuses the headers from the incoming aiohttp.web request and
    that are related to:
    - authentication (Authorization, Cookie)
    - reverse-proxy (X-Forwarded-*)

    Also it sets "X-Origin: aio" to make clear that this request comes from
    django.
    """
    headers = {"X-Origin": "aio"}
    for key in (
            "Authorization",
            "Cookie",
            "User_agent",
            "X-Forwarded-For",
            "X-Forwarded-Host",
            "X-Forwarded-Proto"):
        val = request.headers.get(key)
        if val is not None:
            headers[key] = val
    return headers

def is_ok(response: aiohttp.ClientResponse):
    """Return true if this HTTP response is successful"""
    return 200 <= response.status < 300

class RateLimiter:
    """Generator for rate limiting

    This asynchronous iterator ensures we spend at least `period` seconds in
    each iteration.

    usage:
        async for _ in RateLimiter(60):
            ...
    """
    def __init__(self, period):
        assert period > 0
        self.period = period
        self._t0 = time.monotonic()-period

    def __aiter__(self):
        return self

    async def __anext__(self):
        t1 = time.monotonic()
        delay = self._t0 - t1 + self.period
        if delay > 0:
            log.debug("rate_limit: sleep %f seconds", delay)
            await asyncio.sleep(delay)
            self._t0 = t1 + delay
        else:
            self._t0 = t1

class JsonSeqStreamResponse(StreamResponse):
    """aiohttp response class for streaming json objects

    The stream is line-delimited, i.e: a newline <LF> character is inserted between each JSON
    object (and the objects are guaranteed not to contain any newline)

    usage:
        resp = JsonSeqStreamResponse()
        await resp.prepare(request)

        ...
        resp.send(json_obj)
        await resp.drain()

    """
    def __init__(self, *k, **kw):
        super().__init__(*k, **kw)
        self.content_type = "application/json"
        self.charset = "utf-8"
        self.__lock = asyncio.Lock()

    async def prepare(self, request):
        await super().prepare(request)

        # send periodic nop messages (empty dict) to keep the connection alive
        #
        # It seems that firefox systematically closes the connection after 60
        # seconds of inactivity (and i do not now how to configure it),
        # therefore i set the interval to 50 seconds, this will also prevent
        # NAT firewalls from closing the connection.
        KEEPALIVE_INTERVAL = 50

        async def send_keepalives():
            try:
                while True:
                    await asyncio.sleep(KEEPALIVE_INTERVAL)
                    async with self.__lock:
                        await self.write(b"{}\n")
            except RuntimeError:
                pass # write() after write_eof()
        log_future_exception(asyncio.ensure_future(send_keepalives()))

    async def send(self, json_object):
        async with self.__lock:
            await self.write(json.dumps(json_object).encode() + b"\n")


async def forward_response(reply: aiohttp.ClientResponse) -> aiohttp.web.Response:
    """Forward a HTTP response to the client

    This is used for forwarding django's HTTP responses to the client we are
    serving.
    """
    return Response(status=reply.status, body=await reply.read(), headers=(
        (key, val) for key, val in reply.headers.items()
        if key.lower() != "content-encoding"))


class ErrorResponse(Response):
    """Simple response classes for reporting errors

    eg: `ErrorResponse(status=404)` is equivalent to:
        `Response(status=404, text="404: Not Found")`
    """
    def __init__(self, status):
        super().__init__(status=status,
                text="%d: %s" % (status, http.HTTPStatus(status).phrase))


class StatesDict(weakref.WeakValueDictionary):
    """Dictionary for caching job/app states and handling synchronisation

    This dict & conditions are used for caching redis data and routing the
    redis notifications to the relevant HTTP request handlers.

    The dict key identifies a resource (eg: job id) and the dict value is an
    object initialised with multiple attributes:
      - a 'cond' attribute which is the asyncio.Condition for reporting updates
        to this resource
      - the attributes listed at dict creation, and initialised with None

    Entries are created on the fly if the key is not found in the dict.

    This dictionary is a weakref dict, so that if its entries are destroyed
    when they are no longer used (i.e. nobody is listening to them).
    """

    class State:
        pass

    def __init__(self, attrs=()):
        super().__init__()
        self._attrs = attrs

    def __getitem__(self, key):
        item = self.get(key)
        if item is None:
            item = self[key] = self.State()
            item.cond = asyncio.Condition()
            for name in self._attrs:
                setattr(item, name, None)
        return item


class AllgoAio:
    def __init__(self, bind, loop):
        self.loop = loop
        self.app = aiohttp.web.Application(loop=loop)

        self.django_url  = "http://127.0.0.1:8000"

        # aiohttp client for making request to django/registry
        self.http_client = None

        # ssl client context for the http client
        self.ssl_context = None

        # aiohttp server task
        self.server     = None

        # global redis client for querying data from the reddis server
        # (not used by the notification task which has its own reddis connection)
        self.redis_pool = None

        # state dicts for job & webapp notifications
        #
        # key is the job_id/webapp_id
        self.webapp_states = StatesDict()
        self.job_states = StatesDict(("state", "result"))

        # mutex to be locked when adding new job_states entries
        self.job_states_create_lock = asyncio.Lock()

        # global condition notified when any job is updated
        self.job_list_condition = asyncio.Condition()

        # ---------- routes ---------- #
        rtr = self.app.router

        rtr.add_route("*",   r"/v2/{repo:.*}/manifests/{tag}",   self.handle_image_manifest)
        rtr.add_route("GET", r"/aio/jobs/{job_id:\d+}/events",   self.handle_job_events)
        rtr.add_route("GET", r"/aio/jobs/events",                self.handle_job_list_events)
        rtr.add_route("GET", r"/aio/apps/{docker_name}/events",  self.handle_webapp_events)
        rtr.add_route("GET", r"/api/v1/jobs/{job_id:\d+}/events",self.handle_job_events)

        self.handler = self.app.make_handler()
        self.host, self.port = bind
        self._shutdown_requested = None

    def shutdown(self):
        log.info("shutdown requested")
        try_set_result(self._shutdown_requested, None)


    def django_request(self, method, path, *k, **kw):
        """Create a aiohttp request object to the django server

        The purpose is just to insert self.django_url in front of the path
        """
        assert path.startswith("/")
        return self.http_client.request(method, self.django_url+path, *k, **kw)


    async def run(self, fork, cafile):
        """main task (run the server)"""
        assert self._shutdown_requested is None, "run() must not be called multiple times"

        self._shutdown_requested = asyncio.Future()
        log.info("starting")
        listener = None
        try:
            # create the aiohttp client
            self.http_client = aiohttp.ClientSession()

            # load the CA certificate
            try:
                self.ssl_context = ssl.create_default_context(cafile=cafile)
            except OSError as e:
                k = "unable to load CA file %r (%s)", cafile, e
                if isinstance(e, FileNotFoundError) and cafile == "/vol/ro/certs/registry.crt":
                    log.warning(*k)
                else:
                    log.error(*k)
                    sys.exit(1)

            # create redis pool
            redis_server_addr = config.env.ALLGO_REDIS_HOST, 6379
            self.redis_pool = await aioredis.create_redis_pool(redis_server_addr)

            # start the aiohttp server
            self.server = await self.loop.create_server(self.handler,
                    host=self.host, port=self.port)
            log.info("listening on: %s:%d", self.host, self.port)

            log.info("server ready")
            daemon_ready(fork)

            # start listener task (to receive reddis notifications)
            # NOTE: this has to be done *after* forking (demonise()), because
            # the redis subscriber starts a thread and this cause a race
            # condition that hangs the server
            # https://stackoverflow.com/questions/39884898/large-amount-of-multiprocessing-process-causing-deadlock
            listener = asyncio.ensure_future(
                    self.redis_notification_listener_task(redis_server_addr))

            await self._shutdown_requested
        finally:
            log.info("shutting down")

            # stop the redis client
            if self.redis_pool is not None:
                self.redis_pool.close()
                await self.redis_pool.wait_closed()

            # stop the redis listener
            if listener is not None:
                listener.cancel()
                await listener

            # stop the aiohttp server
            if self.server is not None:
                self.server.close()
                await self.server.wait_closed()
            await self.app.shutdown()
            await self.handler.shutdown()
            await self.app.cleanup()

            if self.http_client is not None:
                await self.http_client.close()

        log.info("shutdown complete")


    async def update_job_state(self, job_id):
        """Get the job state from redis and store it into job_states"""
        job = self.job_states.get(job_id)
        if job is not None:
            async with job.cond:
                new_state = await self.redis_pool.get(
                        REDIS_KEY_JOB_STATE % job_id)
                # note: null state is possible if the redis entry does not
                # exists yet
                if new_state:
                    if new_state == b"DONE":
                        result = await self.redis_pool.get(
                                REDIS_KEY_JOB_RESULT % job_id)
                        job.result = result.decode() if result else "NONE"
                    job.state = new_state.decode()

                log.info("notify job cond %d", job_id)
                job.cond.notify_all()

    async def redis_notification_listener_task(self, redis_server_addr):
        """Task for listening for redis notification

        - subscribes to REDIS_CHANNEL_AIO
        - automatically reconnects to the server (with rate limiting)
        """

        async for _ in RateLimiter(60):
            conn = None
            try:
                # create redis connection and subscribe to the notification channel
                conn = await aioredis.create_redis(redis_server_addr)
                sub, = await conn.subscribe(REDIS_CHANNEL_AIO)
                log.info("subscribed to redis pub/sub channel %r", REDIS_CHANNEL_AIO)

                async with self.job_states_create_lock:
                    for item_id in self.job_states:
                        await self.update_job_state(item_id)

                async for msg in sub.iter():
                    log.info("redis notification: %r", msg)
                    try:
                        item_type, item_id_str = msg.split(b":")
                        item_id = int(item_id_str)
                    except ValueError as e:
                        log.warning("ignored malformatted notification: %r (%s)", msg, e)
                        continue

                    if item_type == b"job":
                        await self.update_job_state(item_id)
                        async with self.job_list_condition:
                            self.job_list_condition.notify_all()
                    elif item_type == b"webapp":
                        webapp = self.webapp_states.get(item_id)
                        if webapp is not None:
                            async with webapp.cond:
                                webapp.cond.notify_all()
                    else:
                        log.warning("ignored notification for unknown item %r", msg)

            except OSError as e:
                log.error("I/O error in the redis listener (%s)", e)
            except asyncio.CancelledError:
                log.info("notification task terminated")
                return
            except Exception:
                log.exception("error in the redis notification loop")
            finally:
                if conn is not None:
                    with contextlib.suppress(Exception):
                        conn.close()
                        await conn.wait_closed()

    async def get_image_description(self, request, repo: str, manifest: bytes) -> str:
        """Get the description of an image in the registry

        The WebappVersion.description string is provided by the user:
        - in a UI form (if committing from a sandbox)
        - in the 'allgo.description' label (if pushing a docker image)
        
        'manifest' is the manifest of the image being pushed to the registry.
        It points to a config blob which contains the image labels.

        This function parses the manifest, then it downloads and parses the
        config blob and finally returns the label if present.

        To be future-proof (the manifest format may change in the future), the
        function returns an empty string and logs a warning if it fails.
        """
        def error(msg, *k, **kw):
            log.warning("unable to get the allgo.description label from pushed image (%s)",
                    msg % k, **kw)
            return ""

        try:
            js = json.loads(manifest.decode())
            if js["schemaVersion"] != 2:
                return error("unknown schemaVersion=%r" % js["schemaVersion"])

            cfg = js["config"]
            digest, size = cfg["digest"], cfg["size"]
            if size > 1024**2:
                return error("config blob too big (%d bytes)" % size)

            if not re.fullmatch("[A-Za-z0-9:]+", digest):
                return error("malformatted digest %r" % digest)

            async with self.http_client.get("%s/v2/%s/blobs/%s" % (
                    config.env.ALLGO_REGISTRY_PRIVATE_URL, repo, digest),
                    headers=prepare_headers(request), ssl=self.ssl_context) as cfg_reply:
                if not is_ok(cfg_reply):
                    return error("unable to get the config blob (Error %d)" % cfg_reply.status)

                js = json.loads(await cfg_reply.text())
                try:
                    return str(js["config"]["Labels"]["allgo.description"])
                except (TypeError, KeyError):
                    return ""
        except Exception as e:
            return error("unhandled exception", exc_info=sys.exc_info())

    async def handle_image_manifest(self, request):
        """Registry endpoint for pushing/pulling image manifests

        The nginx reverse proxy is configured to forward these requests through the
        django server, so that:
        - we are notified when a new image is pushed
        - we can later implement fine-grained permissions (tag-based rather than
          repository-based)

        Note: the DB is transactionally updated before the client receives the 201
        response
        """
        # NOTE: the registry endpoint also supports the DELETE method to delete the
        # image. Since the deletion of a WebappVersion is not yet implemented, we
        # do not allow the delete method here
        if request.method in ("HEAD", "GET"):
            action = "pull"
        elif request.method == "PUT":
            action = "push"
        else:
            # method not allowed
            return ErrorResponse(status=405)

        headers = prepare_headers(request)
        headers["Content-Type"] = request.content_type
        manifest = await request.read()

        repo = request.match_info["repo"]
        tag  = request.match_info["tag"]
        description = (await self.get_image_description(request, repo, manifest)
                ) if action == "push" else ""

        # call django's pre hook
        async with self.django_request("POST", "/jwt/pre-"+action,
                headers=headers, params={"repo": repo, "tag": tag,
                    "description": description}) as django_reply:

            if not is_ok(django_reply):
                return await forward_response(django_reply)
            version_id = int(await django_reply.read())

        # forward the  HTTP request to the registry
        real_url = "%s/v2/%s/manifests/id%d" % (
                config.env.ALLGO_REGISTRY_PRIVATE_URL, repo, version_id)
        async with self.http_client.request(request.method, real_url,
                headers=headers, data=manifest, ssl=self.ssl_context) as registry_reply:

            if action == "pull":
                # pull
                # -> just forward the reply
                return await forward_response(registry_reply)
            else:
                # push
                # -> call the post-push hook to update the db

                registry_success = int(is_ok(registry_reply))

                # call django's post-push hook
                async with self.django_request("POST", "/jwt/post-push",
                        headers=headers, params={"version_id": str(version_id),
                            "success": str(registry_success)},
                        ) as django_reply:
                    if registry_success and not is_ok(django_reply):
                        return await forward_response(django_reply)
                    else:
                        return await forward_response(registry_reply)


    async def handle_job_events(self, request):
        """Channel for monitoring job events

        This function a sequence json objects describing events related to a job. It is used by two
        endpoints:
            - /aio/jobs/{id}/events         (UI)
            - /api/v1/jobs/{id}/events      (API v1)

        It accepts one optional query parameter `offset` which gives the starting offset for
        streaming the logs (useful for resuming an interupted stream). If negative, then the logs
        will not be streamed at all.

        Currently it streams three kind of messages:
        - new logs:
            {"logs": "<CONTENT>"}

        - state changes:
            {"state":   "<NEW_STATE>"}      (UI  variant)
            {"status":  "<NEW_STATUS>"}     (API variant)

            Note:   the API variant returns a 'status' field to be consistent with the existing
                    API calls (also 'state' and 'results' are implementation details and are not
                    shown to the user)

        - end of stream marker:
            {"eof": null}
        """
        try:
            job_id = int(request.match_info["job_id"])
            offset = int(request.url.query.get("offset", 0))
            if offset < 0:
                offset = None
        except ValueError:
            return Response(status=400)

        log_key   = REDIS_KEY_JOB_LOG   % job_id
        state = None
        async with self.job_states_create_lock:
            job = self.job_states[job_id]

        FINAL_STATES = ("DONE", "ARCHIVED", "DELETED")

        rep = JsonSeqStreamResponse()

        # query the django server to have the job details and ensure this user
        # is allowed to view this job
        if request.path.startswith("/aio/"):
            # using the "Cookie" header)
            headers = prepare_headers(request)
            headers["Accept"] = "application/json"
            async with self.django_request("GET", "/jobs/%d" % job_id,
                    headers=headers, allow_redirects=False) as upstream_rep:
                if upstream_rep.status != 200:
                    # FIXME: django should be able to return 401 directly
                    return Response(status=401 if upstream_rep.status==302 else upstream_rep.status)
                state = (await upstream_rep.json())["state"]
                result = None

            async def send_state(state, result):
                await rep.send({"state": state})

        elif request.path.startswith("/api/v1/"):
            # using an API token
            async with self.django_request("GET", "/api/v1/jobs/%d" % job_id,
                    headers=prepare_headers(request)) as upstream_rep:
                if upstream_rep.status != 200:
                    return Response(status=upstream_rep.status, body=await upstream_rep.read())
                status = (await upstream_rep.json())["status"]
                if not isinstance(status, str):
                    raise TypeError(type(status))
            # API v1 uses 'status' which has to be computed from 'state' and 'result'
            if status in ("new", "waiting", "running", "aborting"):
                # job running
                state  = status.upper()
                result = None
            else:
                # job terminated
                state  = "DONE"
                result = "SUCCESS" if status=="done" else status.upper()
            async def send_state(state, result):
                await rep.send({"status": (
                    state   if state not in FINAL_STATES else
                    "DONE"  if result == "SUCCESS"       else
                    (result or "NONE")).lower()})

        await rep.prepare(request)
        await send_state(state, result)
        try:
            while (offset is not None) or (state not in FINAL_STATES):
                async with job.cond:
                    # poll state change
                    new_state = job.state if job.state != state else None
                    # poll new logs
                    data = (b"" if offset is None
                            else await self.redis_pool.getrange(log_key, offset, offset+8000))

                    if new_state is None and not data:
                        await job.cond.wait()
                        continue

                if new_state is not None:
                    log.info("job %d state updated %s -> %s", job_id, state, new_state)
                    await send_state(new_state, job.result)
                    state = new_state

                if data:
                    if data[-1] == 0x04: # EOF marker
                        data = data[:-1]
                        offset = None
                    else:
                        offset += len(data)
                    await rep.send({"logs": data.decode(errors="replace")})

            await rep.send({"eof": None})

        except asyncio.CancelledError:
            pass
        except Exception:
            log.exception("exception in handle_job_events(job_id=%d)", job_id)
        return rep


    async def handle_job_list_events(self, request):
        """Channel for monitoring job list events

        This HTTP endpoint streams a sequence json objects describing events
        related to a list of jobs.

        The job ids are provided in the query string (parameter 'id')


        Currently only two messages are defined:

        - state update sent when the state of the job is updated (note: the
          result is provided only with the 'DONE' state)

            {"id:"     <JOB_ID>,
             "state": "<NEW_STATE>",
             [ "result": "<RESULT>" ],
             }

        - EOF message sent when the stream is terminated

            {"eof": null}
        """

        try:
            job_ids = {int(x) for x in request.url.query.getall("id", ())}
            # limit the number of jobs in a single request
            if len(job_ids) > 20:
                raise ValueError()
        except ValueError:
            return Response(status=400)

        FINAL_STATES = ("DELETED", "ARCHIVED")

        # query the django server to have the current state of each job and
        # ensure this user is allowed to view these jobs (thanks to the
        # "Cookie" header)

        # state dict (key: job id, value: state)
        states = {}
        results = {}
        for job_id in job_ids:
            headers = prepare_headers(request)
            headers["Accept"] = "application/json"
            async with self.django_request("GET", "/jobs/%d" % job_id,
                    headers=headers, allow_redirects=False) as rep:
                if rep.status == 200:
                    js = await rep.json()
                    states[job_id] = js["state"]
                    results[job_id] = js["result"]
                elif rep.status == 404:
                    states[job_id] = "DELETED"
                else:
                    # FIXME: django should be able to return 401 directly
                    return Response(status=401 if rep.status==302 else rep.status)

        async with self.job_states_create_lock:
            jobs = {job_id: self.job_states[job_id] for job_id in job_ids}

        rep = JsonSeqStreamResponse()
        await rep.prepare(request)

        async def send_state_update(job_id, state, result):
            msg = {"id": job_id, "state": state}
            if state == "DONE":
                msg["result"] = result or "NONE"
            await rep.send(msg)

        def remove_deleted_jobs():
            for job_id in [job_id for job_id, state in states.items()
                    if state in FINAL_STATES]:
                del states[job_id]
                del jobs[job_id]
                log.info("removed job %d from the watch list", job_id)
            log.info("current states: %r", states)

        for job_id, state in states.items():
            await send_state_update(job_id, state, results.get(job_id))
        del results

        remove_deleted_jobs()

        def poll():
            for job_id, state in states.items():
                new_state = jobs[job_id].state
                if new_state is not None and new_state != state:
                    yield job_id, new_state
        try:
            while states:
                async with self.job_list_condition:
                    it = poll()
                    if (first := next(it, None)) is None:
                        await self.job_list_condition.wait()
                        continue
                
                for job_id, new_state in itertools.chain((first,), it):
                    log.info("job %d state updated %s -> %s", job_id, state, new_state)
                    await send_state_update(job_id, new_state, jobs[job_id].result)
                    states[job_id] = new_state

                remove_deleted_jobs()

            await rep.send({"eof": None})
        except asyncio.CancelledError:
            pass
        except Exception:
            log.exception("exception in handle_job_events(job_id=%d)", job_id)
        return rep

    async def handle_webapp_events(self, request):
        """Channel for monitoring events related to a webapp events

        The response is a streamed event sequence:

        - state update (when the state of the sandbox is updated)
            {"sandbox_state": "<NEW_STATE>"}

        """
        docker_name = request.match_info["docker_name"]

        # return a dict (on success) or an HTTP error code (on error)
        async def get_webapp_details():
            async with self.django_request("GET", "/apps/%s/json" % docker_name,
                    headers=prepare_headers(request),
                    allow_redirects=False, timeout=1) as rep:
                if rep.status != 200:
                    # FIXME: django should be able to return 401 directly
                    return 401 if rep.status==302 else rep.status
                return await rep.json()

        # query the django server to have the job details and ensure this user
        # is allowed to view this job (thanks to the "Cookie" header)
        details = await get_webapp_details()
        if isinstance(details, int):
            # error
            return Response(status=details)

        webapp_id = details["id"]
        state = details["sandbox_state"]
        webapp = self.webapp_states[webapp_id]

        rep = JsonSeqStreamResponse()
        await rep.prepare(request)
        await rep.send({"sandbox_state": state})

        try:
            while True:
                async with webapp.cond:
                    # poll state change
                    details = await get_webapp_details()
                    if isinstance(details, int):
                        log.error("handle_webapp_events(webapp_id=%d) got error %d from django",
                                webapp_id, details)
                        break
                    new_state = details["sandbox_state"]

                    if new_state != state:
                        log.info("webapp %d state updated %s -> %s", webapp_id, state, new_state)
                        await rep.send({"sandbox_state": new_state})
                        state = new_state

                    await webapp.cond.wait()

        except asyncio.CancelledError:
            pass
        except Exception:
            log.exception("exception in handle_webapp_events(webapp_id=%d)", webapp_id)
        return rep
