#!/usr/bin/env python3

import argparse
import asyncio
import logging
import logging.handlers
import re
import os
import signal

from . import AllgoAio, daemon_startup

def bind(txt):
    mo = re.match(r"(?:(.+):)?(\d+)\Z", txt)
    if mo is None:
        raise ValueError()
    host, port = mo.groups()
    if host is None:
        host = "0.0.0.0"
    return host, int(port)

def init_logging(level, logdir):
    logging.basicConfig(
            level   = min(level, logging.INFO),
            format  = "%(asctime)s %(levelname)-8s %(name)-24s %(message)s",
            datefmt = "%Y-%b-%2d %H:%M:%S",
            )

    console_handler = logging.root.handlers[0]
    def add_handler(level, hnd):
        hnd.setLevel(level)
        hnd.setFormatter(console_handler.formatter)
        logging.root.addHandler(hnd)

    os.makedirs(logdir, exist_ok=True)

    # normal logs
    add_handler(logging.INFO, logging.handlers.TimedRotatingFileHandler(
        os.path.join(logdir, "aio.log"),
        when = "W6",        # rotate every sunday
        backupCount = 52))  # keep 1 year of logs

    # debug logs
    if log_level <= logging.DEBUG:
        add_handler(logging.DEBUG, logging.handlers.TimedRotatingFileHandler(
            os.path.join(logdir, "debug.log"),
            when = "D",         # rotate every day
            backupCount = 7))   # keep a week of logs

    return console_handler

parser = argparse.ArgumentParser("allgo.aio", "Allgo asyncio server")
parser.add_argument("-b", "--bind", metavar="HOST:PORT", type=bind,
        default=("0.0.0.0", 8001),
        help="bind TCP socket (default: 0.0.0.0:8001)")
parser.add_argument("-d", "--debug", action="store_true",
        help="enable debugging")
parser.add_argument("-v", "--verbose", action="count",
        help="increase verbosity (can be used multiple times)")
parser.add_argument("-l", "--logdir", metavar="PATH", default="/vol/log/aio",
        help="log dir path (default: /vol/log/aio)")
parser.add_argument("--daemon", action="store_true",
        help="daemonise after startup")
parser.add_argument("--pidfile", metavar="PATH", default="/run/aio.pid",
        help="daemon pid file (default: /run/aio.pid)")
parser.add_argument("--cafile", metavar="PEMFILE", default="/vol/ro/certs/registry.crt",
        help="path to the registry CA certificate (default: /vol/ro/certs/registry.crt)")


args = parser.parse_args()

log_level = (
        logging.DEBUG if args.debug else
        logging.INFO  if args.verbose == 1 else
        logging.WARNING)

init_logging(log_level, args.logdir)

daemon_startup(args.daemon, args.pidfile)

loop = asyncio.get_event_loop()

app = AllgoAio(args.bind, loop)

try:
    loop.add_signal_handler(signal.SIGINT,  app.shutdown)
    loop.add_signal_handler(signal.SIGTERM, app.shutdown)
    #loop.add_signal_handler(signal.SIGHUP,  app.reload)

    loop.run_until_complete(app.run(args.daemon, args.cafile))
finally:
    loop.remove_signal_handler(signal.SIGINT)
    loop.remove_signal_handler(signal.SIGTERM)
    #loop.remove_signal_handler(signal.SIGHUP)
