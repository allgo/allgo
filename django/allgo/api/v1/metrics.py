#! /usr/bin/env python3

"""
Metrics API allows to get some metrics about some apps.
    * nb_jobs_created
    * nb_jobs_per_status
    * nb_jobs_per user

They all return an aggregation of the count of jobs on a period of time.

The allgo admin will have access to all app metrics,
whereas a user will have access to its own app metrics.

"""

from datetime import datetime, timedelta
import logging

from django.utils.timezone import make_aware
from django.conf import settings
from django.utils import timezone
from django.db.models import Count
from django.db.models.functions import Trunc

from main.models import Job  # , Webapp, JobQueue

log = logging.getLogger('allgo')


# ======================================
class MetricsError(Exception):
    """Generic error generated by this module."""


class ParamError(MetricsError):
    """Error raised for a pb in params,
        like its format."""


# ======================================
def __fmt_date(date):
    """date is datetime object (for from/to)"""
    return date.date().isoformat()


def str2date(date):
    """convert date to formatted string."""
    try:
        return datetime.strptime( date, "%Y-%m-%d" )
    except ValueError as e:
        log.error(e)
        raise ParamError(str(e))


__DEFAULT_DELTA = {
    'year' : timedelta(days = 5 * 365),
    'month': timedelta(days = 2 * 365),
    'day'  : timedelta(days = 30 + 31),
}
DEFAULT_STEP = 'month'


def __get_params(from_, to, step):
    """get params, if needed, set default or format."""
    try:
        delta = __DEFAULT_DELTA[step]
    except KeyError as e:
        msg = "Metrics action {} is unknown.".format( str(e) )
        log.error("%s is an invalid step, fall back to default",
                  e)
        step  = DEFAULT_STEP
        delta = __DEFAULT_DELTA[step]

    if to is None:
        to = timezone.now()
    else:
        to = make_aware(str2date(to))

    """ ensure that to field is include in the results """
    to = to + timezone.timedelta(days=1)
    if from_ is None:
        from_ = to - __DEFAULT_DELTA[step]
    else:
        from_ = make_aware(str2date(from_))

    return from_, to, step


def __api_dict(from_, to, step, l_records ):
    """from_ and to are datetime.datetime object."""
    return {
        'data': l_records,
        'from': __fmt_date(from_),
        'to'  : __fmt_date(to),
        'step': step,
    }


def nb_jobs_per_user(app_id, from_=None, to=None, step=None):
    """compute for the webapp app_id, for each user
        the number of jobs "updated" (launched,terminated)
        per step period between [from\_ ; to[.

        :param app_id: webapp id
        :type  app_id: int
        :param from\_: the beginning of the time interval
        :type  from\_: string
        :param to:     the end of the time interval
        :type  to:     string
        :param step:   data aggregation interval
        :type  step:   string

        :return: a dictionnary with 3 keys 'from', 'to' [date] remind the date range,
            and 'data' which contains a list of dictionnaries
            {user:, uname:, time_period:, n:}
        :rtype:  dictionnary
    """
    from_, to, step = __get_params(from_, to, step)
    msg = "provided for nb_jobs_per_user from '{}' to '{}', step of '{}'"\
        .format(from_, to, step)
    log.info(msg)

    counts = Job.objects.filter(webapp=app_id, updated_at__range=[from_, to]) \
        .annotate(time_period=Trunc('updated_at', step)) \
        .values('time_period', 'user', 'user__username') \
        .annotate(n=Count('user'))

    # record is a dictionnary built from a sql row response
    for record in counts:
        record['uname'] = record.pop('user__username')
        record['time_period'] = __fmt_date( record.pop('time_period') )

    return __api_dict( from_, to, step, list(counts) )


def nb_jobs_per_result(app_id, from_=None, to=None, step=None):
    """compute the number of jobs
        for the webapp app_id, for each result (only terminated)
        per step period between [from\_ ; to[ (updated_at).

        :param app_id: webapp id
        :type  app_id: int
        :param from\_: the beginning of the time interval
        :type  from\_: string
        :param to:     the end of the time interval
        :type  to:     string
        :param step:   data aggregation interval
        :type  step:   string

        :return: a dictionnary with 3 keys 'from', 'to' [date] remind the date range,
            and 'data' which contains a list of dictionnaries {result:, time_period:, n:}
        :rtype:  dictionnary
    """
    from_, to, step = __get_params(from_, to, step)
    msg = "provided for nb_jobs_per_result from '{}' to '{}', step of '{}'"\
        .format(from_, to, step)
    log.info(msg)

    counts = Job.objects.filter(webapp=app_id, updated_at__range=[from_, to]) \
        .exclude(result=Job.NONE) \
        .annotate(time_period=Trunc('updated_at', step)) \
        .values('time_period', 'result' ) \
        .annotate(n=Count('result'))

    # record is a dictionnary built from a sql row response
    for record in counts:
        record['result'] = Job.JOB_RESULT_CHOICES[record['result']][1]
        record['time_period'] = __fmt_date( record.pop('time_period') )

    return __api_dict( from_, to, step, list(counts) )


def nb_jobs_created(app_id, from_=None, to=None, step=None):
    """compute for the webapp app_id, the number of jobs created
        per step period between [from\_ ; to[.

        :param app_id: webapp id
        :type  app_id: int
        :param from\_: the beginning of the time interval
        :type  from\_: string
        :param to:     the end of the time interval
        :type  to:     string
        :param step:   data aggregation interval
        :type  step:   string

        :return: a dictionnary with 3 keys 'from', 'to' [date] remind the date range,
            and 'data' which contains a list of dictionnaries {time_period:, n:}
        :rtype:  dictionnary
    """

    from_, to, step = __get_params(from_, to, step)

    counts = Job.objects.filter(webapp=app_id, created_at__range=[from_, to]) \
        .annotate(time_period=Trunc('created_at', step)) \
        .values('time_period') \
        .annotate(n=Count('id'))

    # record is a dictionnary built from a sql row response
    for record in counts:
        record['time_period'] = __fmt_date( record.pop('time_period') )

    return __api_dict( from_, to, step, list(counts) )


def all_jobs_created(app_id, from_=None, to=None, step=None):
    """compute for the webapp app_id, the number of jobs created
        per step period between [from\_ ; to[.

        :param app_id: webapp id
        :type  app_id: int
        :param from\_: the beginning of the time interval
        :type  from\_: string
        :param to:     the end of the time interval
        :type  to:     string
        :param step:   data aggregation interval
        :type  step:   string

        :return: a dictionnary with 3 keys 'from', 'to' [date] remind the date range,
            and 'data' which contains a list of dictionnaries {time_period:, n:}
        :rtype:  dictionnary
    """

    from_, to, step = __get_params(from_, to, step)
    msg = "provided from '{}' to '{}', step of '{}'".format(from_, to, step)
    log.info(msg)

    counts = Job.objects.filter(webapp=app_id, created_at__range=[from_, to]) \
        .exclude(result=Job.NONE) \
        .annotate(time_period=Trunc('created_at', step)) \
        .values('time_period', 'user', 'user__username', 'result') \
        .annotate(n=Count('id'))

    # record is a dictionnary built from a sql row response
    for record in counts:
        record['result'] = Job.JOB_RESULT_CHOICES[record['result']][1]
        record['user_name'] = record.pop('user__username')
        record['time_period'] = __fmt_date( record.pop('time_period') )

    return __api_dict( from_, to, step, list(counts) )


# ======================================
ACTIONS = {
    "per_user"  : nb_jobs_per_user,
    "per_result": nb_jobs_per_result,
    "created"   : nb_jobs_created,
    "all"   : all_jobs_created,
    # ~ "": ,
}
