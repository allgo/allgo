from django.conf.urls import url

from . import views

app_name = 'api'

urlpatterns = [
    url(r'^jobs$', views.jobs, name='jobs'),
    url(r'^metrics/(?P<what>\w+)/(?P<app_id>\d+)', views.Metrics.as_view(), name='metrics'),
    url(r'^jobs/(?P<pk>\d+)$', views.APIJobView.as_view(), name='job'),
    url(r'^jobs/(?P<pk>\d+)/abort$', views.APIJobAbortView.as_view(), name='job_abort'),
    url(r'^datastore/(?P<pk>\d+)/(.*)$', views.APIDownloadView.as_view(), name='download'),
]
