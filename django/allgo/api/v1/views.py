import logging
import os
from   typing import Mapping, Union

import config.env
from django.core.validators import ValidationError
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.http import JsonResponse
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

import api.v1.metrics as metrics

from main.helpers import (upload_data, get_base_url, lookup_job_file, get_request_user, abort_job,
        delete_job)
from main.mixins import JobAuthMixin
from main.models import Job, Webapp, JobQueue

log = logging.getLogger('allgo')

DATASTORE = config.env.ALLGO_DATASTORE
BUF_SIZE = 65536


def get_link(jobid, filename, request):
    return '/'.join((get_base_url(request), "api/v1/datastore", str(jobid), filename))


class APIJobView(JobAuthMixin, View):
    handle_deleted_job = lambda self: None

    @classmethod
    def as_view(cls, **kw):
        return csrf_exempt(transaction.non_atomic_requests(super().as_view(**kw)))

    def get(self, request, pk):
        try:
            job = Job.objects.get(id=pk)
            files = {}
            for filename in os.listdir(job.data_dir):
                if lookup_job_file(job.id, filename):
                    files[filename] = get_link(job.id, filename, request)

            response = {
                job.id: files,
                "status": "done" if job.result==Job.SUCCESS else job.status.lower(),
            }
            return JsonResponse(response)
        except Job.DoesNotExist as e:
            log.error("Job not found %s", str(e))
            return JsonResponse({'error': 'Job not found'}, status=404)

    def delete(self, request, pk):
        job, status, message = delete_job(int(pk))
        level = "info" if status == 200 else "error"
        return JsonResponse({level: message}, status=status)

class APIJobAbortView(JobAuthMixin, View):

    @classmethod
    def as_view(cls, **kw):
        return csrf_exempt(super().as_view(**kw))

    def post(self, request, pk):
        msg = abort_job(int(pk))
        return JsonResponse({'info': msg})


class Metrics(View):
    """Metrics view provides only a get access."""

    def get(self, request, app_id, what):
        """Method used when accessing the Metrics Api.

            :param request: the request object.
            :param  app_id: the id of the app we want metrics from.
            :type   app_id: integer
            :param    what: the kind of metrics we want.
            :type     what: keyword, key of metrics.ACTIONS
        """
        user = get_request_user(request)
        if not user:
            msg  = "API request without http authorisation\n"
            msg += "\tagent : %s\n\tadress : %s\n\tquery : %s\n"
            log.info(
                msg,
                request.META.get('HTTP_USER_AGENT', 'agent_unknown'),
                request.META.get('REMOTE_ADDR', 'remote_adress_unknown'),
                request.META.get('QUERY_STRING', 'query_unknown'),
            )

            msg = 'API request without http authorisation'
            return JsonResponse({'error': msg}, status=401)

        try:
            msg = "%s is asking for '%s' with app_id '%s'"
            log.info(msg, user, what, app_id)

            if user.is_superuser:
                app = Webapp.objects.get(id=app_id)
            else:
                app = Webapp.objects.get(id=app_id, user=user)
            log.info("app name is *%s*", app.name)

            from_ = request.GET.get('from', None)
            to    = request.GET.get('to',   None)
            step  = request.GET.get('step', None)
            msg = "asked from '%s' to '%s', step of '%s'"
            log.info(msg, from_, to, step)

            # jobs is a dictionnary
            jobs = metrics.ACTIONS[what](app_id, from_, to, step)
            return JsonResponse({ app.name: jobs })

        except Webapp.DoesNotExist as e:
            log.error("Webapp not found -%s-", str(e))
            return JsonResponse({'error': 'Webapp not found'}, status=404)

        except metrics.ParamError as e:
            # the error should have already be logged
            return JsonResponse({'error': str(e)}, status=400)

        except KeyError as e:
            log.error("Metrics action %s is unknown.", str(e) )
            return JsonResponse({'error': str(e)}, status=404)

def get_webapp(post: Mapping[str, str]) -> Union[JsonResponse,Webapp]:
    webapp_id = post.get("job[webapp_id]")
    docker_name = post.get("job[webapp]")

    try:
        if webapp_id is not None:
            if docker_name is not None:
                return JsonResponse(
                        {"error": "must provide either 'job[webapp]' or 'job[webapp_id]', not both"})
            elif webapp_id.isdigit():
                return Webapp.objects.get(id=webapp_id)
            else:
                # The API used to accept an id or docker_name as `job[webapp]`. It was not very clean,
                # but we keep it so as not to break existing code.
                docker_name = webapp_id

        if docker_name is None:
            return JsonResponse({"error": "no app selected (must provide either 'job[webapp]=NAME'"
                    " or 'job[webapp_id]=ID')"}, status=400)
        else:
            return Webapp.objects.get(docker_name=docker_name)

    except Webapp.DoesNotExist:
        return JsonResponse({'error': 'application not found'}, status=404)


@csrf_exempt
def jobs(request):
    user = get_request_user(request)
    if not user:
        log.info("API request without http authorisation %s %s %s", request.META['HTTP_USER_AGENT'],
                 request.META['REMOTE_ADDR'], request.META['QUERY_STRING'])
        return JsonResponse({'error': 'API request without http authorisation'}, status=401)

    if request.method != "POST":
        return JsonResponse({"error": "method not allowed"}, status=405)
    try:
        post = request.POST
    except Exception:
        return JsonResponse({"error": "malformatted request body"}, status=400)

    if isinstance((app := get_webapp(request.POST)), JsonResponse):
        return app

    requested_version = request.POST.get('job[version]')
    if requested_version == "sandbox" and app.sandbox_state == Webapp.RUNNING:
        version = "sandbox"
    elif (wv := app.get_webapp_version(requested_version)) is not None:
        version = wv.number
    else:
        return JsonResponse({"error":
            "This app is not yet published" if requested_version is None else
            f"This app does not have a version named {requested_version!r}"},
            status=404)

    queue = app.job_queue
    if 'job[queue]' in request.POST:
        try:
            queue = JobQueue.objects.get(name=request.POST['job[queue]'])
        except JobQueue.DoesNotExist:
            return JsonResponse({'error': 'Unknown queue'}, status=400)

    log.info("Job submit by user %s", user)
    job = Job(param=request.POST.get('job[param]', ''), queue=queue, webapp=app, user=user)
    job.version = version
    # run the Job validators
    try:
        job.save()
    except ValidationError as e:
        return JsonResponse({'error': "Invalid parameters: %s" % e.message_dict}, status=400)

    # FIXME: possible infoleak: because we have ATOMIC_REQUESTS=True the
    # current id can be reused for another job in case anything fails before
    # the end of the request
    job.files = upload_data(request.FILES.values(), job)

    # start the job
    job.state = Job.WAITING
    job.save()

    path = reverse('api:job', kwargs={'pk':job.id})
    response = {
        "avg_time": 0, # legacy, not relevant anymore
        "id"      : job.id,
        "url"     : get_base_url(request)+path,
    }

    return JsonResponse(response)


class APIDownloadView(JobAuthMixin, View):

    def get(self, request, *args, **kwargs):
        log.error("datastore requests must be served by nginx (bad config!)")
        return JsonResponse({'error': 'Internal Server Error'}, status=500)
