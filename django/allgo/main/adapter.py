from allauth.account.adapter import DefaultAccountAdapter
from django.core.validators import EmailValidator


class AccountAdapter(DefaultAccountAdapter):
    """Adapter for the default Django account using Allauth plugin
    """

    def save_user(self, request, user, form, commit=True):
        """
        Change the default behaviour to save the email as the username.

        Compared to the original method, I don't save any data related to the
        first and last name and force to save the email field as the username
        instead of extracting the first part of the email (before the @) and
        save it as the username.

        This is a workaround for the issue of email signing issue.

        """
        data = form.cleaned_data
        user.email = data.get('email')
        user.username = data.get('email')
        if 'password1' in data:
            user.set_password(data["password1"])
        else:
            user.set_unusable_password()
        self.populate_username(request, user)
        if commit:
            # Ability not to commit makes it easier to derive from
            # this adapter by adding
            user.save()
        return user


# Validator for the username as a proper email
custom_username_validators = [EmailValidator()]
