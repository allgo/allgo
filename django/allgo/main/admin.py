from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

# Define an inline admin descriptor for Employee model
# which acts a bit like a singleton
from .models import (
        AllgoUser,
        DockerOs,
        Job,
        JobQueue,
        Runner,
        Webapp,
        WebappParameter,
        WebappVersion,
        Tos,
        UserAgreement,
)


class AllgoUserInline(admin.StackedInline):
    model = AllgoUser
    can_delete = False
    verbose_name_plural = 'allgo users'


# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (AllgoUserInline, )


class WebappAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'docker_name', 'user')


class WebappParameterAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_webapp', 'name')

    def get_webapp(self, obj):
        return obj.webapp.name

    get_webapp.short_description = 'Webapp'
    get_webapp.admin_order_field = 'webapp__name'


class WebappVersionAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_webapp', 'number')

    def get_webapp(self, obj):
        return obj.webapp.name
    get_webapp.short_description = 'Webapp'
    get_webapp.admin_order_field = 'webapp__name'


class JobAdmin(admin.ModelAdmin):
    list_display = ('webapp', 'state', 'result')


class RunnerAdmin(admin.ModelAdmin):
    list_display = ('id', 'hostname', 'cpu_count', 'mem_in_GB')


# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(DockerOs)
admin.site.register(Runner, RunnerAdmin)
admin.site.register(Webapp, WebappAdmin)
admin.site.register(WebappParameter, WebappParameterAdmin)
admin.site.register(WebappVersion, WebappVersionAdmin)
admin.site.register(Job, JobAdmin)
admin.site.register(AllgoUser)
admin.site.register(JobQueue)
admin.site.register(Tos)
admin.site.register(UserAgreement)
