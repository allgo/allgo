from allauth.account.forms import SignupForm
from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from taggit.forms import TagField

import config
from .models import (
        AllgoUser,
        DockerOs,
        Job,
        JobQueue,
        Runner,
        Token,
        Webapp,
        WebappParameter,
        WebappVersion,
)
from .validators import docker_name_validator, DANGEROUS_PARAM_CHARS


# overrides mandated since CVE-2023-31047 for multiple files in the same input field
# see:
# - https://www.djangoproject.com/weblog/2023/may/03/security-releases/
# - https://docs.djangoproject.com/en/4.2/topics/http/file-uploads/#uploading-multiple-files
class MultipleFileInput(forms.ClearableFileInput):
    allow_multiple_selected = True


class MultipleFileField(forms.FileField):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault("widget", MultipleFileInput())
        super().__init__(*args, **kwargs)

    def clean(self, data, initial=None):
        single_file_clean = super().clean
        if isinstance(data, (list, tuple)):
            result = [single_file_clean(d, initial) for d in data]
        else:
            result = single_file_clean(data, initial)
        return result





class UserForm(forms.ModelForm):

    first_name = forms.CharField(
        label='First name',
        label_suffix='',
        required=False,
    )
    last_name = forms.CharField(
        label='Last name',
        label_suffix='',
        required=False,
    )

    class Meta:
        model = User
        fields = ('first_name', 'last_name')


class SSHForm(forms.ModelForm):

    sshkey = forms.CharField(
            label="SSH Key",
            label_suffix='',
            widget=forms.Textarea,
            help_text=mark_safe('Before you can add an SSH key you need to '
            '<a href="https://gitlab.inria.fr/help/ssh/README#generating-a-new-ssh-key-pair">'
            'generate one</a> or use an '
            '<a href="https://gitlab.inria.fr/help/ssh/README#locating-an-existing-ssh-key-pair">'
            'existing key.</a>'))

    class Meta:
        model = AllgoUser
        fields = ('sshkey',)


class HomeSignupForm(SignupForm):

    def __init__(self, *args, **kwargs):
        super(HomeSignupForm, self).__init__(*args, **kwargs)


class JobForm(forms.ModelForm):
    version = forms.CharField(
            label = 'Version',
            label_suffix='',
            help_text='Version of the application')
    files = MultipleFileField(
            required=False,
            label='Input files',
            label_suffix='',
            help_text='Select or drag-and-drop the input files to be uploaded.')
    queue_id = forms.ModelChoiceField(
            queryset=JobQueue.objects.all().distinct().order_by("timeout"),
            initial=1,
            label='Queue',
            label_suffix='',
            help_text=mark_safe('The '
                '<a href="https://allgo.gitlabpages.inria.fr/doc/run.html?highlight=queue#queue">'
                'queue for scheduling your job</a>. '
                'Queues with shorter limit have a higher priority.'),
            )
    param = forms.CharField(label='Parameters', label_suffix='', required=False,
            help_text='Enter the parameters you need or click on the "presets" button to select '
                      'any predefined one.')
    webapp_parameters = forms.ModelChoiceField(
            queryset=WebappParameter.objects.all(),
            label="presets",
            required=False)

    def __init__(self, *args, **kwargs):
        webapp = kwargs.pop('webapp')
        super().__init__(*args, **kwargs)
        self.instance.webapp = webapp # so that the job_param_validator can read it

        self.fields['webapp_parameters'].queryset = WebappParameter.objects.filter(webapp=webapp)

        for field in self.fields.values():
            field.error_messages = {'required':'The field "{fieldname}" is required'.format(
                fieldname=field.label)}

    class Meta:
        model = Job
        fields = ('param', 'queue_id', 'webapp_parameters')


class RunnerForm(forms.ModelForm):

    open_bar = forms.BooleanField(required=False, label="Open bar runner", label_suffix='')
    hostname = forms.CharField(required=False, label_suffix='')
    cpu_count = forms.IntegerField(required=False, label='Number of CPUs', label_suffix='')
    mem_in_GB = forms.IntegerField(required=False, label='Memory', label_suffix='')
    webapps = forms.ModelMultipleChoiceField(
            required=False,
            queryset=Webapp.objects.all(),
            label="Your applications",
            label_suffix='',
            widget=forms.CheckboxSelectMultiple)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        super(RunnerForm, self).__init__(*args, **kwargs)
        self.fields['webapps'].queryset = Webapp.objects.filter(user_id=self.request.user.id)
        self.fields['webapps'].choices = [(webapp.id, webapp.name)
                for webapp in Webapp.objects.filter(user_id=self.request.user.id)]

        for field in self.fields.values():
            field.error_messages = {'required':'The field "{fieldname}" is required'.format(
                fieldname=field.label)}

    class Meta:
        model = Runner
        fields = ('hostname', 'cpu_count', 'mem_in_GB', 'webapps', 'open_bar')


class WebappForm(forms.ModelForm):

    # Basic
    name = forms.CharField(label="Application name", label_suffix="")
    contact = forms.EmailField(label="Email contact", label_suffix="", required=False,
            help_text="By default this will be your personnal e-mail address. "
                      "You may fill this field if you wish to use a different contact address.")
    description = forms.CharField(widget=forms.Textarea, label="Description", label_suffix="")
    private = forms.TypedChoiceField(
            coerce=lambda x: x == 'True',
            choices=((False, 'Public'), (True, 'Private')),
            initial=True,
            label="Private mode",
            label_suffix='',
            help_text='Private apps do not appears in the public list, '
                      'but they are reachable by users knowing the url.',
            widget=forms.RadioSelect)

    # Advanced
    ADVANCED_FIELDS = ("docker_os", "memory_limit_mb", "job_queue",
            "entrypoint", "owner", "tags", "allow_param_chars")
    docker_os = forms.ModelChoiceField(
            queryset=DockerOs.objects.all().distinct(),
            label='Operating sytem',
            label_suffix='')
    memory_limit_mb = forms.IntegerField(label="Memory limit", label_suffix='',
            initial=config.env.ALLGO_WEBAPP_DEFAULT_MEMORY_LIMIT_MB,
            min_value=0)
    job_queue = forms.ModelChoiceField(
            queryset=JobQueue.objects.all().distinct().order_by("timeout"),
            label='Default job queue',
            help_text=mark_safe('The default '
                '<a href="https://allgo.gitlabpages.inria.fr/doc/deploy.html#queue">'
                'queue for scheduling new jobs</a> using this app.'),
            label_suffix='')
    entrypoint = forms.CharField(label="Entrypoint", label_suffix="",
            help_text=mark_safe('This is the '
                '<a href="https://allgo.gitlabpages.inria.fr/doc/deploy.html#entrypoint">'
                'command executed when allgo runs a job</a> for this app.'),
            initial="/home/allgo/entrypoint")
    allow_param_chars = forms.CharField(required=False, label="Allowed symbols in job parameters",
            label_suffix="",
            help_text=format_html('For security reasons, job parameters cannot include any of '
                '<tt><b>{}</b></tt> so as to prevent '
                '<a href="https://en.wikipedia.org/wiki/Code_injection">shell code injection</a> '
                'and <a href="https://en.wikipedia.org/wiki/Directory_traversal_attack">directory '
                'traversal</a> attacks by a malicious user of your app. If your <i>really</i> need '
                'to accept any of these, then you may list the allowed symbols here <b>but you '
                'must first ensure that your app inputs are properly sanitised</b>. If in doubt, '
                'you should leave this field empty.', DANGEROUS_PARAM_CHARS))
    owner = forms.CharField(required=False, label="Owner", label_suffix='',
            help_text="Username of the new owner of the application."
                      " You will immediately loose access to the application.")
    tags = TagField(required=False, label_suffix='', help_text="Tags are separated by a comma.")

    def __init__(self, *args, **kwargs):
        super(WebappForm, self).__init__(*args, **kwargs)

        try:
            self.fields['owner'].initial = self.instance.user
        except ObjectDoesNotExist:
            pass

        self.instance.job_queue = JobQueue.objects.filter(
                is_default=True).first()

        for field in self.fields.values():
            field.error_messages = {'required':'The field "{fieldname}" is required'.format(
                fieldname=field.label)}

    def get_memory_limit(self, request):
        # - only allgo admins are allowed to set an arbitrary limit
        # - value is converted into bytes
        return (self.cleaned_data["memory_limit_mb"]
                if request.user.is_superuser
                else int(config.env.ALLGO_WEBAPP_DEFAULT_MEMORY_LIMIT_MB)
                ) * 1024**2

    class Meta:
        model = Webapp
        fields = ('name', 'description', 'contact', 'entrypoint', 'job_queue',
                'private', 'docker_os', 'entrypoint', 'owner', 'tags', 'allow_param_chars')


class WebappImportForm(forms.Form):
    webapp_id   = forms.IntegerField(label="Webapp ID", required=False)
    docker_name = forms.CharField(label="Short name", required=False,
            validators=[docker_name_validator])


class WebappTokenForm(forms.ModelForm):
    name = forms.CharField(help_text="An arbitrary name for this token")
    lifetime = forms.ChoiceField(initial="7", required=False,
            choices=[
                ("1",   "1 day"),
                ("7",   "1 week"),  
                ("30",  "1 month"),
                ("91",  "3 months"),
                ("182", "6 months"),
                ("365", "1 year"),
                ("1095","3 years"), 
                ("0",   "unlimited"),
                ])
    class Meta:
        model = Token
        fields = ("name", "expires_at")

class WebappVersionForm(forms.ModelForm):
    number = forms.CharField(label="Version", help_text="""The version number. It must be a valid
        docker tag.""")
    description = forms.CharField(help_text="An optional description for this version.",
            required=False)
    published = forms.BooleanField(required=False, help_text="""Tick this box to make this version
        usable by other users.""")

    class Meta:
        model = WebappVersion
        fields = ("number", "description", "published")
