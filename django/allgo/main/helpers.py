import base64
import hashlib
import itertools
import logging
import os
import re
import shutil
from   typing import Optional, Tuple

import redis
import IPy
from django.conf import settings
from django.core.paginator import Paginator
import django.db
from django.db.models import Q
from django.shortcuts import get_object_or_404
import config
from .models import (
        AllgoUser,
        Job,
        Webapp,
        WebappVersion,
        )


log = logging.getLogger('allgo')
DEFAULT_ENTROPY = 32 # number of bytes to return by default


##################################################
# redis keys


# job log
REDIS_KEY_JOB_LOG       = "log:job:%d"
REDIS_KEY_JOB_STATE     = "state:job:%d"

# pubsub channels for waking up allgo.aio (frontend) and the controller
# (ourselves)
REDIS_CHANNEL_AIO        = "notify:aio"
REDIS_CHANNEL_CONTROLLER = "notify:controller"

# pubsub messages
REDIS_MESSAGE_JOB_UPDATED    = "job:%d"
REDIS_MESSAGE_WEBAPP_UPDATED = "webapp:%d"

##################################################


# global redis connection pool
_redis_connection_pool = None


def get_ssh_data(key):
    """
    Return the fingerprint and comment of a given SSH key.

    It has been tested only on RSA keys
    """
    # FIXME: this implementation computes a MD5 hash, which was superseded a
    #        long time ago. The current openssh fingerprinats are based on
    #        use SHA256, the output looks like:
    #           2048 SHA256:sjsPbfDzfuylskauytlylfpaltjufjhqnphYvVYnhbI
    #        We should use this format too
    key_parts = key.strip().split(None, 2)
    if len(key_parts) == 3:
        comment = key_parts[2]
    else:
        comment = None

    key = base64.b64decode(key.strip().split()[1].encode('ascii'))
    fp_plain = hashlib.md5(key).hexdigest()
    fp_encoded = ':'.join(a+b for a, b in zip(fp_plain[::2], fp_plain[1::2]))

    return fp_encoded, comment

def upload_data(uploaded_files, job):
    """
    Upload any data according to a specific job id

    Args:
        uploaded_files:   iterable that yields
                          django.core.files.uploadedfile.UploadedFile objects
        job (Job):        job

    Examples:

        >>> upload_data(self.request.FILES.getlist('files'), job)

    Returns:
        An ordered list suitable for the Job.files field
        [{"name": "...", "size": "...}, ...]
    """

    job_dir = job.data_dir
    os.makedirs(job_dir)

    result = []
    for file_data in uploaded_files:
        filename = file_data.name

        # sanitise the filename to prevent directory escape and options injection
        #
        # The filename is provided by the user submitting the job, it cannot be
        # trusted. Dangerous characters are replaced with "_" so as to
        # guarantee that the user won't:
        #   - read/write anything outside the job dir
        #   - inject options (starting with '-') in a command
        #
        # This is a security feature, do not remove it.
        #
        if filename in (".", ".."):
            filename = filename.replace(".", "_")
        filename = filename.replace("/", "_")
        if filename.startswith("-"):
            filename = "_" + filename[1:]

        filepath = os.path.join(job_dir, filename)
        with open(filepath, 'wb') as destination:
            for chunk in file_data.chunks():
                destination.write(chunk)
            result.append({"name": filename, "size": destination.tell()})
    return result

def lookup_job_file(job_id, filename):
    """Look up a job data file and return its real path

    This function also performs additional security checks to prevent escaping
    from the job data directory:
    - prevent accessing subdirectories or other job directories
    - exclude non-regular files
    - exclude symbolic links

    returns None if lookup fails
    """

    path = os.path.join(settings.DATASTORE, str(job_id), filename)
    if (        "/" not in filename
        and     os.path.isfile(path)
        and not os.path.islink(path)
        ):
        return path

def get_redis_connection():
    "Get a redis connection from the global pool"
    global _redis_connection_pool

    if _redis_connection_pool is None:
        _redis_connection_pool = redis.ConnectionPool(
                host=config.env.ALLGO_REDIS_HOST)

    return redis.Redis(connection_pool=_redis_connection_pool)


def notify_controller(obj):
    """Notify the controller that an entry was updated in the db

    The notification is sent through the redis pubsub channel
    REDIS_CHANNEL_CONTROLLER.
    """
    conn = get_redis_connection()

    if isinstance(obj, Job):
        conn.publish(REDIS_CHANNEL_CONTROLLER, REDIS_MESSAGE_JOB_UPDATED % obj.id)
    elif isinstance(obj, Webapp):
        conn.publish(REDIS_CHANNEL_CONTROLLER, REDIS_MESSAGE_WEBAPP_UPDATED % obj.id)
    else:
        raise TypeError(obj)


_ALLOWED_IP_NETWORKS = list(map(IPy.IP, config.env.ALLGO_ALLOWED_IP_ADMIN.split(",")))
def is_allowed_ip_admin(ip_address):
    """Return true if admin actions are allowed from this IP address

    The function return true if the provided ip address is included in at least
    one network listed in ALLGO_ALLOWED_IP_ADMIN.
    """
    return any(ip_address in net for net in _ALLOWED_IP_NETWORKS)



def get_base_url(request):
    """Extract the base url from this django request object

        typically this will be "https://allgo.inria.fr"
    """
    scheme = request.META.get("HTTP_X_FORWARDED_PROTO", request.scheme)
    # NOTE: django's request.get_host()/.get_port() are kind of broken because
    # they do not expect the port to be provided in the Host/X-Forwarded-Host
    # headers (which is quite common)
    host = request.META.get("HTTP_X_FORWARDED_HOST")
    if host is None:
        host = request.get_host()
    return "%s://%s" % (scheme, host)


def get_request_user(request):
    """Return the authenticated user from the provided request

    Depending on the request path, the authentication is attempted on:
    - the token provided in the HTTP Authorization header for /api/ urls
    - the session cookie for other urls

    In case of /auth requests we assume that 'X-Original-URI' is the path of
    the current request.

    Args:
        request

    Returns:
        a User or None
    """
    path = request.path
    if path == "/auth":
        path = request.META['HTTP_X_ORIGINAL_URI']
    if path.startswith("/api/"):
        # authenticated by token for API requests
        #
        # NOTE: we must NOT authenticate by cookie because the CORS
        #       configuration in the nginx.conf allows all origins
        mo = re.match(r"Token token=(\S+)",
                request.META.get('HTTP_AUTHORIZATION', ''))
        if mo:
            return getattr(
                    # FIXME: user token should have a unicity constraint
                    AllgoUser.objects.filter(token=mo.group(1)).first(),
                    "user", None)
    else:
        # authenticated by cookie for other requests
        if request.user.is_authenticated:
            return request.user


def query_webapps_for_user(user):
    """Return a queryset of all webapps visible by a given user"""

    if user.is_superuser:
        return Webapp.objects.all()
    
    # a webapp is visible in the public index if it is not private (obviously) and if it has at
    # least one version published and ready.
    with django.db.connection.cursor() as cur:
        cur.execute("""SELECT webapp_id FROM dj_webapp_versions WHERE webapp_id IN (
            SELECT id FROM dj_webapps WHERE private != 1
        ) AND published=1 AND state=%s GROUP BY webapp_id""", (WebappVersion.READY,))
        public_ids = list(itertools.chain(*cur.fetchall()))

    return Webapp.objects.filter(Q(user_id=user.id) | Q(id__in=public_ids))


def abort_job(pk: int) -> str:
    # switch state to ABORTING if the job is running (this is done
    # atomically to avoid messing up with the controller)
    aborted = (
            Job.objects.filter(id=pk, state=Job.WAITING).update(state=Job.DONE, result=Job.ABORTED,
                exec_time=0) or
            Job.objects.filter(id=pk, state=Job.RUNNING).update(state=Job.ABORTING))

    job = Job.objects.get(id=pk)
    if aborted:
        from .signals import job_post_save
        job_post_save(job)
        return "aborting job"
    elif job.state == Job.ABORTING:
        return "job abort already in progress"
    else:
        return "job already terminated"


def delete_job(pk: int) -> Tuple[Job, int, str]:

    # NOTE: if job is in WAITING state, then any state update must be done
    # atomically so as not to mess up with che controller
    deleted = (
            Job.objects.filter(id=pk, state=Job.DONE).update(state=Job.ARCHIVED) or
            Job.objects.filter(id=pk, state__in=(Job.NEW, Job.WAITING)).update(state=Job.DELETED))

    django.db.transaction.commit()

    job = get_object_or_404(Job, id=pk)
    if deleted:
        notify_controller(job) # so that the DELETED/ARCHIVED state is propagated into the redis db

        # delete the data dir if present
        # FIXME: if this fail then we have dangling files staying in the way
        job_dir = job.data_dir
        if os.path.exists(job_dir):
            shutil.rmtree(job_dir)

        if job.state == Job.DELETED:
            job.delete()
        return job, 200, "job successfully deleted"

    elif job.state in (Job.DELETED, Job.ARCHIVED):
        return job, 200, "job already deleted"
    else:
        return job, 409, "cannot delete a running job"


class BoundedPaginator(Paginator):
    """A paginator that always returns a valid page

    It uses the behaviour of Paginator.get_page(), i.e. return the first page if the page is not a
    valid number and the last page if the page number is too big (instead of thowing a 404 error)
    https://docs.djangoproject.com/en/4.0/ref/paginator/#django.core.paginator.Paginator.get_page

    Usage: add a paginator_class attribute to ListView/MultipleObjectMixin classes

        class MyView(ListView):
            paginator_class = BoundedPaginator
            ...
    """
    def page(self, number):
        return Paginator.get_page(super(), number)

