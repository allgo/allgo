#!/usr/bin/python3

from   datetime import datetime, timezone, timedelta
import os.path
import shutil
import textwrap


from   django.conf import settings
from   django.core.management.base import BaseCommand
from   django.db import transaction

import config.env
from   main.models import Job

ALLGO_DATASTORE_EXPIRE_DAYS = int(config.env.ALLGO_DATASTORE_EXPIRE_DAYS)


class Command(BaseCommand):
    help = "Daily cleanup (expire old jobs and remove their files)"

    def add_arguments(self, parser):
        parser.add_argument("-n", "--dry-run", action="store_true", help="dry run")

    def handle(self, *, dry_run, verbosity, **kw):
        def log(lvl, fmt, *k):
            if lvl <= verbosity:
                print(fmt % k)
        def log_ids(lvl, query):
            if lvl <= verbosity and query.count():
                print(textwrap.fill(
                    " ".join(str(j.id) for j in query),
                    initial_indent="    ", subsequent_indent="    "))


        # select jobs older than ALLGO_DATASTORE_EXPIRE_DAYS

        if ALLGO_DATASTORE_EXPIRE_DAYS < 1:
            raise ValueError("ALLGO_DATASTORE_EXPIRE_DAYS must be at least 1 day")

        threshold = datetime.now(timezone.utc) - timedelta(days=ALLGO_DATASTORE_EXPIRE_DAYS)
        log(1, "Threshold date: %s" % threshold)

        expired_jobs = Job.objects.select_for_update().filter(updated_at__lt=threshold)

        # delete those that are terminated or not yet started
        with transaction.atomic():
            done_jobs = expired_jobs.filter(state=Job.DONE)
            new_jobs  = expired_jobs.filter(state=Job.NEW)

            log(1, "Expired jobs in state DONE to be archived: %d" % done_jobs.count())
            log_ids(2, done_jobs)

            log(1, "Expired jobs in state NEW  to be deleted:  %d" % new_jobs.count())
            log_ids(2, new_jobs)

            for job in done_jobs.union(new_jobs):
                data_dir = job.data_dir
                if os.path.isdir(data_dir):
                    if dry_run:
                        log(2, "delete job dir: %s (dry-run)", data_dir)
                    else:
                        log(2, "delete job dir: %s", data_dir)
                        shutil.rmtree(data_dir)

            if not dry_run:
                done_jobs.update(state=Job.ARCHIVED)
                new_jobs.delete()

        # walk the datastore path and issue a warning for unknown jobs
        known_job_ids = Job.objects.exclude(state=Job.ARCHIVED).values_list("id", flat=True)
        for name in sorted(os.listdir(settings.DATASTORE)):
            try:
                job_id = int(name)
            except ValueError:
                log(0, "warning: unexpected datastore item: %s/%s", settings.DATASTORE, name)
            else:
                if job_id not in known_job_ids:
                    log(0, "warning: dangling job directory: %s/%s", settings.DATASTORE, name)
