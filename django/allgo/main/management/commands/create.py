import argparse
import re
import sys

from django.core.management.base import BaseCommand, CommandParser

from main.models import User, Webapp, WebappVersion, AllgoUser, Runner, DockerOs, JobQueue
from django.contrib.auth.models import User, Group


DEFAULT_PASSWORD = "allgo"

def resolve_user(name):
    if re.match("\d$", name):
        return User.objects.get(id=int(name))
    else:
        return User.objects.get(email=name)

def resolve_webapp(name):
    if re.match("\d$", name):
        return Webapp.objects.get(id=int(name))
    else:
        return Webapp.objects.get(docker_name=name)

def resolve_group(name):
    if re.match("\d$", name):
        return Group.objects.get(id=int(name))
    else:
        return Group.objects.get(name=name)

class Command(BaseCommand):

    def add_arguments(self, parser):
        sub = parser.add_subparsers(dest="resource_type", help="resource type",
                parser_class=CommandParser)

        def add_parser(type, metavar=None, help=None, **kw):
            p = sub.add_parser(type, help=help or ("create %s" % type), **kw)
            p.add_argument("name", help="resource name", metavar=metavar)
            return p

        p = add_parser("user", metavar="EMAIL")
        p.add_argument("--admin", action="store_true", help="create superuser")
        p.add_argument("--password", default=DEFAULT_PASSWORD, help="password")
        p.add_argument("--group", metavar="GROUP|ID", help="in group")

        p = add_parser("add_sshkey", metavar="USER|ID",
                help="add sshkey to an existing user")
        p.add_argument("ssh_key", nargs=argparse.REMAINDER)

        p = add_parser("group", metavar="GROUP")

        p = add_parser("webapp", metavar="SHORTNAME")
        p.add_argument("--user", metavar="USER|ID", required=True, help="webapp owner")
        p.add_argument("--long-name", metavar="NAME", help="webapp long name")
        p.add_argument("--entrypoint", metavar="CMD", required=True, help="entrypoint")

        p = add_parser("webapp_version", metavar="TAG")
        p.add_argument("--webapp", metavar="WEBAPP|ID", required=True, help="webapp")

        p = add_parser("job_queue", metavar="NAME")
        p.add_argument("--timeout", metavar="SECONDS", type=int, help="timeout")

        p = add_parser("docker_os", metavar="IMAGE[:TAG]")

    def handle(self, *, resource_type, name=None, **kw):

        # from stdin
        if name is None:
            parser = self.create_parser("manage.py", "create")
            # line by line parsing
            for line in sys.stdin:
                argv = line.split()
                if argv:
                    args = parser.parse_args(argv)
                    if args.resource_type is None:
                        print("error: invalid command %r" % line)
                        sys.exit(1)
                    self.handle(**args.__dict__)
            return


        # ssh_key
        if resource_type == "add_sshkey":
            print("add sshkey to %s" % name)
            user = resolve_user(name).allgouser
            user.sshkey = (user.sshkey or "") + " ".join(kw["ssh_key"]) + "\n"
            user.save()
            return

        print("create %s %r" % (resource_type, name))

        # user
        if resource_type == "user":
            if kw["admin"]:
                create_func = User.objects.create_superuser
            else:
                create_func = User.objects.create_user

            # create user (in table 'auth_user')
            user = create_func(name, name, kw["password"])

            # create & validate email account
            user.emailaddress_set.create(email=name, verified=True, primary=True)

            if kw["group"]:
                user.groups.add(resolve_group(kw["group"]))

        # group
        elif resource_type == "group":
            Group.objects.create(name=name)

        # webapp
        elif resource_type == "webapp":
            user = resolve_user(kw["user"])
            Webapp.objects.create(
                    user=user,
                    docker_name = name,
                    name = kw["long_name"] or name,
                    docker_os = DockerOs.objects.first(),
                    job_queue = JobQueue.objects.get(is_default=True),
                    sandbox_state = Webapp.IDLE,
                    entrypoint = kw["entrypoint"],
                    contact = user.email,
                    private = False,
                    memory_limit = 1024**3,
                    )

        # webapp version
        elif resource_type == "webapp_version":
            WebappVersion.objects.create(
                    webapp=resolve_webapp(kw["webapp"]),
                    number=name,
                    state=WebappVersion.READY,
                    published=True,
                    )

        # job queue
        elif resource_type == "job_queue":
            queue = JobQueue.objects.create(
                    name=name,
                    timeout=kw["timeout"],
                    is_default=(JobQueue.objects.count() == 0)
                    )

        # docker os
        elif resource_type == "docker_os":
            name, version = re.match("([^:]+)(?::([^:]+))?$", name).groups()
            queue = DockerOs.objects.create(
                    name=name, docker_name=name,
                    version=version or "latest",
                    )

