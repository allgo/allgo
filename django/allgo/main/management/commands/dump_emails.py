#!/usr/bin/python3

from django.core.management.base import BaseCommand

from main.models import User

class Command(BaseCommand):
    help = "Dump user emails"

    def add_arguments(self, parser):
        grp = parser.add_mutually_exclusive_group()
        grp.add_argument("--provider", action="store_true",
                help="select only the users who are allowed to create an app")
        grp.add_argument("--guest", action="store_true",
                help="select only the users who are not allowed to create an app")

    def handle(self, *, provider, guest, **kw):
        if provider:
            pred = lambda u: u.is_provider
        elif guest:
            pred = lambda u: not u.is_provider
        else:
            pred = lambda u: True

        print("\n".join(u.email for u in filter(pred, User.objects.order_by("email"))))




