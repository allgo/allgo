from urllib.parse import quote_plus

from django.conf import settings
from django.contrib.auth.mixins import UserPassesTestMixin, LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.http import JsonResponse, Http404
from django.shortcuts import redirect

from allauth.socialaccount.providers import registry as allauth_registry

import config
from config.settings import parse_bool
from .models import Job
from .helpers import get_request_user


ALLGO_ALLOW_LOCAL_ACCOUNTS = parse_bool(config.env.ALLGO_ALLOW_LOCAL_ACCOUNTS)

# FIXME: should we validate API calls with this mixin too ? The answer is not
# obvious because it would not be too good to break the API when the ToS are
# updated (at least there should be a grace period)
#
# FIXME: right now we ensure that at least one address is validated. Should we
# force the validation of all addresses ?
class AllgoAuthMixin:
    """Common authentication mixin for allgo

    Its purpose is to:
    - validate user accounts
    - customise the login_url (so that the user is redirected to the requested page after login)
    - and adds the sign_in_url/sign_in_provider values in the context

    An account is valid if:
     - at least one of its email address is verified
     - its owner agreed the latest ToS

    In the case of the 'ProviderAccessMixin' the verified address must also
    have its domain listed in ALLGO_ALLOWED_DEVELOPER_DOMAINS.
    """

    def dispatch(self, request, *args, **kwargs):
        user = request.user
        if not user.is_anonymous:

            # registered users must have their email validated

            if isinstance(self, ProviderAccessMixin):
                # user visiting 'ProviderAccess' pages must have at least one
                # of their provider addresses validated
                email_addresses = [addr for addr in user.email_addresses
                        if addr.is_provider]

                if not email_addresses:
                    # user does not have Provider access
                    # -> return 403
                    raise PermissionDenied
            else:
                # user visiting 'UserAccess' pages must have any of their
                # addresses validated
                email_addresses = user.email_addresses

            if not any(addr.verified for addr in email_addresses):
                # user has no verified address
                return redirect("main:user_need_validation")


            # registered users must have agreed the latest ToS
            if not user.has_agreed_tos:
                return redirect("main:user_need_validation")


        return super().dispatch(request, *args, **kwargs)

    def get_login_url(self):
        if (not ALLGO_ALLOW_LOCAL_ACCOUNTS and len(providers := allauth_registry.get_list()) == 1):
            # local accounts are disabled and we have only one external auth provider
            # -> go directly to the provider's login page
            base_url = providers[0].get_login_url(self.request)
            self.__provider_name = providers[0].name
        else:
            # otherwise go to the our login page
            base_url = settings.LOGIN_URL
            self.__provider_name = None
        return f"{base_url}?next={quote_plus(self.request.get_full_path())}"

    def get_context_data(self, **kwargs):
        ctx = super().get_context_data(**kwargs)
        if self.request.user.is_anonymous:
            ctx["sign_in_url"] = self.get_login_url()
            ctx["sign_in_provider"] = self.__provider_name
        return ctx

class UserAccessMixin(LoginRequiredMixin, AllgoAuthMixin):
    """Mixin to be included in views usable by registered users only"""
    pass

class ProviderAccessMixin(LoginRequiredMixin, AllgoAuthMixin):
    """Mixin to be included in views that require provider-level access

    (i.e. user allowed to create new web applications)
    """
    pass

class AllAccessMixin(AllgoAuthMixin):
    """Mixin to be included in views usable by any user (registered or not)

    Note: the purpose of using this mixin (rather that no mixin at all) is that
    it ensures that the user registration is complete (email address
    validated). Thus the user is invited to complete the registration before
    landing to the webapp_detail page rather that when he submits his first job
    (which would be discarded)
    """
    pass



class JobAuthMixin(AllgoAuthMixin, UserPassesTestMixin):
    """Check authorization to access a given job"""

    def test_func(self):
        """Check if user has access to a job

        - redirects to the login page if unauthenticated
        - allow access if user is the job owner or if user is a superuser
        """
        user = get_request_user(self.request)
        if user is None:
            return False    # must authenticate
        job = Job.objects.only("user").filter(id=self.kwargs['pk']).exclude(state=Job.NEW).first()
        if job is None or not (user.is_superuser or user.id == job.user_id):
            raise Http404("job not found")
        elif job.state in (Job.DELETED, Job.ARCHIVED):
            self.handle_deleted_job()
        return True

    def handle_deleted_job(self):
        """Handler called when the job exists but is deleted

        Raise Http404 by default. Derived view may reimplement it as a no-op
        if they need to work on deleted jobs too (eg. to implement idempotent actions)
        """
        raise Http404("job is deleted")

    def handle_no_permission(self):
        if not self.raise_exception and self.request.path_info.startswith("/api/"):
            return JsonResponse({"error": "401 Unauthorized"}, status=401)
        return super().handle_no_permission()
