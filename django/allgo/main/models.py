from __future__ import unicode_literals
import datetime
import os

from django.conf import settings
from django.contrib import auth
from django.contrib.auth.models import User, AnonymousUser
from django.core.validators import MinValueValidator, RegexValidator, ValidationError
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.crypto import get_random_string
from jsonfield import JSONField
from taggit.managers import TaggableManager
from allauth.account.models import EmailAddress


from .validators import job_param_validator, docker_container_id_validator, \
                        docker_tag_validator, docker_name_validator, \
                        sshkey_validator, token_validator


def generate_token(length=32):
    """ Generate a random string according to its length.

    This function has been created to ensure that the function is called
    every time an object is created.

    Args:
        length (int): number of characters for the token. (default = 32)

    Returns:
        token as a string of n characters.

    """
    return get_random_string(length)

class BaseModel(models.Model):
    """Base model for all allgo models

    This base class overrides .save() to enforce validation of the model
    constraints before creating or updating an entry.

    The validation is automatically performed unless `force_insert` or
    `force_update` is true.

    see also:
    - https://www.xormedia.com/django-model-validation-on-save/
    - https://en.wikipedia.org/wiki/Fail-fast
    """
    def save(self, force_insert=False, force_update=False, **kw):
        if not (force_insert or force_update):
            self.full_clean()
        super().save(force_insert, force_update, **kw)

    class Meta:
        abstract = True


class TimeStampModel(BaseModel):
    """
    An abstract base class model that provides self-updating ``created_at`` and
    ``updated_at`` fields.
    """

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class AllgoUser(BaseModel):
    """
    Class linked to the Django user management. If there is a need to add a
    field related to a user, it should be added here and not in the auth_user
    table.
    """

    sshkey = models.TextField(null=True, blank=True,
            validators=[sshkey_validator])
    token = models.CharField(max_length=32, default=generate_token,
            validators=[token_validator])

    # Relationships
    user = models.OneToOneField(
            User, on_delete=models.CASCADE, related_name="allgouser")

    class Meta:
        db_table = 'dj_users'
        verbose_name = 'allgo user'
        verbose_name_plural = 'allgo users'

    def __str__(self):
        return self.user.username


class DockerOs(BaseModel):
    """
    Contains the different Operating Systems that a user can choose from to
    build its container.
    """

    # Fields
    name = models.CharField(max_length=255)
    version = models.CharField(max_length=255,
            validators=[docker_tag_validator])
    docker_name = models.CharField(max_length=255,
            validators=[docker_name_validator])

    class Meta:
        db_table = 'dj_docker_os'

    def __str__(self):
        return self.name + ' ' + self.version


class JobQueue(TimeStampModel):
    """
    Data regarding the type of queues available for the system.
    """

    # Fields
    name = models.CharField(max_length=255)
    timeout = models.IntegerField(blank=True, null=True,
            validators=[MinValueValidator(1)])
    is_default = models.BooleanField()

    class Meta:
        db_table = 'dj_job_queues'

    def __str__(self):
        return self.name



class Webapp(TimeStampModel):
    """
    Webapp model related to the creation and management of a particular web
    app belonging to one or more users.
    """

    #### Sandbox States ####
    IDLE = 0            # no active sandbox
    RUNNING = 1         # the sandbox is running
    STARTING = 2        # the sandbox is starting
    START_ERROR = 3     # the sandbox failed to start
    STOPPING = 4        # the sandbox is stopping
    STOP_ERROR = 5      # the sandbox failed to stop

    SANDBOX_STATE_CHOICES = (
        (IDLE, 'IDLE'),
        (RUNNING, 'RUNNING'),
        (STARTING, 'STARTING'),
        (START_ERROR, 'START_ERROR'),
        (STOPPING, 'STOPPING'),
        (STOP_ERROR, 'STOP_ERROR'),
    )

    # Notes about sandbox states
    #
    # - Possible state transitions
    #   - by django:
    #       (any) -> STARTING       (sandbox start order)
    #       (any) -> STOPPING       (sandbox stop order)
    #
    #   - by the controller:
    #       STARTING -> RUNNING     (successfully started)
    #       STARTING -> START_ERROR (start error)
    #       STOPPING -> IDLE        (successfully stopped)
    #       STOPPING -> STOP_ERROR  (stop error)
    #
    # - Both django and the controller are allowed to change the state, but
    #   django has the priority. Transitions by the controller must be performed
    #   *atomically* (compare and swap).
    #
    # - In the STARTING state, the docker image for creating the sandbox is
    #   specified by the 'sandbox_version_id' field (if not NULL), or by the
    #   'docker_os_id' (if sandbox_version_id is NULL).
    #
    # - In the STOPPING state, sandbox_version_id must be reset to NULL (to
    #   avoid later issues with foreign key constraints). If a new version has
    #   to be committed before destroying the sandbox, django may create a new
    #   WebappVersion entry in the SANDBOX state.
    #
    # - Errors (in states START_ERROR and STOP_ERROR) are generally
    #   recoverable. Django can switch back to STARTING/STOPPING for retrying
    #   the operation.
    #

    # Fields
    name = models.CharField(unique=True, max_length=255)
    description = models.TextField(blank=True, null=True)

    # Should given according to the number of users declared as admin to this
    # app
    contact = models.CharField(max_length=255, blank=True, null=True)

    # Logo
    logo_file_name = models.CharField(max_length=255, blank=True, null=True)
    logo_content_type = models.CharField(max_length=255, blank=True, null=True)
    logo_file_size = models.IntegerField(blank=True, null=True)
    logo_updated_at = models.DateTimeField(blank=True, null=True)

    # Default quota
    default_quota = models.IntegerField(blank=True, null=True)

    # Docker and parameters related stuff
    docker_name = models.CharField(max_length=255, unique=True, validators=[
        docker_name_validator,
        # 'root' and 'sshd' are reserved because webapp docker_names are mapped
        # to system users inside the ssh container
        # '_.*' is reserved because of url conflicts
        RegexValidator(r"\A(root\Z|sshd\Z|_)", inverse_match=True,
            message="This is a reserved name"),
        ])
    readme = models.IntegerField(blank=True, null=True)
    entrypoint = models.CharField(max_length=255, blank=True,
            validators=[RegexValidator(r"\A/[/\w+._-]*[\w+._-]\Z",
                "Malformatted path")])
    exec_time = models.IntegerField(blank=True, null=True)
    private = models.BooleanField(default=1)

    #FIXME: unused field
    access_token = models.CharField(max_length=255, blank=True, null=True,
            validators=[token_validator])
    sandbox_state = models.IntegerField(null=True, choices=SANDBOX_STATE_CHOICES, default=IDLE)
    sandbox_version = models.ForeignKey('WebappVersion', on_delete=models.CASCADE,
            null=True, blank=True, related_name='webappversions')
    notebook_gitrepo = models.CharField(max_length=255, blank=True, null=True)

    memory_limit = models.BigIntegerField(null=True,
            validators=[MinValueValidator(0)])

    allow_param_chars = models.CharField(max_length=16, blank=True)

    # flag indicating if this webbapp was imported from rails
    # (if True, then we can import webapp versions)
    imported = models.BooleanField(default=False)

    # Relationships

    # A webapp has one docker os type
    docker_os = models.ForeignKey(DockerOs, on_delete=models.CASCADE, related_name="webappdockeros")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="webappuser")
    job_queue = models.ForeignKey(JobQueue, on_delete=models.CASCADE, related_name="webappjobqueue")
    tags = TaggableManager()

    class Meta:
        db_table = 'dj_webapps'

    def __str__(self):
        return self.name

    @staticmethod
    def _resolve_user(actor):
        if actor is None or isinstance(actor, User):
            return actor
        elif isinstance(actor, AllgoUser):
            return actor.user
        elif isinstance(actor, Runner):
            allgo_user = actor.user
            return None if allgo_user is None else allgo_user.user
        elif isinstance(actor, AnonymousUser):
            return None
        raise TypeError()

    def is_pullable_by(self, actor, *, client_ip=None):
        """Return True if the given actor is allowed to pull an image of this webapp

        `actor` may be a User, AllgoUser, Runner, Token or None

        `client_ip` is client IP address (used for limiting admin/open_bar
        access to the adresses listed in ALLGO_ALLOWED_IP_ADMIN)
        """
        if (isinstance(actor, Runner) and actor.open_bar
                and is_allowed_ip_admin(client_ip)):
            return True

        if isinstance(actor, Token):
            # deploy tokens are just for pushing (for the moment)
            return False
        user = self._resolve_user(actor)
        if user == self.user:
            return True
        elif user is not None and user.is_superuser and client_ip:
            return is_allowed_ip_admin(client_ip)
        return False

    def is_pushable_by(self, actor):
        """Return True if the given actor is allowed to push an image of this webapp

        `actor` may be a User, AllgoUser, Runner, Token or None
        """
        if isinstance(actor, Token):
            return actor.webapp == self and actor.user is None

        return self._resolve_user(actor) == self.user

    def get_webapp_version(self, number=None):
        """Return the WebappVersion object to be used for running a job

        'number' is the requested version (provided as a string). If no version
        number is provided, then the function selects the most recent.

        This function may return None if there is no useable version
        """
        query = WebappVersion.objects.filter(webapp=self,
                state__in = (WebappVersion.READY, WebappVersion.COMMITTED))
        if number is not None:
            query = query.filter(number=number)
        return query.order_by("-state", "-id").first()



class WebappParameter(TimeStampModel):
    """
    Given parameters for a specific webapp
    """

    # Fields
    value = models.CharField(max_length=255, blank=True, validators=[job_param_validator])
    name = models.CharField(max_length=255)
    detail = models.CharField(max_length=255, blank=True, null=True)

    # Relationships
    # a Webapp parameters corresponds to one webapp
    webapp = models.ForeignKey(Webapp, related_name="webappparameters",
            on_delete=models.CASCADE)

    class Meta:
        db_table = 'dj_webapp_parameters'

class WebappVersion(TimeStampModel):
    """
    Version of a given webapp
    """

    #### WebappVersion States ####

    SANDBOX = 0         # this version not yet committed
                        #  - it is the current content of the sandbox
                        #  - there may be at most one version in SANDBOX state
                        #    per webapp

    COMMITTED = 1       # the docker image is committed but not yet pushed to the registry

    READY = 2           # the docker image is on the registry

    ERROR = 3           # an error occurred during the creation of this version
                        #  - this version is not useable
                        #  - a separate 'recovery' version may have been
                        #    committed by the controller

    DELETED = 4         # this version is deleted (either overwritten or directly deleted)
                        #  - it is no longer visible by the user, but may be restored within the
                        #    next `ALLGO_EXPUNGE_DELAY` days
                        #  - it may still be in use by a job or a sandbox

    USER = 5            # this version is being pushed directly by the user

    IMPORT = 6          # this version is being imported from rails

    SANDBOX_STATE_CHOICES = (
        (SANDBOX, 'SANDBOX'),
        (COMMITTED, 'COMMITTED'),
        (READY, 'READY'),
        (ERROR, 'ERROR'),
        (DELETED, 'DELETED'),
        (USER, 'USER'),
        (IMPORT, 'IMPORT'),
    )

    # Notes about WebappVersion states
    #
    # - Allowed state changes:
    #   - by django:
    #       (none)      -> SANDBOX,USER,IMPORT      (commit/push/import started)
    #       USER        -> READY,ERROR              (push terminated)
    #       READY       -> DELETED                  (deletion/replacement)
    #       DELETED     -> READY                    (user restore)
    #   - by the controller:
    #       SANDBOX     -> COMMITTED,ERROR          (sandbox committed)
    #       IMPORT      -> COMMITTED,ERROR          (image pulled from the origin registry) 
    #       COMMITTED   -> READY,DELETED            (image pushed to the internal registry)
    #       READY       -> DELETED                  (version replaced)
    #
    # - As soon as the version is in the SANDBOX, the version is considered to
    #   exist and to be usable for launching a job or a sandbox.
    #
    # - The process for pushing images is completely asynchronous, consequently
    #   if a version is uptated multiples times quickly, then the images may
    #   not reach the READY state in the same order. If multiple images with
    #   the same version number reach the READY state, then only the higest id
    #   is kept at the READY state, older versions are switched to DELETED.
    #   Note: switching to the READY/REPLACING state must be done atomically
    #   (by locking the db rows) because it may be done by django (when
    #   pushing) or the controller (when committing a sandbox).
    #
    # - The docker images are named as <REPO>/<Webapp.docker_name>:id<WebappVersion.id>
    #   When a version is replaced by the user, under the hood, a separate
    #   WebappVersion entry is created with a different id. Docker images are never
    #   overwritten.
    #
    # - The deletion time (when switching to the DELETED state) is recorded in
    #   the 'deleted_at' column. The user may recover deleted versions up to
    #   ALLGO_EXPUNGE_DELAY` days.

    # Fields
    number = models.CharField(max_length=255, validators=[
        docker_tag_validator,
        # 'sandbox' is a reserved name because it is used for running jobs in
        # sandboxes
        RegexValidator(r"\Asandbox\Z", inverse_match=True,
            message="This is a reserved name"),
        ])
    description = models.CharField(max_length=255, blank=True)
    docker_image_size = models.FloatField(blank=True, null=True)
    state = models.IntegerField(choices=SANDBOX_STATE_CHOICES)

    # deletion time, it is meaningful only when in the DELETED state
    # Note: old (pre-migration) deleted version have 'deleted_at=None'
    deleted_at = models.DateTimeField(blank=True, null=True)

    # True if the version is usable by other users.
    published = models.BooleanField()

    # flag indicating if this version was imported from rails
    imported = models.BooleanField(default=False)

    webapp = models.ForeignKey('Webapp', on_delete=models.CASCADE, related_name="webapp")

    class Meta:
        db_table = 'dj_webapp_versions'

    def __str__(self):
        return self.number


class Runner(TimeStampModel):
    """Allgo job runners

    Notes:
    - a runner is owned by a user and it can only run webapps that this user is
      allowed to pull
    - if `open_bar` is True and the source IP (of the http request) is listed
      in ALLGO_ALLOWED_IP_ADMIN, then this runner can pull all  images
    """
    token = models.CharField(unique=True, max_length=255, blank=False,
            default=generate_token, validators=[token_validator])
    hostname = models.CharField(max_length=255, blank=True, null=True)
    cpu_count = models.IntegerField(blank=True, null=True,
            validators=[MinValueValidator(1)])
    mem_in_GB = models.IntegerField(blank=True, null=True,
            validators=[MinValueValidator(1)])
    last_seen = models.DateTimeField(null=True)
    webapps = models.ManyToManyField(Webapp, related_name="webapps")
    open_bar = models.BooleanField(default=False)

    user = models.ForeignKey(User, null=True, on_delete=models.CASCADE, related_name="runneruser")

    class Meta:
        db_table = 'dj_runners'

    def __str__(self):
        return "%s / %s" % (self.token, self.hostname)

class Quota(TimeStampModel):
    """
    Quota given to a certain user for a given webapp.

    default quantity = 1000 KB = 1 MB

    Todo
        - write a function that calculates the quota left to the user
    """

    # Fields
    quantity = models.BigIntegerField(blank=True, null=True, default=1000)

    # Relationships
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="quotauser")
    webapp = models.ForeignKey(Webapp, on_delete=models.CASCADE, related_name="quotawebapp")

    class Meta:
        db_table = 'dj_quotas'


class Job(TimeStampModel):
    """
    Jobs ran with the specific data (user, webapp, queue, ...)
    """

    #### Job States ####

    NEW = 0         # job is being created (not yet submitted)
    WAITING = 1     # the job is submitted (waiting to be executed)
    RUNNING = 2     # job is running
    DONE = 3        # job is terminated

    ARCHIVED = 4    # job is archived (no longer visible by the user)
    DELETED = 5     # job is being deleted (transient state)
    ABORTING = 6    # job is being aborted

    # Notes about job states
    #
    # - possible state transitions:
    #   - by django:
    #       (no object) -> NEW      (job creation)
    #       NEW      -> WAITING     (job submission)
    #       NEW      -> DELETED     (deletion before submit)
    #       WAITING  -> DELETED     (deletion before job is started)
    #       WAITING  -> DONE        (user abort)
    #       RUNNING  -> ABORTING    (user abort request)
    #       DONE     -> ARCHIVED    (deletion after job completion)
    #       DELETED  -> (no object) (job removal)
    #
    #   - by the controller:
    #       WAITING  -> RUNNING     (job start)
    #       ABORTING -> DONE        (job terminated after user abort request)
    #       RUNNING  -> DONE        (job terminated, all other cases)
    #
    # - both django and the controller are allowed to change the state when the
    #   job is WAITING or RUNNING. In order to prevent race conditions, the
    #   following transitions *must* be performed atomically in the db (compare
    #   and swap):
    #       WAITING  -> RUNNING
    #       WAITING  -> DELETED
    #       RUNNING  -> ABORTING
    #
    # - from the user point of view, a deleted job (removed from the db) and
    #   and a job in the DELETED or ARCHIVED state are equivalent (job is
    #   deleted, i.e. no longer visible). The only current purpose of the
    #   ARCHIVED state is to compute webapp usage statistics (see issue #221)
    #
    # - the job data dir (in the DATASTORE path) is created after the job db
    #   entry is created (in the NEW) state, and deleted after the job states
    #   is changed to ARCHIVED/DELETED
    #
    JOB_STATE_CHOICES = (
        (NEW, 'NEW'),
        (WAITING, 'WAITING'),
        (RUNNING, 'RUNNING'),
        (ARCHIVED, 'ARCHIVED'),
        (DONE, 'DONE'),
        (DELETED, 'DELETED'),
        (ABORTING, 'ABORTING'),
    )

    ### job results ##
    #
    # The 'result' field has a meaning only when the job is in state DONE or
    # ARCHIVED. In every other states, the result is NONE.
    #
    NONE = 0        # no result yet
    SUCCESS = 1     # job terminated successfully (exit 0)
    ERROR = 2       # job error (non-zero exit, or any other error, eg: webapp
                    #            version not found)
    ABORTED = 3     # job aborted by the user
    TIMEOUT = 4     # job timeout (aborted because it exceeded the maximum
                    #              duration allowed by the job queue)

    JOB_RESULT_CHOICES = (
            (NONE, 'NONE'),
            (SUCCESS, 'SUCCESS'),
            (ERROR, 'ERROR'),
            (ABORTED, 'ABORTED'),
            (TIMEOUT, 'TIMEOUT'),
    )

    # Fields
    param = models.CharField(max_length=255, blank=True, null=True)
    datasize = models.IntegerField(blank=True, null=True)
    version = models.CharField(max_length=255)
    exec_time = models.IntegerField(blank=True, null=True)
    access_token = models.CharField(max_length=255, blank=True, null=True,
            validators=[token_validator])
    state = models.IntegerField(choices=JOB_STATE_CHOICES, default=NEW)
    result = models.IntegerField(choices=JOB_RESULT_CHOICES, default=NONE)
    container_id = models.CharField(max_length=64, blank=True, null=True,
            validators=[docker_container_id_validator])

    # Details about the input files (metadata)
    #
    # schema:    [{"name": "<FILENAME>", "size": <SIZE>}, ...]
    # Note: the list is ordered
    files = JSONField()

    # Relationships
    queue = models.ForeignKey(JobQueue, on_delete=models.CASCADE, related_name="job_queue")
    webapp = models.ForeignKey(Webapp, on_delete=models.CASCADE, related_name="job_webapp")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="user")

    runner = models.ForeignKey(Runner, on_delete=models.CASCADE, related_name="runner", blank=True, null=True)

    class Meta:
        db_table = 'dj_jobs'

    @property
    def status(self):
        """Return a textual representation of the job status

        The job status is what we display to the user (the 'state' and 'result'
        fields are internal to allgo).

        The status is the textual representation of:
        - the 'result' field if the job is terminated (success, error, timeout
          or aborted)
        - the 'state' field otherwise (new, waiting, running, aborting)
        """

        return (self.get_result_display
                if self.state in (Job.DONE, Job.ARCHIVED)
                else self.get_state_display)()


    @property
    def destroyable(self):
        """Tell if this job can be destroyed from the UI

        The current job state machine does not support aborting and deleting a
        running job in the same time. Destroying is disabled until its state is
        DONE.
        """
        return self.state not in (Job.RUNNING, Job.ABORTING)


    @property
    def data_dir(self):
        """Return the system path to the job data directory"""
        return os.path.join(settings.DATASTORE, str(self.id))


    def full_clean(self, exclude=None, validate_unique=True):
        """Specialisation of full_clean() that also runs the job_param_validator
        (which needs context)
        """
        errors = {}
        try:
            super().full_clean(exclude, validate_unique)
        except ValidationError as e:
            errors = e.update_error_dict(errors)

        if "param" not in (exclude or ()):
            try:
                job_param_validator(self.param, self.webapp.allow_param_chars)
            except ValidationError as e:
                errors.setdefault("param", []).extend(e.error_list)

        if errors:
            raise ValidationError(errors)


class Tos(BaseModel):
    """ The Terms of Service model

    It can contains several version of a same ToS. The current view will always
    look for the latest version in the model. Check the view if you have any
    doubt.

    NOTE: ToS versions are ordered by Tos.id (not Tos.version), because
    comparing version numbers with MySQL is quite a messy job. The latest ToS
    is the one with the highest id.

    """
    url =  models.URLField(blank=True)
    version = models.CharField(unique=True, max_length=32)

    def __str__(self):
        return str(self.version)

    @classmethod
    def get_latest(cls):
        """Get the most up-to-date Terms of Service"""
        return Tos.objects.order_by("-id").first()

    class Meta:
        db_table = "dj_tos"


class UserAgreement(BaseModel):
    """ User agreement model

    Link a user to one or several ToS.
    """
    tos = models.ForeignKey('Tos', on_delete=models.CASCADE, related_name='tos')
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_agreement')
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{0} - {1}".format(self.user, self.tos)

    class Meta:
        db_table = 'dj_user_agreement'


class Token(BaseModel):
    """Authentication token model
 
    Tokens are 64 bytes long and are made of:
     - a 12-byte unique id
     - a 52-byte secret

    The secrets are stored as a digest (using a password hasher), therefore the plaintext is
    displayed to the user at creation time and cannot be recovered aftefwards.

    They may have an expiration date (if null, the token never expires).

    For the moment, the tokens ae only for pushing a new WebappVersion with the docker registry API.
    """

    id = models.CharField(max_length=12, primary_key=True,
            validators=[RegexValidator(r'\A[a-zA-Z0-9]{12}\Z')])
    secret = models.CharField(max_length=128)

    name   = models.CharField(max_length=128)
    user   = models.ForeignKey(User,   null=True, blank=True, on_delete=models.CASCADE)
    webapp = models.ForeignKey(Webapp, null=True, blank=True, on_delete=models.CASCADE)

    created_at = models.DateTimeField(auto_now_add=True)
    expires_at = models.DateField(null=True, blank=True)

    _hasher = auth.hashers.PBKDF2PasswordHasher()

    @classmethod
    def generate(cls, **kw):
        """Generate a new token

        The 'id' and 'secret' fields are automatically generated.
        The extra '**kw' arguments provide the values for the other fields.

        Return a tuple with:
             - the newly created Token object
             - a string containing the plain unencrypted token
        """
        assert "id"     not in kw
        assert "secret" not in kw
        secret = get_random_string(52)
        kw["secret"] = cls._hasher.encode(secret, cls._hasher.salt())

        # Collisions on the id are very unlikely. If this happens then we retry at most 10 times.
        for _ in range(10):
            try:
                token = Token(id=get_random_string(12), **kw)
                token.save()
                return token, token.id+secret
            except ValidationError as e:
                if "id" not in e.error_dict:
                    raise
        raise ValueError("too many collisions")


    @classmethod
    def authenticate(cls, raw_token: str):
        """Authenticate with a token

        This function looks up the token in the db, verifies it and returns the relevant Token
        object.
        
        It returns None if the token is not valid (if not found, if expired or if the secret does
        not match).
        """
        token = Token.objects.filter(id=raw_token[:12]).first()
        if (    (token is not None)
                and ((token.expires_at is None) or (token.expires_at > datetime.date.today()))
                and cls._hasher.verify(raw_token[12:], token.secret)):
            return token
        return None

    class Meta:
        db_table = 'dj_tokens'
        verbose_name = 'allgo token'
        verbose_name_plural = 'allgo tokens'



@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        AllgoUser(user=instance).save()


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.allgouser.save()

@property
def email_is_provider(email_addr: EmailAddress) -> bool:
    """Return true if the email address is in the list of domains allowed to provide applications

    WARNING: the address may not be verified
    """

    try:
        _, domain = email_addr.email.split("@")
        return domain in settings.ALLOWED_DEVELOPER_DOMAINS
    except ValueError:
        # malformatted email
        return False

EmailAddress.add_to_class('is_provider', email_is_provider)

@property
def email_addresses(user: User):
    """Get the email addresses associated to a user account

    returns a db query of EmailAddress objects
    """
    return EmailAddress.objects.filter(user=user)

auth.models.User.add_to_class('email_addresses', email_addresses)


@property
def user_is_provider(user: User):
    """Return true if the user has at least one email address in the allowed
    developer domains

    Warning: this function returns True even if the email address is not
    verified
    """
    return any(addr.is_provider for addr in user.email_addresses)

# Add the `is_provider` method to the `User` model
auth.models.User.add_to_class('is_provider', user_is_provider)

@property
def has_agreed_tos(user: User):
    """Return true if the user has agreed the latest ToS (if present)"""
    tos = Tos.get_latest()
    return tos is None or UserAgreement.objects.filter(
            user=user, tos=tos).exists()

auth.models.User.add_to_class('has_agreed_tos', has_agreed_tos)

# NOTE: because there is a circular dependency between models.py and
# helpers.py, we have to do this import after 'Job' and 'Webapp' are defined
from .helpers import is_allowed_ip_admin    # pylint: disable=wrong-import-position
