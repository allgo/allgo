#!/usr/bin/python3

import logging

from django.db import transaction
from django.db.models.signals import post_save

from .helpers import notify_controller
from .models import Job, Webapp

log = logging.getLogger('allgo')

def job_post_save(instance, **kw):
    # the controller needs to be notified if the job state is set to WAITING or
    # ABORTING
    #
    # NOTE: switching state to ABORTING must be done atomically (we must ensure
    # not to switch from DONE to ABORTING)
    #
    if instance.state in (Job.WAITING, Job.ABORTING):
        transaction.on_commit(lambda: notify_controller(instance))

def webapp_post_save(instance, **kw):
    # the controller needs to be notified if the sandbox is in
    # STARTING/STOPPING states
    #
    if instance.sandbox_state in (Webapp.STARTING, Webapp.STOPPING):
        transaction.on_commit(lambda: notify_controller(instance))

post_save.connect(job_post_save, sender=Job)
post_save.connect(webapp_post_save, sender=Webapp)
