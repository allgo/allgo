from django import template
from django.urls import resolve

register = template.Library()


@register.simple_tag
def is_active(request, *urlnames):
    resolver_match = resolve(request.path)

    for urlname in urlnames:
        if resolver_match.url_name == urlname:
            return 'active'

    return ''
