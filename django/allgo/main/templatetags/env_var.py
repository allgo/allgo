from django import template

import config.env

register = template.Library()

@register.simple_tag(name="env_var")
def env_var(name):
    return getattr(config.env, name, "")
