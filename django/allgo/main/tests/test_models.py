from django.test import TestCase
from django.contrib.auth.models import User
from django.core.validators import ValidationError

from main.models import AllgoUser, Job, Webapp, WebappVersion



class TestAllgoUser(TestCase):
    def setUp(self):
        user1 = 'test1@allgo.inria.fr'
        user2 = 'test2@allgo.inria.fr'
        password = 'top_s3cr3t'
        self.user1 = User.objects.create_user(
                username=user1,
                email=user1,
                password=password,
        )
        self.user2 = User.objects.create_user(
                username=user2,
                email=user2,
                password=password,
        )

    def test_string_representation(self):
        allgouser = AllgoUser(user=self.user1)
        self.assertEqual(str(allgouser), allgouser.user.username)

    def test_meta_verbose_name(self):
        self.assertEqual(str(AllgoUser._meta.verbose_name), 'allgo user')

    def test_meta_verbose_name_plural(self):
        self.assertEqual(str(AllgoUser._meta.verbose_name_plural), 'allgo users')

    def test_token_length(self):
        allgouser = AllgoUser(self.user2)
        self.assertEqual(len(allgouser.token), 32)

class TestAllgoJobParam(TestCase):
    fixtures = ["default"]

    def create(self, **kw):
        job = Job.objects.create(webapp_id=1, version="latest", user_id=1, queue_id=1, **kw)
        job.save()
        return job

    def test_param(self):

        self.create(param="foo bar")
        self.create(param="foo 'bar'")
        self.assertRaises(ValidationError, self.create, param="foo[1] bar[2]")
        self.assertRaises(ValidationError, self.create, param="foo<bar")
        self.assertRaises(ValidationError, self.create, param="foo 'bar")

        webapp = Webapp.objects.first()
        webapp.allow_param_chars = "[]"
        webapp.save()

        self.create(param="foo bar")
        self.create(param="foo 'bar'")
        self.create(param="foo[1] bar[2]")
        self.assertRaises(ValidationError, self.create, param="foo<bar")
        self.assertRaises(ValidationError, self.create, param="foo 'bar")

