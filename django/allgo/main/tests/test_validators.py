import re

from django.core.validators import ValidationError
from django.test import SimpleTestCase

from main.validators import job_param_validator


class TestJobParamValidator(SimpleTestCase):

    def check_allowed(self, allowed: list[str], allowed_chars=""):
        for param in allowed:
            with self.subTest(param=param, allowed_chars=allowed_chars):
                job_param_validator(param, allowed_chars)

    def check_disallowed(self, disallowed: list[str], *, regex, allowed_chars=""):
        for param in disallowed:
            with self.subTest(param=param, allowed_chars=allowed_chars):
                self.assertRaisesRegex(ValidationError, regex,
                        job_param_validator, param, allowed_chars)

    def test_default_allowed_chars(self):
        self.check_allowed(["foo bar", "foo 'bar'", 'foo "bar"'])

        self.check_disallowed([
            "fo<o", "fo>o", "fo(o", "fo)o", "fo[o", "fo]o", "fo;o",
            "fo`o", "fo$o", "fo!o", "fo|o", "fo~o", "fo&o", "fo/o"],
            regex=re.escape('Forbidden symbols: <>()[];`$!|~&/'))

    def test_whitelisted_chars(self):
        self.check_allowed(
                ["foo bar", "foo 'bar'", 'foo "bar"', "fo<o"],
                allowed_chars="<")

        self.check_disallowed([
            "fo>o", "fo(o", "fo)o", "fo[o", "fo]o", "fo;o",
            "fo`o", "fo$o", "fo!o", "fo|o", "fo~o", "fo&o", "fo/o"],
            allowed_chars="<",
            regex=re.escape('Forbidden symbols: >()[];`$!|~&/'))

        self.check_allowed(
                ["foo bar", "foo 'bar'", 'foo "bar"', "fo[o", "fo]o"],
                allowed_chars="[a]b]c]")

        self.check_disallowed([
            "fo<o", "fo>o", "fo(o", "fo)o", "fo;o",
            "fo`o", "fo$o", "fo!o", "fo|o", "fo~o", "fo&o", "fo/o"],
            allowed_chars="[a]b]c]",
            regex=re.escape('Forbidden symbols: <>();`$!|~&/'))

    def test_quotes(self):
        self.check_allowed([
                ''' foo 'bar' ''',
                ''' foo "bar" ''',
                ''' foo "b'a\\"r" ''',
                ])
        self.check_disallowed([
                ''' foo 'bar ''',
                ''' foo "bar ''',
                ''' foo "ba" "r ''',
                ''' foo 'ba' 'r ''',
                ''' foo 'b"a\\'r' ''',
                ],
                regex="No closing quotation")
