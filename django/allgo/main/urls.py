# -*- coding: utf-8 -*-
"""Main url module

This module handles all URLs related to the `main` django application.

Below a short description of the different type of URLs available for
front-end.

/apps                       list all the public apps
/apps/author/<username>     list all public apps of a particular owner
/apps/tag/<tag>             list all apps according to a particular tag
/app[s]/<docker_name>       app details

/runners/                   list all runners of a given user
/runners/create             create a runner
/runners/<runner_id>/       details of a runner
/runners/<runner_id>/update update a given runner
/runners/<runner_id>/delete delete a given runner

/profile                    profile of user (name, first name, email)
/profile/password           update password
/profile/security           SSH key and token

/jobs/                      list all jobs of the user
/jobs/<job_id>/             details of a given job
/jobs/<job_id>/delete       delete the job
/jobs/<job_id>/issue        report an issue
/jobs/<job_id>/relaunch     re-run a given job

# same url schema as the API, but here display graphs.
/metrics                    form to define the graph to display

Todo:
    - make the app details available both at /app/<docker_name> (historic
      version) and at /apps/<docker_name>/ (more coherent with the overall
      system.

See also:
    This url file depends on the main `urls.py` file located in the `config`
    folder at the root of the Django application.
"""
from functools import partial
# Third party import
from django.conf.urls import url
import django.conf.urls
import django.views.defaults

# Local import
from . import views

app_name = 'main'

urlpatterns = [
    url(r'^$', views.IndexDetail.as_view(), name="home"),
    url(r'^apps/$', views.WebappList.as_view(), name='webapp_list'),
    url(r'^apps/_authors/(?P<username>[\w.@+-]+)/$', views.UserWebappList.as_view(),
        name='user_webapp_list'),
    url(r'^apps/_create/$', views.WebappCreate.as_view(), name='webapp_creation'),
    url(r'^apps/_import/$', views.WebappImport.as_view(), name='webapp_import'),
    url(r'^apps/(?P<docker_name>[\w-]+)/import$', views.WebappVersionImport.as_view(),
        name="webapp_version_import"),
    url(r'^apps/(?P<docker_name>[\w-]+)/tokens$', views.WebappTokenCreate.as_view(),
        name="webapp_token_create"),
    url(r'^apps/(?P<docker_name>[\w-]+)/tokens/(?P<token>\w+)/delete$', views.WebappTokenDelete.as_view(),
        name="webapp_token_delete"),
    url(r'^apps/(?P<docker_name>[\w-]+)/update$', views.WebappUpdate.as_view(),
        name="webapp_update"),
    url(r'^apps/(?P<docker_name>[\w-]+)/sandbox$', views.WebappSandboxPanel.as_view(),
        name="webapp_sandbox_panel"),
    url(r'^apps/(?P<docker_name>[\w-]+)/versions/$', views.WebappVersionList.as_view(),
        name="webapp_version_list"),
    url(r'^apps/(?P<docker_name>[\w-]+)/versions/(?P<pk>\d+)/update',
        views.WebappVersionUpdate.as_view(), name="webapp_version_update"),
    url(r'^apps/(?P<docker_name>[\w-]+)/json$', views.WebappJson.as_view(), name='webapp_json'),
    url(r'^apps/(?P<docker_name>[\w-]+)$', views.JobCreate.as_view(), name='webapp_detail'),

    url(r'^tags/$', views.TagList.as_view(), name='tag_list'),
    url(r'^tags/(?P<slug>[\w-]+)$', views.TagWebappList.as_view(), name='tag_webapp_list'),

    url(r'^jobs/$', views.JobList.as_view(), name='job_list'),
    url(r'^jobs/(?P<pk>\d+)$', views.JobDetail.as_view(), name='job_detail'),
    url(r'^jobs/(?P<pk>\d+)/abort$', views.JobAbort.as_view(), name='job_abort'),
    url(r'^jobs/(?P<pk>\d+)/delete$', views.JobDelete.as_view(), name='job_delete'),
    url(r'^jobs/(?P<pk>\d+)/archive$', views.JobFileDownloadAll.as_view(), name='job_download_all'),
    url(r'^jobs/(?P<pk>\d+)/download/(?P<filename>(.*))$', views.JobFileDownload.as_view(),
        name='job_download_file'),

    url(r'^profile$', views.UserUpdate.as_view(), name='user_detail'),
    url(r'^profile/token/update$', views.UserToken.as_view(), name='user_token'),
    url(r'^profile/ssh/add$', views.UserSSHAdd.as_view(), name='user_ssh_add'),
    url(r'^profile/ssh/delete$', views.UserSSHDelete.as_view(), name='user_ssh_delete'),
    url(r'^profile/password$', views.UserPasswordUpdate.as_view(), name='user_password'),
    url(r'^profile/need_validation$', views.UserNeedValidation.as_view(),
        name='user_need_validation'),

    url(r'^metrics$', views.Metrics.as_view(), name='metrics'),

    # Terms of service urls
    url(r'^tos$', views.TosDetail.as_view(), name='tos_detail'),

    #  url(r'^runners/$', views.RunnerList.as_view(), name='runner_list'),
    #  url(r'^runners/_add$', views.RunnerCreate.as_view(), name='runner_create'),
    #  url(r'^runners/(?P<pk>\d+)/update$', views.RunnerUpdate.as_view(), name='runner_update'),
    #  url(r'^runners/(?P<pk>\d+)/delete$', views.RunnerDelete.as_view(), name='runner_delete'),

    url(r'^auth$', views.auth, name="auth"),


    # important legacy URLs (from the rails implementation)
    url(r'^webapps/(?P<pk>\d+)/?$',  views.LegacyWebappDetail.as_view()),
    url(r'^app/(?P<slug>[\w-]+)/?$', views.LegacyWebappDetail.as_view()),
]


# error handlers
# NOTE: the 404 and 500 handlers are ignored when settings.DEBUG is enabled
django.conf.urls.handler400 = partial(views.error_handler,
        400, "Bad Request", django.views.defaults.bad_request)
django.conf.urls.handler403 = partial(views.error_handler,
        403, "Forbidden", django.views.defaults.permission_denied)
django.conf.urls.handler404 = partial(views.error_handler,
        404, "Not Found", django.views.defaults.page_not_found)
django.conf.urls.handler500 = partial(views.error_handler,
        500, "Server Error", django.views.defaults.server_error)
