import re
import shlex

from django.core.validators import RegexValidator, ValidationError, validate_slug


docker_container_id_validator = RegexValidator(
        r"\A[0-9a-f]{64}\Z", "Not a docker container id")

def sshkey_validator(value):
    """Validate the user sshkey field

    The most important point is to ensure that the user does not insert
    authorized_keys options like 'force_command' which would allow arbitrary
    command execution in the ssh-container.
    """

    # multiline regex for validating ssh keys in the authorized_keys format
    #
    # Allows:
    #  - blank line
    #  - comment line (starting with '#')
    #  - ssh key line (but without options)
    #
    # NOTE: this format is the format that is currently accepted by the ssh
    #       container. It supports multiple keys and allows disabling them
    #       by inserting a # character).
    #
    if not re.match(r"\A(\s*(|#.*|ssh-\w+\s+[\w\/+=]+(\s.*)?)(\r?\n|\Z))*\Z",
            value, re.ASCII):
        raise ValidationError("Malformatted ssh key")


    # the UserUpdate view attempts to decode the base64 string and raise an
    # exception if it is not well padded. We ensure that we are able to decode
    # it, otherwise the UserUpdate view is no longer usable.
    try:
        from . import helpers
        helpers.get_ssh_data(value)
    except ValueError:
        raise ValidationError("Malformatted ssh key")

# allowed pattern for docker container names
#
# source: https://github.com/docker/docker-ce/blob/master/components/engine/daemon/names/names.go
#
docker_name_validator = RegexValidator(r"\A[a-zA-Z0-9][a-zA-Z0-9_.-]+\Z",
        "Invalid docker name")

# allowed pattern for docker tag names
#
# source: https://github.com/docker/docker-ce/blob/master/components/engine/vendor/github.com/docker/distribution/reference/regexp.go       pylint: disable=line-too-long
docker_tag_validator = RegexValidator(r"\A[\w][\w.-]{0,127}\Z", flags=re.ASCII,
        message="Invalid docker tag")


DANGEROUS_PARAM_CHARS = "<>()[];`$!|~&/"

def job_param_validator(value, allowed_chars=""):
    """Validate a job param string"""
    errors = []

    # Job parameters must not contain any special character
    # interpreted by bash (because the webapp developers are
    # terrible at preventing shell injections)
    # NOTE: / is forbidden too to prevent accessing files outside
    #       the current directory (like for stealing the webapp
    #       sources)
    # FIXME: should disallow .. too
    forbidden = "".join(c for c in DANGEROUS_PARAM_CHARS if c not in allowed_chars)
    if re.search(f"[{re.escape(forbidden)}]", value):
        errors.append(ValidationError(f"Forbidden symbols: {forbidden}", code="invalid"))

    # If params contains quotes (' or ") the quoted strings must be correctly
    # terminated. The string must be parsable by shlex.split (which is used by
    # the controller)
    #
    # This validator fails if:
    # - the command contains an unterminated quote (' or ")
    # - the command ends with a \\ (incomplete escaped character)
    try:
        shlex.split(value)
    except ValueError as e:
        errors.append(ValidationError(str(e), code="invalid"))

    if errors:
        raise ValidationError(errors)

def token_validator(value):
    """Validate a secret token"""

    # short tokens are insecure
    if len(value) < 32:
        raise ValidationError("Token is too short")

    # token should not use any special character
    validate_slug(value)
