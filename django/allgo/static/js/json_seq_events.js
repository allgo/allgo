
// Start an asynchronous long-polling HTTP GET request for streaming a sequence
// of line-delimited JSON objects
//
//   url:       string                    http url
//   on_event:  function(event)           event callback
//   on_error:  function(status, reason)  error callback
//
// On HTTP error, the function calls the 'on_error' callback with the HTTP
// status code and error message.
//
// On success the function listens for incoming objects and:
//   - calls 'on_event(obj)' on each decoded json object
//   - calls 'on_event(undefined)' at stream EOF
//
// The returned value is the underlying XMLHttpRequest object.
//
function json_seq_event_listener(url, on_event=null, on_error=null)
{
	var req = new XMLHttpRequest();
	req.open("GET", url, true);
	req.send();

	var send_event = function(msg) {
		if (req.on_event != null) {
			try {
				req.on_event(msg);
			}
			catch (error) {
				console.log("error in json event handler:", error);
			}
		}
	};

	req.offset = null;
	req.on_error = on_error;
	req.on_event = on_event;
	req.onreadystatechange = function() {
		if (req.offset == null) {
			if (req.readyState >= 3) {
				if ((req.status >= 200) && (req.status < 300)) {
					// success
					req.offset = 0;
				} else {
					// HTTP error
					if (req.on_error != null) {
						req.on_error(r.status, r.statusText)
					}
					req.onreadystatechange = null;
					return;
				}
			}
		}
		
		// streaming
		while(true) {
			var end = req.responseText.indexOf("\n", req.offset);
			if (end < 0) {
				break;
			}
			var txt = req.responseText.substr(req.offset, end - req.offset);
			req.offset = end+1;
			if (end > 0) {
				try{
					var msg = JSON.parse(txt);
				}
				catch(error) {
					console.log("warning: malformatted json event:", txt);
					continue;
				}
				send_event(msg);
			}
		}
		if (req.readyState == 4) {
			if (req.offset != req.responseText.length) {
				console.log("error in json event handler: unterminated event:",
						req.responseText.substr(req.offset));
			}
			req.onreadystatechange = null;
			send_event(undefined);
		}
	};
	return req;
}
