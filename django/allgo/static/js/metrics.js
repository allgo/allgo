/* AIMS  : manage the metrics.html template.
    Base class is Metrics.
        Its constructor calls _setChartjsData which format the data to be used in Chart object.
        Then it defines the build_chart method which build the Chart object.

    CreatedPlot inherit from Metrics,
        It formats the data (in _setChartjsData) to draw a line chart.
    PerResultPlot inherit from Metrics,
        It formats the data (in _setChartjsData) to draw a bar chart.
    PerUserPlot inherit from Metrics,
        It formats the data (in _setChartjsData) to draw a bar chart.

    The function draw_plots() is called when the form is submitted.
    It queries the API for all metrics, then it build the Metrics object corresponding,
    with the data, last it builds the chart.
*/

"use strict";

// ===========================================
function get_random_color()
{
    let letters = '0123456789ABCDEF';
    let color = '#';
    for( let i = 0; i < 6; i++)
        color += letters[Math.floor(Math.random() * 16)];

    return color;
}

const BLACK = '#000000';
const RED   = '#FF0000';
const BLUE  = '#0000FF';

function fmt_date(d)
{ return new Date(d).toISOString().substring(0,10); }

function build_list( l_data, key, fn_map )
{
    /* build a set of keys from l_data records,
     * then return a sorted list of those keys,
     * where fn_map is applied on each key.
     * It is used to reformat dates.
     */

    let d_keys = {};
    for( let i=0; i<l_data.length; ++i )
    {
        let val = l_data[i][key];
        d_keys[val] = 1;
    }

    return Object.keys(d_keys).sort()
                .map( x => fn_map(x) );
}

function build_default_dict( l_keys, default_value=0 )
{
    let d_defaults = {};
    for( let i=0; i<l_keys.length; ++i )
    {   let key = l_keys[i]; d_defaults[key] = default_value;   }

    return d_defaults;
}

// ===========================================
class Metrics
{
    constructor( app_name, d_app )
    {
        // attribute to alter scale of chart. reformat date
        this.from = fmt_date(d_app['from']);
        this.to   = fmt_date(d_app['to']);
        this.step = d_app['step'];

        this.app_name = app_name;

        this.chartjs_data = null;
        this.title = null;
        this.type  = null;

        this.d_legend = { position:'top' };
        this.chartjs  = null;

        this._setChartjsData( d_app['data'] );
        //~ console.log( this.constructor.name + ": chartjs_data = " + JSON.stringify(this.chartjs_data) );
    }

    build_chart( id )
    {
        let ctx = $(id)[0].getContext('2d');

        let local_options = {
                    legend: this.d_legend,
                    title :
                    {
                        display: true,
                        text: this.title
                    },
                    scales:
                    {
                        xAxes:
                        [{
                            type: 'time',
                            time:
                            {
                                min : this.from,
                                max : this.to,
                                unit: this.step,
                                displayFormats:
                                {
                                    //~ [this.step]: 'DD-MM-YYYY'   //same display whatever the scale
                                    'year' : 'YYYY',
                                    'month': 'MM-YYYY',
                                    'day'  : 'DD-MM-YYYY'
                                }
                            },
                            ticks:
                            {
                                autoSkip: true
                            }
                        }],
                        yAxes:
                        [{
                            display: true,
                            ticks:
                            {
                                beginAtZero: true,   // minimum value will be 0.
                                precision: 0
                            }
                        }]
                    }
                };
        const options = jQuery.extend( true, {}, local_options, this.options );

        this.chartjs = new Chart( ctx,
            {
                type: this.type,
                data: this.chartjs_data,
                options: options
        });
    }
}

class PerUserPlot extends Metrics
{
    constructor( app_name, d_app )
    {
        super( app_name, d_app );
        this.title = [
                "# jobs per user for " + this.app_name + " app.",
                "Time period : " + d_app['from'] + " - " + d_app['to']
            ];
        this.type  = 'bar';

        this.d_legend.display = false;
        this.options = {
                scales: {
                        xAxes: [{ stacked: true }],
                        yAxes: [{
                                stacked: true,
                                ticks: { suggestedMax: this.max_y + 1 }
                            }]
                    }
            };
    }

    _setChartjsData( l_data )
    {
        function __dictionnarize2( l_data, key, l_periods )
        {
            // turn [ {'time_period':x, key:y, 'n':z }, ...]
            //  to { y: [{ x: z },{ x: z },...], ... }
            // d_default is the default dictionnary {any_key: default_val}
            let d_defaults = build_default_dict( l_periods, 0 );

            let d_data = {};
            let max_n  = 0;
            for( let i=0; i<l_data.length; ++i )
            {
                let tp  = fmt_date(l_data[i]["time_period"]);
                let val = l_data[i][key];
                let n   = l_data[i]["n"];

                if( !d_data[val] )
                    d_data[val] = JSON.parse(JSON.stringify(d_defaults)); // new object.

                d_data[val][tp] = n;
                if( max_n < n )
                    max_n = n;
            }

            return {
                    data: d_data,
                    max : max_n
                };
        }

        let l_periods = build_list( l_data, 'time_period', fmt_date );

        // turn [ {'time_period':tp,'uname':u,'n':n }, ...]
        //  to { u: [{tp: n},{tp: n},, ...], ... }
        let reformat_data = __dictionnarize2( l_data, 'uname', l_periods );
        this.max_y = reformat_data.max;

        let chartjs_data  = {
                // keys are dates, and they are strings, in Y-m-d format, so should work.
                labels  : l_periods,
		backgroundColor: RED,
                borderColor: RED,
                datasets: []
            };
        let random_color = 8777215; // random color, white as base
        for( let user in reformat_data.data )
        {
	    let tmp = random_color/16777216;
	    tmp =  Math.trunc((tmp - Math.trunc(tmp))*16777216);
	    let tmp_hex = "#" + tmp.toString(16);
            let d_dataset = {
                    data : l_periods.map( x => reformat_data.data[user][x] ),
                    label: user,
                    backgroundColor: tmp_hex,
                    borderColor    : tmp_hex,
                    //~ borderWidth    : 1,
					     };
	    random_color = random_color * 1.3 ; // random value
            chartjs_data.datasets.push( d_dataset );
        }

        this.chartjs_data = chartjs_data;
    }

}


class PerResultPlot extends Metrics
{
    constructor( app_name, d_app )
    {
        super( app_name, d_app );
        this.title = [
                "# jobs per result for " + app_name + " app.",
                "Time period : " + d_app['from'] + " - " + d_app['to']
            ];
        this.type  = 'bar';

        this.d_legend.display = true;
        this.options = {
                scales: {
                        xAxes: [{ stacked: true }],
                        yAxes: [{
                                stacked: true,
                                ticks: { suggestedMax: this.max_y + 1 }
                            }]
                    }
            };
    }

    _setChartjsData( l_data )
    {
        function __dictionnarize( l_data, key, l_periods, l_states )
        {
            // turn [ {'time_period':x, key:y, 'n':z }, ...]
            //  to { y: { x: z }, ... }
            // d_defaults is the default dictionnary {any_key: default_val}
            let d_defaults = build_default_dict( l_periods, 0 );

            let d_data = {};
            let max_n  = 0;
            for( let i=0; i<l_states.length; ++i )
            {
               d_data[l_states[i]] = JSON.parse(JSON.stringify(d_defaults)); // new object.
            }


            for( let i=0; i<l_data.length; ++i )
            {
                let tp  = fmt_date( l_data[i]["time_period"] );
                let val = l_data[i][key];
                let n   = l_data[i]["n"];

                d_data[val][tp] = n;
                if( max_n < n )
                    max_n = n;
            }

            return {
                    data: d_data,
                    max : max_n
                };
        }

        // SLETORT: Note : I really don't like the way I had to write 3 times the labels !
        let d_colors   = {
            "SUCCESS": '#00FF00',
            "ERROR"  : '#FF0000',
            "ABORTED": '#FF8000',
            "TIMEOUT": '#DDAA22'
        };

        let l_periods = build_list( l_data, 'time_period', fmt_date );
        let l_status  = [ "SUCCESS", "ERROR", "ABORTED", "TIMEOUT" ]; // to ensure the order.

        let reformat_data = __dictionnarize( l_data, 'result', l_periods, l_status );
        this.max_y = reformat_data.max;

        // now let's turn this dict to chartjs_data
        let chartjs_data = { labels: l_periods, datasets: [] };
        for( let i=0; i<l_status.length; ++i )
        {
            let status = l_status[i];

            chartjs_data.datasets[i] =
                {
                    data : l_periods.map( x => reformat_data.data[status][x] ),
                    label: status.toLowerCase(),

                    backgroundColor: d_colors[status],
                    borderColor    : d_colors[status],
                    borderWidth    : 1,

                    fill : false,
                };
        }

        this.chartjs_data = chartjs_data;
    }
}


class CreatedPlot extends Metrics
{
    constructor( app_name, d_app )
    {
        super( app_name, d_app );
        this.title = [
                "# jobs created for " + app_name + " app.",
                "Time period : " + d_app['from'] + " - " + d_app['to']
            ];
        this.type  = 'line';
        this.d_legend.display = false;
        this.options = {
                lineTension: 0,
                scales: {
                        yAxes: [{
                            ticks: { suggestedMax: this.max_y + 1 }
                        }]
                    }
            };
    }

    _setChartjsData( l_data )
    {
        let l_periods = build_list( l_data, 'time_period', fmt_date );

        let d_data = {};
        this.max_y = 0;
        for( let i=0; i<l_data.length; ++i )
        {
            let tp  = fmt_date( l_data[i].time_period );
            let val = l_data[i].n;
            d_data[tp] = val; // by construction there should be no collision.

            if( this.max_y < val )
                this.max_y = val;
        }

        let color = RED;
        let chartjs_data = {
                labels: l_periods,
                datasets: [{
                    data:[],
                    lineTension: 0,
                    backgroundColor: color,
                    borderColor: color,
                    fill: false
                }]
            };

        for( let i=0; i<l_periods.length; ++i )
        {
            let tp = l_periods[i];
            chartjs_data.datasets[0]['data'].push( d_data[tp] );
        }

        this.chartjs_data = chartjs_data;
    }
}


function build_API_url( which_chart )
{
    // get valuable info from form
    let app_id      = $('#app').val();
    let step        = $('#step').val();
    let from        = $('#from').val();
    let to          = $('#to').val();

    // build the API URL
    let api_url  = "api/v1/metrics"
                    + "/" + which_chart
                    + "/" + app_id
                    + "?step=" + step;
    if( from )
        api_url += "&from=" + from;
    if( to )
        api_url += "&to=" + to;
    console.log( "API URL : " + api_url );

    return api_url;
}


function export_json(data, app_name) 
{
    let l_data = data['data'];

    //  {"app_name": {
    //     result:ERROR,$
    //     uname:admin,
    //     time_period:2020-11-01
    //    } }
    let json_val = "{ \""+app_name+"\": { \"data\": [";

    for( let i=0; i<l_data.length; ++i )
        {
	    json_val = json_val + "{ \"result\" : \""+ l_data[i]["result"] + "\", ";
	    json_val = json_val + "\"uname\" : \""+ l_data[i]["uname"] + "\", ";
	    json_val = json_val + "\"time_period\" : \""+ l_data[i]["time_period"] + "\"}";
	    if (i+1 < l_data.length) {
		json_val = json_val + ", ";
	    }
	}
    json_val = json_val + "] } } ";
    return json_val;
}

function export_csv(data, app_name) 
{
    let l_data = data['data'];

    // HEADER: app_name","result","uname","time_period"
    let csv_val = "\"app_name\", \"result\", \"uname\", \"time_period\" \n";

    for( let i=0; i<l_data.length; ++i )
        {
	    csv_val = csv_val + "\"" + app_name +
		"\", \"" + l_data[i]["result"] +
		"\", \"" + l_data[i]["uname"] +
		"\", \"" + l_data[i]["time_period"] +
		"\" \r\n";
	}
    return csv_val;
}

function draw_plots()
{
     let app_id = $('#app').val();
     let D_CHARTS = {
       // api keyword : [ html elt id, js class ]
       'per_user' : [ '#per_user_plot_' + app_id, PerUserPlot ],
       'per_result': [ '#per_result_plot_' + app_id, PerResultPlot ],
       'created' : [ '#created_plot_' + app_id, CreatedPlot ],
       'all' : [ '', null ]
      };

      for( let which_chart in D_CHARTS )
    {
        let API_URL = build_API_url( which_chart );
        let http_headers = {
                'Authorization': 'Token token='+$('#token').val(),
                'Accept': 'application/json'
            };

        let init = {
                    method: "GET",
                    headers: new Headers(http_headers),
                    mode: 'cors'
                };
        let req = new Request( API_URL, init );

        //~ let options = { 'Authorization': "Token token=ss3ksGJWDIyKndXJU7o2LfIVyqnIsFNb"};
        //~ fetch(API_URL, options).then(function(response)
        fetch(req).then(function(response)
        {
            if( !response.ok )
            {
                let msg = "There was a problem querying the API.\n";
                msg += "reponse status : " + response.status;
                msg += " (" + response.statusText + ").";
                console.log(msg);
                //~ $("#metric_plot").value = "Problem with the API. Can't get the data.";
            }
            else
            {
                response.json().then(function(json)
              {
		    // code for json export
                    var json_all; 
                    var csv_all; 
		    var app_name;
		  
                    // code for ChartJS
                    for( let chart_ref in json )
                    {
                        let [id,cstr ] = D_CHARTS[which_chart];
			app_name = chart_ref;
			
			// json and csv export for one dataset
			if (which_chart == "all") {
			    // json export
			    json_all = export_json(json[chart_ref],app_name);
			    // csv export
                            csv_all = export_csv(json[chart_ref],app_name);
			    // Build export links
			    var blob_json = new Blob([json_all], {type: "text/json"});
			    var url_json  = URL.createObjectURL(blob_json);
			    $('#export_json_'+app_id).attr('href', url_json);
			    $('#export_json_'+app_id).attr('download', app_name + ".json");

			    var blob_csv = new Blob([csv_all], {type: "text/json"});
			    
			    // Build export link
			    var url_csv  = URL.createObjectURL(blob_csv);
			    $('#export_csv_'+app_id).attr('href', url_csv);
			    $('#export_csv_'+app_id).attr('download',  app_name + ".csv");
			}
			
                        if (json[chart_ref].data.length > 0) {
                          if ( cstr ) {
                            let o_plot = new cstr( chart_ref, json[chart_ref] );
                            o_plot.build_chart( id );
                            D_CHARTS[which_chart][2] = o_plot.chartjs;
			  }
			}
			else {      
                          var canvas = $(id)[0];
			  if (canvas)  {
              		    if (canvas.getContext) {
 			      var ctx = canvas.getContext('2d');
  			      ctx.font = 'italic 32px sans-serif';
			      ctx.fillText('No data', 10, 50); 
  			    }
			  }
                        }
                    }  
				 },
                function( reason )
                {
                    // error in response.json() promise
                    console.log(reason);
                });
            }
        });
    } // for which_chart

    return false;
}

