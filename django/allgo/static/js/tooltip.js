/*
 * Tooltip
 *
 * Activate a function of bootstrap to add a tip to any html tag such as 
 * links.
 */
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
