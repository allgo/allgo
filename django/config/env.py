import os
from . import env_loader

with env_loader.EnvironmentVarLoader(__name__, "ALLGO_",
                                     {"{ENV}": os.getenv("ENV", "ENV_IS_UNSET")}) as env_var:
    #
    # core django config
    #

    env_var("ALLGO_SECRET_KEY_PATH", protected=True,
            default="/vol/cache/allgo/secret_key",
            help="""path where the django secret key is stored

            This key is generated automatically at startup and rotated every
            `ALLGO_SECRET_KEY_DAYS` days
            """)

    env_var("ALLGO_SECRET_KEY_DAYS",
            default="30",
            help="""lifetime of the django secret key in days

            Note: the regeneration of the key happens only at django startup
            time (i.e. django needs to be restarted to regenerate the key)
            """)

    env_var("ALLGO_DEBUG",
            default="False",
            help="enable debugging")

    env_var("ALLGO_ALLOWED_HOSTS",
            default="localhost,127.0.0.1,{ENV}-django",
            help="""list of hostnames that this server can serve over HTTP

            In production, this value must be set to the fully qualified domain
            name where this allgo instance is reachable.
            """)

    env_var("ALLGO_ADDITIONAL_APPS", default="",
            help="comma-separated list of additional django apps to be enabled")

    env_var("ALLGO_STATIC_PATH", protected=True,
            default="/var/www/html/static",
            help="path when the static files are stored for deployment")

    env_var("ALLGO_MEDIA_PATH", protected=True,
            default="/vol/rw/media",
            help="path where the user-uploaded files are stored")

    env_var("ALLGO_SITE_ID",
            default="4",
            help="default site ID, used by some third-party application")

    env_var("ALLGO_DATASTORE", protected=True,
            default="/vol/rw/datastore",
            help="path where the jobs files are stored")

    env_var("ALLGO_DATASTORE_EXPIRE_DAYS",
            default="30",
            help="number of days jobs files are kept (after job completion)")

    env_var("ALLGO_ALLOWED_IP_ADMIN",
            default="127.0.0.1/32",
            help="Comma-separated list of IP networks from where admin tokens (for the open-bar"
            " runners and for the controller) can be requested")

    #
    # authentication
    #
    env_var("ALLGO_AUTH_GITLAB_URL", default="https://gitlab.inria.fr",
            help="Url of the gitlab identity provider")


    #
    #  runner
    #

    env_var("ALLGO_DJANGO_MAXUPLOADSIZE", protected=False,
            default="16384",
            help="max upload size of chunk sended by runner as results")

    env_var("ALLGO_DJANGO_MAXUPLOADSIZE", protected=False,
            default="16384",
            help="max upload size of chunk sended by runner as results")

    env_var("ALLGO_DJANGO_DOWNLOAD", protected=False,
            default="False",
            help="download file using django or only send an http 200 signal to nginx")

    #
    #   jupyter
    #

    env_var("ALLGO_JUPYTER_URL", protected=False,
            default="http://0.0.0.0:8000/hub/login",
            help="Url user to redirect allgo user to jupyter notebook")



    env_var("ALLGO_REDIS_HOST", protected=True,
            default="{ENV}-redis",
            help="redis host")

    #
    # email
    #

    env_var("ALLGO_EMAIL_BACKEND",
            default="django.core.mail.backends.smtp.EmailBackend",
            help="django backend for sending emails")

    env_var("ALLGO_EMAIL_FROM", default="no-reply@allgo.inria.fr",
            help="sender e-mail address for outgoing mails")

    env_var("ALLGO_EMAIL_HOST", default="smtp.inria.fr", help="host name of the SMTP relay")
    env_var("ALLGO_EMAIL_PORT", default="25", help="tcp port of the SMTP relay")
    env_var("ALLGO_EMAIL_USER", default="", help="user name for the SMTP relay")
    env_var("ALLGO_EMAIL_PASSWORD", default="", help="password for the SMTP relay")
    env_var("ALLGO_EMAIL_TLS", default="False", help="use SMTP over TLS")

    #
    # database
    #

    env_var("ALLGO_DATABASE_ENGINE", protected=True,
            default="django.db.backends.mysql",
            help="django database engine")

    env_var("ALLGO_DATABASE_NAME", protected=True,
            default="allgo",
            help="database name")

    env_var("ALLGO_DATABASE_USER", protected=True,
            default="allgo",
            help="database user name")

    env_var("ALLGO_DATABASE_PASSWORD",
            default="allgo",
            help="database password")

    env_var("ALLGO_DATABASE_HOST", protected=True,
            default="{ENV}-mysql",
            help="database host name")

    env_var("ALLGO_DATABASE_MODE", protected=True,
            default="STRICT_ALL_TABLES",
            help="""
            database sql mode

            see: https://mariadb.com/kb/en/library/sql-mode/
            """)

    #
    # allgo-specific variables
    #

    # note: this variable is not used inside django. It is listed just for
    # documentation purpose
    env_var("ALLGO_HTTP_SERVER", protected=True,
            default="gunicorn",
            help="""selection of the HTTP server running allgo

            Possible values are ``gunicorn`` and ``django``.

            * ``gunicorn`` runs the gunicorn server, with the logs sent into
              `/vol/log/django/`.

            * ``django`` runs django's native server (`django-admin
              runserver`), with logs sent to stdout/stderr. It should never be
              used in production.
            """)

    env_var("ALLGO_CONTROLLER_HOST", protected=True,
            default="{ENV}-controller",
            help="Hostname of the allgo controller")

    env_var("ALLGO_CONTROLLER_PORT", protected=True,
            default="4567",
            help="TCP port of the allgo controller (for the notifications)")

    env_var("ALLGO_CONTROLLER_TOKEN_PATH", protected=True,
            default="/vol/ro/controller_token",
            help="Path to the file containing the controller token")

    env_var("ALLGO_REGISTRY_PRIVATE_URL",
            default="http://{ENV}-registry:5000",
            help="Backend URL of the registry")


    env_var("ALLGO_SSH_HOST", default="localhost",
            help="hostname where allgo is reachable by ssh")

    env_var("ALLGO_SSH_PORT", default="2222",
            help="tcp port where allgo is reachable by ssh")

    env_var("ALLGO_SSH_PROXYJUMP", default="",
            help="""Intermediate ssh jumps (comma-separated) for connecting from an external
            location (ssh option 'ProxyJump')""")

    env_var("ALLGO_ALLOWED_DEVELOPER_DOMAINS",
            default="localhost",
            help="allowed domains to create applications")

    env_var("ALLGO_WEBAPP_DEFAULT_MEMORY_LIMIT_MB", default=str(1024),
            help="default memory limit (in megabytes) for newly created webapps")

    env_var("ALLGO_IMPORT_URL", default="https://allgo.inria.fr",
            help="url of the legacy allgo instance (for importing webapps)")
    env_var("ALLGO_IMPORT_REGISTRY", default="cargo.irisa.fr:8003/allgo/prod/webapp",
            help="registry of the legacy allgo instance (for importing webapps)")

    env_var("ALLGO_ALLOW_LOCAL_ACCOUNTS", protected=True, default="False",
            help="""Allow signing in with a local account (for development
            purpose only)""")

    env_var("ALLGO_HELPDESK_URL", default="https://helpdesk.inria.fr/categories/227/submit",
            help="""Helpdesk URL for the ALLGO service""")

    env_var("ALLGO_BUG_TRACKER_URL", default="https://gitlab.inria.fr/allgo/allgo.inria.fr/issues",
            help="""Bug tracker URL for ALLGO service""")

    env_var("ALLGO_CONTACT", default="mailto:allgo@inria.fr",
            help="""Email contact for ALLGO service""")

    env_var("ALLGO_EXPUNGE_DELAY", default="30",
            help="""Number of days a WebappVersion stays in the registry after deletion.
            A deleted version can be restored by its owner as long as this limit is not reached.""")

    #
    # allgo authentication tokens
    #

    env_var("ALLGO_TOKEN_SIGNING_KEY_PATH",
            default="/vol/ro/certs/tokens.key",
            help="path of the secret key (PEM file) for signing authentication tokens")

    env_var("ALLGO_TOKEN_SIGNING_KEY_TYPE", protected=True,
            default="RSA",
            help="""type of the secret key for signing authentication tokens

            For the moment, only 'RSA' is supported.
            """)

    env_var("ALLGO_TOKEN_SIGNING_KEY_ALG",
            default="RS256",
            help='RFC 7515 "alg" parameter (signature algorithm)')

    env_var("ALLGO_TOKEN_ISSUER",
            default="allgo_oauth",
            help='RFC 7519 "iss" parameter (identifies the principal that issuset the token)')

    env_var("ALLGO_TOKEN_EXPIRATION",
            default="3600",
            help='RFC 7519 "exp" parameter (lifetime of authentication tokens in seconds')
