import json
import logging
import os
import re
import sys
import textwrap

log = logging.getLogger('allgo')

class EnvironmentVarLoader:
    class UNSET:
        pass

    def __init__(self, module_name, prefix, replace=None):
        """new environment var loader

        `module_name`   is the name of the module where the environment
                        variables will be stored
        `prefix`        is the common prefix of all env variables
        `replace`       is a mapping of strings that must be replaced in the
                        value of loaded variables
        """
        self.module = sys.modules[module_name]
        self.prefix = prefix
        self.replace = {} if replace is None else replace

        # list of env vars for doc generation
        # [(name, protected, default, help), ...]
        self.lst = []

        # set of unseen variables to issue a warning if the user sets an
        # unknown env variable)
        self.unseen = {v for v in os.environ if v.startswith(prefix)}

    def __call__(self, name, default=UNSET, *, protected=False, help): # pylint: disable=redefined-builtin
        """load the env var `name`

        `default`   is the default value for this variable if not present in
                    the environment
        `protected` boolean
                    - False: this variable is tunable by the user
                    - True:  this is a "protected" variable. It is not
                      recommended to change its value because it may break
                      something.
        Notes:
        - occurences of "{ENV}" are expanded with os.environ["ENV"]
        - the result as a attribute of self.module
        - an unset value
        """

        assert name.startswith(self.prefix) and re.match(r"[A-Z0-9_]+\Z", name)
        self.unseen.discard(name)

        # store the info for the doc generation
        self.lst.append((name, protected, default, help)) # pylint: disable=redefined-builtin

        # read the environment variable
        value = os.getenv(name, default)
        if value is self.UNSET:
            raise RuntimeError("environment variable %r must be set" % name)

        # apply the substitutions
        for orig, repl in self.replace.items():
            value = value.replace(orig, repl)

        # store the value as a module attribute
        setattr(self.module, name, value)

    def __enter__(self):
        return self

    def __exit__(self, *exc_info):
        """finish the loader task

        - log the current config
        - warn about unknown variables
        - update the docstring (for the generation of the sphinx doc)
        """

        # log the current configuration
        for name, *_ in self.lst:
            log.info("%s=%r", name, getattr(self.module, name))

        # warn if we have unknown variables
        for name in sorted(self.unseen):
            log.warning("unexpected environment variable: %s", name)

        # generate the doc
        self._patch_docstring()

    def _patch_docstring(self):
        doc = [self.module.__doc__ or "", "\n"]

        fmt_value = lambda v: "**(unset)**" if v is self.UNSET else (
                "``'%s'``" % v)

        # summary tables
        def output_table(title, iterator):
            nonlocal doc
            doc += [ "\n\n.. csv-table :: %s" % title,
                    '\n :header: "name", "default value"',
                    '\n']
            for name, _protected, default, _help in iterator:
                doc.append("\n %s, %s" % (name, json.dumps(fmt_value(default))))
            doc += ["\n\n\n"]

        output_table("list of allgo env variables that are tunable",
                filter(lambda x: not x[1], self.lst))
        output_table("list of allgo variables that should not be modified",
                filter(lambda x: x[1], self.lst))

        # detailed doc
        for name, _protected, default, hlp in self.lst:
            doc.append("\n\n.. envvar:: %s\n\n default: %s\n\n%s\n\n"
                    % (name, fmt_value(default),
                        textwrap.indent(textwrap.dedent(hlp).strip(), " ")))

        self.module.__doc__ = "".join(doc)
