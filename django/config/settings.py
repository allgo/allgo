import logging
import os
import time

from django.contrib.messages import constants as messages

from . import env

ROOT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
APPS_DIR = os.path.join(ROOT_DIR, 'allgo')


#FIXME: we should rather do these type conversions in config/env.py because
#       this is very error prone
def parse_bool(value: str):
    if value.lower() in ("1", "true"):
        return True
    if value.lower() in ("0", "false", ""):
        return False
    raise ValueError("invalid value %r (expected 'true' or 'false')" % value)


# load and possibly regenerate the secret key
def load_secret_key(path, expire):
    key = None
    try:
        mtime = os.path.getmtime(path)
        if mtime <= time.time() <= mtime + expire*86400:
            key = open(path).read()
        else:
            os.unlink(path)
    except FileNotFoundError:
        pass

    if key is None:
        logging.getLogger("allgo").info("regenerating a new secret key")
        key = os.urandom(128).hex()
        try:
            old_umask = os.umask(0o0077)
            with open(path, "w") as out:
                out.write(key)
        finally:
            os.umask(old_umask)

    assert len(key)>=256
    return key


# REQUIRED SETTINGS
# ------------------------------------------------------------------------------

SECRET_KEY = load_secret_key(env.ALLGO_SECRET_KEY_PATH, int(env.ALLGO_SECRET_KEY_DAYS))


# GENERAL
# ------------------------------------------------------------------------------
DEBUG = parse_bool(env.ALLGO_DEBUG)
ALLOWED_HOSTS = env.ALLGO_ALLOWED_HOSTS.split(",")

# Allgo is expected to be hosted behind a reverse proxy, thus we trust the
# X-Forwarded-Host header set by the proxy
USE_X_FORWARDED_HOST = True


TIME_ZONE = 'UTC'
LANGUAGE_CODE = 'en-us'
SITE_ID = env.ALLGO_SITE_ID
USE_I18N = True
USE_L10N = True
USE_TZ = True


# DATABASES
# ------------------------------------------------------------------------------
DATABASES = {
        'default': {
            'ENGINE':   env.ALLGO_DATABASE_ENGINE,
            'NAME':     env.ALLGO_DATABASE_NAME,
            'USER':     env.ALLGO_DATABASE_USER,
            'PASSWORD': env.ALLGO_DATABASE_PASSWORD,
            'HOST':     env.ALLGO_DATABASE_HOST,
            'OPTIONS': {'sql_mode': env.ALLGO_DATABASE_MODE},
            'TEST': {
                'NAME': 'allgo_test',
                'CHARSET': 'utf8',
                'COLLATE': 'utf8_general_ci',
            },
        },
}
DATABASES['default']['ATOMIC_REQUESTS'] = True


# URLS
# ------------------------------------------------------------------------------
ROOT_URLCONF = 'config.urls'
WSGI_APPLICATION = 'config.wsgi.application'
APPEND_SLASH = False

# APPS
# ------------------------------------------------------------------------------

DJANGO_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sites',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
]
THIRD_PARTY_APPS = [
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.gitlab',
    'django_extensions',
    'jsonfield',
    'taggit',
]
LOCAL_APPS = [
    'main',
    'api.v1',
    'jwt'
]

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS + list(
        filter(bool, env.ALLGO_ADDITIONAL_APPS.split(",")))


# MIGRATIONS
# ------------------------------------------------------------------------------
MIGRATION_MODULES = {
        'sites': 'django.contrib.sites.migrations'
}


# AUTHENTIFICATION
# ------------------------------------------------------------------------------
AUTHENTIFICATION_BACKENDS = [
        'allauth.account.auth_backends.AuthenticationBackend',
]
LOGIN_REDIRECT_URL = 'main:home'
LOGOUT_REDIRECT_URL = 'main:home'


# PASSWORDS
# ------------------------------------------------------------------------------
PASSWORD_HASHERS = [
        'django.contrib.auth.hashers.PBKDF2PasswordHasher',
        'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
        'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
        'django.contrib.auth.hashers.BCryptPasswordHasher',
]
AUTH_PASSWORD_VALIDATORS = [
        {
            'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
        },
        {
            'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
        },
]


# MIDDLEWARE
# ------------------------------------------------------------------------------
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]


# STATIC
# ------------------------------------------------------------------------------
STATIC_ROOT = env.ALLGO_STATIC_PATH
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(APPS_DIR, 'static'),
]
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]


# MEDIA
# ------------------------------------------------------------------------------
MEDIA_ROOT = env.ALLGO_MEDIA_PATH
MEDIA_URL = '/media/'


# TEMPLATES
# ------------------------------------------------------------------------------
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(APPS_DIR, 'templates'),
        ],
        'OPTIONS': {
            'debug': DEBUG,
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.media',
                'django.template.context_processors.i18n',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]


# EMAIL
# ------------------------------------------------------------------------------
EMAIL_BACKEND = env.ALLGO_EMAIL_BACKEND
DEFAULT_FROM_EMAIL = env.ALLGO_EMAIL_FROM
EMAIL_HOST = env.ALLGO_EMAIL_HOST
EMAIL_PORT = env.ALLGO_EMAIL_PORT
EMAIL_HOST_USER = env.ALLGO_EMAIL_USER
EMAIL_HOST_PASSWORD = env.ALLGO_EMAIL_PASSWORD
EMAIL_USE_TLS = parse_bool(env.ALLGO_EMAIL_TLS)


# ADMIN
# ------------------------------------------------------------------------------
ADMIN_URL = r'^admin/'
ADMINS = [
    ("""Matthieu Berjon""", 'matthieu.berjon@inria.fr'),
]
MANAGERS = ADMINS

# Allauth
# ------------------------------------------------------------------------------
ACCOUNT_AUTHENTICATION_METHOD = 'username_email'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_EMAIL_SUBJECT_PREFIX = '[A||go] '
ACCOUNT_CONFIRM_EMAIL_ON_GET = True
ACCOUNT_ADAPTER = 'allgo.main.adapter.AccountAdapter'
ACCOUNT_PRESERVE_USERNAME_CASING = False  # force lowercase on username

SOCIALACCOUNT_PROVIDERS = {
    'gitlab': {
        'GITLAB_URL': env.ALLGO_AUTH_GITLAB_URL,
        'SCOPE': ['read_user'],
    },
}

ALLOWED_DEVELOPER_DOMAINS = env.ALLGO_ALLOWED_DEVELOPER_DOMAINS.split(",")

# Logging
# ------------------------------------------------------------------------------
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '{levelname} {asctime} {module} {process:d} {thread:d} {message}',
            'style': '{',
        },
        'simple': {
            'format': '{levelname} {message}',
            'style': '{',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
    },
    'loggers': {
        'allgo': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
        'jwt': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        }
    },
}

# DATASTORE
# ------------------------------------------------------------------------------
DATASTORE = os.environ.get('ALLGO_DATASTORE', default='/vol/rw/datastore')

MESSAGE_TAGS = {
    messages.ERROR: 'danger'
}
