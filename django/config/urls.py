from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views import defaults as default_views


urlpatterns = [
    # Django admin
    url(settings.ADMIN_URL, admin.site.urls),

    # Allgo stuff here
    url(r'', include('main.urls')),
    url(r'', include('jwt_auth.urls')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^api/v1/', include('api.v1.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += [
        url(r'^400/$',
            default_views.bad_request,
            kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$',
            default_views.permission_denied,
            kwargs={'exception': Exception('Permission denied!')}),
        url(r'^404/$',
            default_views.page_not_found,
            kwargs={'exception': Exception('Page not found!')}),
        url(r'^500/$', default_views.server_error),
    ]
