#! /bin/bash

# TODO: the command should be placed in the Makefile or an official doc.
# cf https://django-extensions.readthedocs.io/en/latest/graph_models.html
docker exec dev-django python3 ./manage.py graph_models --all-applications --group-models --output docs/allgo_model.png
