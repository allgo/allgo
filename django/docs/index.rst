Welcome to Allgo
================

`Allgo`_ is a platform for building deploying apps that analyze massive data in
Linux containers, it has been specifically designed for use in scientific
applications. This documentation is related to its front-end. Please see the
:ref:`installation` to get started.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   readme
   installation

Additional Notes
----------------

Design notes, legal information and changelog are here for the interested.

.. toctree::
   :maxdepth: 1

   changelog
   license

.. _Allgo: http://allgo.inria.fr/

