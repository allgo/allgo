.. _installation:

Installation
============

This Django application can be run locally or through a django container. The
application has one set of settings that can be overriden
(:ref:`environment-variable-label`) and satisfies a production type
configuration (:ref:`production-environment-label`) .


Dependencies
------------

Mandatory
^^^^^^^^^

- python 3.4 or newer
- Django 1.11
- Django-allauth 0.35.0
- mysql v15.1 or newer
- python3-mysql

Optional
^^^^^^^^

For development purposes you can install optional dependencies such as
Sphinx. They can be installed through pip.

.. code-block:: bash

  pip install sphinx


Development environment
-----------------------

The configuraton of Allgo is set using environment variables and there is a
default value for most of them.

Some variables are overriden in the `docker-compose.yml` so as to provide an
environment suitable for development.

For a detailled list of all environment variables, please refer to
:ref:`environment-variable-label`.

.. code-block:: bash

  # run the application
  python manage.py runserver

  # The application can be reached at http://localhost:8000

.. todo::
  
  ensure the use of https protocol by generating or using appropriate
  certificates. More information at
  https://docs.djangoproject.com/fr/1.11/topics/security/

Docker
^^^^^^

When the application is launched within the docker container, it can be reached
at https://localhost/django.

.. warning::

  The environement variables can be declared wherever you feel the best and
  can be managed at the docker-compose level for example.

Database
^^^^^^^^

.. warning::

  because we depend at the moment on a legacy database we have a specific 
  setup for Django. We have chose to recreate the database into Django and by 
  integrating the database constraints (that are managed by Ruby on Rails and
  not the SGDB).

At the moment the django docker container take care of the migration by calling
the migration script. The migration process consist of two files located int
`tools` folder:

- `migration2django.sql`: sql file executing the migration
- `migration.sh`: script executing the migration and setting up the Django
  application

You can run manually the migration using `migration.sh`:

.. code-block:: bash
  
  # If exists, will delete django app related tables
  ./tools/migration.sh -d USER [PASSWORD] HOST DB

  # Dump the current rails tables
  ./tools/migration.sh -e USER [PASSWORD] HOST DB

  # Create Django tables, execute the migration, set up django
  ./tools/migrations.sh -m USER [PASSWORD] HOST DB

  # Help
  ./tools/migrations.sh -h

The migration script works only on Unix related operating system as it saves
the database tables in the `/tmp` folder.

.. _production-environment-label:

Production environment
-----------------------

By default, the `config/env.py` is setup for a production config and
requires to setup at minimum one environment variable:

- `ALLGO_ALLOWED_HOSTS` to be set to the hostname where this allgo instance is
  reachable


Docker setup
^^^^^^^^^^^^

The actual docker setup relies on a Debian stretch image and Stretch backport.
The different configuration file for the docker file such as the nginx
configuration in the `setup/dk` directory. This includes:

- `allgo.conf`: nginx configuration for the django docker
- `container_init`: initialisation of the container
- `nginx.patch`: main nginx configuration
- `run-allgo`: bash script creating the necessary directories and running the
  different services necessary for the application

.. _environment-variable-label:

Environment variables
---------------------

.. warning::

  All environment variables **must** be prefixed by `ALLGO`.

.. automodule:: config.env

