from django.conf.urls import url
from . import views

app_name = 'jwt'

urlpatterns = [
    # oauth endpoint to get a token for the docker registry
    url(r'^jwt/auth$', views.jwt_auth, name="jwt_auth"),

    # hooks for registry pull/push for image manifests
    url(r'^jwt/pre-(push|pull)$', views.pre_pushpull,  name="pre_pushpull"),
    url(r'^jwt/post-push$', views.post_push, name="post_push"),

    # default catch-all route for docker registry urls (normally unused because
    # the reverse-proxy is expected to route them directly to the registry)
    url(r'^v2/', views.registry_notfound),
]
