import base64
import logging

import config.env
from django.http import JsonResponse, HttpResponse
import django.utils
from django.views.decorators.csrf import csrf_exempt
from main.models import User, Runner, Token, Webapp, WebappVersion
from main.helpers import is_allowed_ip_admin

from .tokens import encode_jwt_token

log = logging.getLogger('jwt')

# tokens below this size will automatically be rejected (to prevent any
# misconfiguration)
MIN_TOKEN_SIZE = 32


# on startup read the controller token from ALLGO_CONTROLLER_TOKEN_PATH
def _read_controller_token():
    path = config.env.ALLGO_CONTROLLER_TOKEN_PATH
    try:
        with open(path) as fp:
            token = fp.read().strip()
        return token
    except OSError as e:
        log.warning("failed to get the controller token at %r (%s)", path, e)
CONTROLLER_TOKEN = _read_controller_token()

@csrf_exempt
def pre_pushpull(request, action):
    """pre-hook for pushing/pulling image manifests

    This endpoint is called by allgo.aio before pushing/pulling an image to the
    registry.

    it returns a 200 response with the WebappVersion.id in the body if
    successful
    """

    if request.META.get("HTTP_X_ORIGIN") != "aio":
        # this endpoint is only usable by allgo.aio
        return HttpResponse(status=404)
    if request.method != "POST":
        return HttpResponse(status=405)

    repo = request.GET["repo"]
    tag  = request.GET["tag"]
    description = request.GET["description"]

    try:
        # find the relevant webapp
        webapp = Webapp.objects.get(docker_name=repo)
    except Webapp.DoesNotExist:
        return JsonResponse({"errors": [
            {"code": "NAME_INVALID", "message": "unknown repository"}]}, status=404)

    if action == "pull":
        # find the id of the WebappVersion to be pulled
        version = WebappVersion.objects.filter(webapp=webapp, number=tag,
                state=WebappVersion.READY).order_by("-id").first()
        if version is None:
            return JsonResponse({"errors": [
                {"code": "TAG_INVALID", "message": "unknown tag"}]}, status=404)

    elif action == "push":
        # create a new WebappVersion entry in state USER
        version = WebappVersion(
                webapp=webapp, number=tag, state=WebappVersion.USER,
                published=True, description=description)
        version.save()

    else:
        return HttpResponse(status=500)

    log.info("%s docker image %s:%s -> id%d",
            action, webapp.docker_name, tag, version.id)

    # return the version id of the image being pushed/pulled
    return HttpResponse(str(version.id))

@csrf_exempt
def post_push(request):
    """post-push hook for image manifests

    This endpoint is called by allgo.aio after pushing an image to the
    registry, but before the result is forwarded to the client.

    It is responsible of updating the database with the new webapp version.
    """
    if request.META.get("HTTP_X_ORIGIN") != "aio":
        # this endpoint is only usable by allgo.aio
        return HttpResponse(status=404)
    if request.method != "POST":
        return HttpResponse(status=405)

    version_id = int(request.GET["version_id"])
    success = int(request.GET["success"])

    if not success:
        # push failed
        # -> remove the version
        WebappVersion.objects.filter(id=version_id,
                state=WebappVersion.USER).delete()
    else:
        # Switch the version state to READY
        # (see PushManager._process() in controller.py for more details about the
        # process)

        # version being pushed
        version_query = WebappVersion.objects.filter(id=version_id)
        version = version_query.get()

        # query and lock candidate versions to be READY
        versions = list(version_query.union(WebappVersion.objects.filter(
            webapp=version.webapp, number=version.number,
            state=WebappVersion.READY)).select_for_update())

        # set the latest one to READY and the others to DELETED
        latest_id = max(v.id for v in versions)
        for ver in versions:
            old = ver.get_state_display()
            if ver.id == latest_id:
                ver.state = WebappVersion.READY
            else:
                ver.state = WebappVersion.DELETED
                ver.deleted_at = django.utils.timezone.now()
            new = ver.get_state_display()
            log.info("version id %d: %s -> %s", ver.id, old, new)
            ver.save()

    return HttpResponse(status=204)

@csrf_exempt
def registry_notfound(request):
    """Default endpoint for all registry urls

    should never be served (if the reverse-proxy is well configured)
    """
    return JsonResponse({"error":
        "registry not found (this is very likely a reverse-proxy config issue)"},
        status=404)


def jwt_auth(request):
    """JWT auth endpoint used by the docker registry

    Spec: https://docs.docker.com/registry/spec/auth/jwt/

    The HTTP request is expected to include an Authorization header using the
    Basic authentication method

    The client may provide either:
    - the email+password of an allgo user
    - a runner token, in which case the username is the arbitrary value "token"
      and the password is the token value

    :param request:
    :return:
    """

    #
    # Identify the actor making the request (either a User or a Runner)
    #
    auth_header = request.META.get('HTTP_AUTHORIZATION', '')
    if not auth_header:
        log.info("Token request without http authorisation %s %s %s",
                request.META['HTTP_USER_AGENT'],
                request.META['REMOTE_ADDR'], request.META['QUERY_STRING'])
        return HttpResponse(status=401)
    token_type, credentials = auth_header.split(' ')
    if token_type != "Basic":
        log.info("Token request with unknown http authentication method %s %s %r",
                request.META['HTTP_USER_AGENT'],
                request.META['REMOTE_ADDR'], token_type)
        return HttpResponse(status=401)
    username, password = base64.b64decode(credentials).decode('utf-8').split(':', 1)
    #log.debug('HTTP_AUTHORIZATION %s username %s', auth_header, username)
    if username == "token":
        if len(password) < MIN_TOKEN_SIZE:
            log.info("provided token is too short")
            return HttpResponse(status=401)
        if password == CONTROLLER_TOKEN:
            actor = "CONTROLLER"
            if not is_allowed_ip_admin(client_ip := get_client_ip(request)):
                log.warning("attempt to use controller token from unauthorized ip %s", client_ip)
                return HttpResponse(status=401)
        else:
            actor = Token.authenticate(password)
            if actor is None:
                return HttpResponse(status=401)
    else:
# FIXME: user authentication is disabled for the moment because we do not work with users
# authenticated via allauth
        return HttpResponse(status=401)
#
#        try:
#            actor = User.objects.get(email=username)
#        except User.DoesNotExist:
#            log.warning("Token request but user doest not exist")
#            return HttpResponse(status=401)
#        password_valid = actor.check_password(password)
#        if token_type != 'Basic' or not password_valid:
#            log.info("Token request but user password mismatch")
#            return HttpResponse(status=401)

    #
    # Evaluate the allowed actions
    #
    try:
        resource_type, repository, requested_actions = request.GET.get('scope', "::").split(":")
    except ValueError:
        return JsonResponse({'error': 'Invalid scope parameter'}, status=400)

    allowed_actions = set()
    if resource_type == "repository":
        if actor == "CONTROLLER":
            allowed_actions.update(("pull", "push"))
        else:
            try:
                webapp = Webapp.objects.get(docker_name = repository)
            except Webapp.DoesNotExist:
                pass
            else:
                if "push" in requested_actions and webapp.is_pushable_by(actor):
                    allowed_actions.add("push")
                    if "pull" in requested_actions:
                        # NOTE: the official docker client requests both push & pull rights for
                        # pushing (and it is unable to push without the pull permission)
                        # (anyway the nginx config prevents pulling blobs for the moment)
                        allowed_actions.add("pull")

                if "pull" in requested_actions and webapp.is_pullable_by(actor,
                        client_ip = get_client_ip(request)):
                    allowed_actions.add("pull")

    #
    # Generate the token
    #
    service = request.GET['service']
    log.info("Token authorized for %s on %s actions %s", actor, repository, allowed_actions)

    return JsonResponse({
        'token': encode_jwt_token(service, resource_type, repository, list(allowed_actions))
        })


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip
