#!/bin/sh

set -e

step() {
	echo "----------  $*  ----------"
}

step "make dirs"
mkdir -p			\
	/vol/rw/datastore	\
	/vol/rw/app		\
	/vol/rw/media		\
	/vol/rw/system		\
	/vol/log/nginx		\
	/vol/log/django		\
	/vol/log/gunicorn	\
	/vol/log/aio		\
	/vol/cache/allgo	\
	/vol/cache/nginx 


# start nginx
update-nginx-config
pid="`cat /run/nginx.pid 2>/dev/null || true`"
if [ -z "$pid" ] || [ ! -d "/proc/$pid/" ] ; then
	step "start nginx"
	nginx
else
	# If nginx is already started we just reload its config. This is just
	# for convenience. In developement this allows launching 'run-allgo'
	# manually multiple times inside an interactive container.
	step "reload nginx config"
	nginx -t
	kill -HUP "$pid"
fi

# wait until the mysql server is ready
step "wait until mysql is ready"
wait-mysql

# start the asyncio server
step "start the allgo.aio daemon"
python3 -m allgo.aio --daemon

# start allgo
case "$ALLGO_HTTP_SERVER" in
  django)
	step "start django (development mode)"
	set -x
	exec python3 manage.py runserver 0.0.0.0:8000
	;;
  ''|gunicorn)
	step "start gunicorn"
	set -x
	exec gunicorn3 -c setup/gunicorn.py config.wsgi:application
	;;
  *)
	echo "error: invalid ALLGO_HTTP_SERVER: '$ALLGO_HTTP_SERVER'" >&2
	exit 1
	;;
esac

