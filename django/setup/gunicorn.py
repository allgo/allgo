import multiprocessing
import os


bind = "0.0.0.0:8000"
workers = int(os.getenv("GUNICORN_WORKERS") or multiprocessing.cpu_count() or 1)

# restart the workers after 1000 requests (to avoid memory leaks)
max_requests = 1000
max_requests_jitter = 100

# load applications before forking the worker processes
preload_app = True

# log files
accesslog = "/vol/log/gunicorn/access.log"
errorlog  = "/vol/log/gunicorn/error.log"
