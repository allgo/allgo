#!/bin/sh

set -e -x

# make /run world writable (so that the container cae be run under any uid)
chown -R nobody /run
chmod -R a+rwX  /run

# ensure there is not stale nginx.pid in the image (because run-allgo checks it)
rm -f /run/nginx.pid



# apply patches in setup/patches/
apply-patches /opt/allgo/setup/patches/*.diff


# remove nginx default site
rm /etc/nginx/sites-enabled/default


# install all files in setup/files/
# (but using symbolic links to ease the development)
(cd /opt/allgo/setup/files && find * \! -type d | while read path ; do
	mkdir -p -- "`dirname -- "$path"`"
	rm -f -- "/$path"
	ln -s "$PWD/$path" "/$path"
done)

# create /etc/nginx/conf.d/allgo.conf and make it word-writable
# (it is dynamically generated at runtime)
(umask 0000 && touch /etc/nginx/conf.d/allgo.conf)

# install django static files into /var/www
(cd /opt/allgo && ALLGO_SECRET_KEY_PATH=/tmp/dummy_key python3 manage.py collectstatic --no-input -l -v0)

# create the allgo user
useradd -m -s /bin/bash allgo


# make the migration dir world-writable to allow generating migrations in qualif
# TODO: remove when we deploy in production
chmod 1777 /opt/allgo/allgo/main/migrations
