# -*- coding: utf-8 -*-
"""
Convert a html page into markdown or RST format

In order to use this script, you need to install pandoc on your system.
"""

import click
import pypandoc
from bs4 import BeautifulSoup
import os


def detect_html(readme_file):
    with open(readme_file, 'r') as html_detector:
        return bool(BeautifulSoup(html_detector, "html.parser").find())


@click.command(context_settings=dict(help_option_names=['-h', '--help']))
@click.option(
        '-f',
        '--input-file',
        type=click.STRING,
        help="File you want to convert")
@click.option(
        '-t',
        '--output-type',
        type=click.Choice(['md', 'rst']),
        default='md',
        help="Output type. Default ['md']")
def main(input_file, output_type):
    """Detect and convert HTML files into markdown or RST"""

    if input_file:
        if detect_html(input_file):
            output = pypandoc.convert_file(input_file, 'md', format='html')
            output_file_path = os.path.dirname(input_file)
            output_filename = output_file_path + '/README.md'
            print(output_filename)
            with open(output_filename, 'w') as output_file:
                output_file.write(output)
                output_file.close()


if __name__ == '__main__':
    main()
