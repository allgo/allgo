#!/bin/sh
#

TEMP_DIR=/tmp/sql
RAILS_TABLES="docker_os webapps webapp_parameters webapp_versions users quotas \
  job_queues jobs job_uploads"
DJANGO_TABLES="dj_runners dj_runners_webapps dj_docker_os dj_webapps dj_webapp_parameters dj_webapp_versions \
  dj_users dj_quotas dj_job_queues dj_jobs dj_job_uploads auth_group \
  auth_group_permissions auth_permission auth_user auth_user_groups \
  auth_user_user_permissions django_admin_log django_content_type \
  django_migrations django_session django_site django_admin_log \
  account_emailaddress account_emailconfirmation \
  socialaccount_socialaccount socialaccount_socialapp \
  socialaccount_socialapp_sites socialaccount_socialtoken"

CUR_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"


ParseArgs () {
  if [ $# -eq 3 ]; then
    PASS=
    USER=$1
    HOST=$2
    DB=$3
  else
    PASS="--password=$2"
    USER=$1
    HOST=$3
    DB=$4
  fi
}


DumpDb () {
  # Dump the table without the creation schema
  # using the argument -n and -t
  mkdir -p $TEMP_DIR
  for TABLE in $RAILS_TABLES
  do
    mysqldump -u $USER $PASS -h $HOST $DB $TABLE > $TEMP_DIR/db_$TABLE.sql
  done
}


DeleteDjangoTables () {
  for TABLE in $DJANGO_TABLES
  do
    mysql -u $USER $PASS -h $HOST $DB -e \
      "SET FOREIGN_KEY_CHECKS = 0; DROP TABLE IF EXISTS $TABLE; SET FOREIGN_KEY_CHECKS = 1;"
  done
}


LoadDb () {
  set -x
  # Use Django to recreate the tables
  # If start from scratch, it would be useful to empty your migration folder
  # to avoid side effects

  # Tell Django to use the current status of the database and start the migrations
  # from that point
  mysql -u $USER $PASS -h $HOST $DB -e 'ALTER DATABASE `allgo` CHARACTER SET utf8;'
  #python3 manage.py makemigrations auth sessions sites socialaccount admin contenttypes main
  python3 manage.py migrate

  # Apply the migration (through mysql insertions)
  mysql -u $USER $PASS -h $HOST $DB < $CUR_DIR/migration2django.sql
  
  # Create a super user
  python3 manage.py shell -c shell -c \
    "from django.contrib.auth.models import User;\
    User.objects.create_superuser('admin@allgo.inria.fr', 'admin@allgo.inria.fr', 'y7gu3xaKdwoVFnKxTVbz')"
}


Help() {
  cat << EOF
Use: $0 [options]
Options:
  -d, --delete-django-tables USER [PASSWORD] HOST DB    Delete the Django tables
  -e, --extract-rails-db USER [PASSWORD] HOST DB      Dump the tables of the allgo database into the sql folder
  -m, --migrate-to-django USER [PASSWORD] HOST DB      Load the tables into the allgopy database
  -h, --help                                    Show this help
EOF
}

set -e


while getopts d:e:m:h OPTIONS
do
  case $OPTIONS in

    d | --delete-django-tables)
      ParseArgs $2 $3 $4 $5
      DeleteDjangoTables 
    ;;

    e | --extract-rails-db)
      ParseArgs $2 $3 $4 $5
      DumpDb
      ;;

    m | --migrate-to-django)
      ParseArgs $2 $3 $4 $5
      LoadDb $OPTARG
      ;;

    h | --help)
      Help
    ;;

  esac
done
