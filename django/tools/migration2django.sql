-- NOTE: this script is no longer used at container init 
--       (to be resurrected when we implement on-demand import of old allgo webapps)

--
-- Migrate user data
--
INSERT INTO auth_user 
  (id, password, is_superuser, username, email, date_joined)
  SELECT id, encrypted_password, admin, email, email, created_at
  FROM users;
INSERT INTO dj_users 
  (id, user_id, sshkey)
  SELECT id, id, sshkey
  FROM users;

--
-- Migrate Docker OS data
--
INSERT dj_docker_os SELECT * FROM docker_os;

--
-- Migrate job queues data
--
INSERT INTO dj_job_queues
  (id, created_at, updated_at, name, timeout, is_default)
  SELECT id, created_at, updated_at, name, timeout, is_default
  FROM job_queues;

--
-- Migrate webapps data
--
SET foreign_key_checks=0;
INSERT INTO dj_webapps
  (id,
    created_at,
    updated_at,
    name,
    description,
    contact,
    logo_file_name,
    logo_content_type,
    logo_file_size,
    logo_updated_at,
    default_quota,
    docker_name,
    readme,
    entrypoint,
    exec_time,
    private,
    access_token,
    sandbox_state,
    memory_limit,
    docker_os_id,
    job_queue_id,
    user_id)
  SELECT 
    id,
    created_at,
    updated_at,
    name,
    description,
    contact,
    logo_file_name,
    logo_content_type,
    logo_file_size,
    logo_updated_at,
    default_quota,
    docker_name,
    readme,
    entrypoint,
    exec_time,
    private,
    access_token,
    sandbox_state,
    memory_limit,
    docker_os_id,
    default_job_queue_id,
    user_id
  FROM webapps;

--
-- Migrate webapps version data
--
INSERT INTO dj_webapp_versions
  (id, created_at, updated_at, number, description, docker_image_size, state, published, webapp_id)
  SELECT *
  FROM webapp_versions;

--
-- Migrate quotas data
--
INSERT INTO dj_quotas
  (id, created_at, updated_at, quantity, user_id, webapp_id)
  SELECT *
  FROM quotas;

--
-- Migrate jobs data
--
INSERT INTO dj_jobs
  (id, created_at, updated_at, param, datasize, version, exec_time, access_token, state, result, container_id, queue_id, user_id, webapp_id)
  SELECT id, created_at, updated_at, param, datasize, version, exec_time, access_token, state, result, container_id, queue_id, user_id, webapp_id
  FROM jobs;

--
-- Migrate job uploads data
-- FIXME: broken: ERROR 1054 (42S22) at line 104: Unknown column 'job_id' in 'field list'
--INSERT INTO dj_job_uploads
--  (id, created_at, updated_at, job_file_file_name, job_file_content_type, job_file_file_size, job_id)
--  SELECT id, job_file_updated_at, job_file_updated_at, job_file_file_name, job_file_content_type, job_file_file_size, job_id
--  from job_uploads;

--
-- Insert the sites
--
INSERT INTO `django_site` 
  VALUES (NULL, 'http://localhost:8000', 'A||go dev'),
  (NULL, 'https://allgo.inria.fr', 'A||go');

--
-- Gitlab OAuth2 login
--
INSERT INTO `socialaccount_socialapp`
  VALUES(NULL, 'gitlab', 'gitlab.irisa.fr', 'efe8e4bc3ebf02a320e5b9954e2fda5e3f1a5dc340183c533b2cc7c1962b86b5', '5f0a2c32d34bdc35fbcfccf6bf005ae4ab2d7387a1cba1996fa3d1298b614d74', '');

--
-- Connect the social app to the site ID
--
INSERT INTO `socialaccount_socialapp_sites`
  VALUES(NULL, 1, 4);
INSERT INTO `socialaccount_socialapp_sites`
  VALUES(NULL, 1, 5);
INSERT INTO `socialaccount_socialapp_sites`
  VALUES(NULL, 1, 6);

SET foreign_key_checks=1;
COMMIT;
