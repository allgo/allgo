#!/usr/bin/python3

from astroid.node_classes import Attribute, Call

from pylint import checkers, interfaces



class ObjectsCreateChecker(checkers.BaseChecker):
    __implements__ = interfaces.IAstroidChecker

    name = "objects-create-checker"
    priority = -100
    msgs = {"W9901": (".objects.create() is called.", "objects-create-called", 
        "We prefer to avoid using Queryset.create() in django because it"
        " bypasses the integrity checks implemented by"
        " allgo.main.models.BaseModel.save().")}

    def visit_attribute(self, node):
        if node.attrname == "objects":
            if (isinstance(node.parent, Attribute)
                    and node.parent.attrname=="create"
                    and isinstance(node.parent.parent, Call)):

                self.add_message("objects-create-called", node=node)


def register(linter):
    linter.register_checker(ObjectsCreateChecker(linter))
