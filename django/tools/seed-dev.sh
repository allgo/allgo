#!/bin/bash
#

print_help()
{
	cat >&2 <<EOF
usage: $0 [--django|--registry] ENV [REGISTRY]

Seed the allgo database

ENV is the environment, it is used as the prefix for container names
	(default: dev)

REGISTRY is the registry where the created images shall be pushed
	(default: "localhost:5000")

option:
  --django	seed the django db only
  --registry	seed the registry only
EOF
}

seed_django=1
seed_registry=1
if [ "$1" == --django ] ; then
	seed_registry=
	shift
elif [ "$1" == --registry ] ; then
	seed_django=
	shift
fi

if [ "$1" = "-h" ] || [ "$#" -gt 2 ] ; then
	print_help
	exit 1
fi

ENV="${1:-dev}"
REGISTRY="${2:-localhost:5000}"

echo "ENV is $ENV"
echo "REGISTRY is $REGISTRY"

set -e

# seed the django db
if [ -n "$seed_django" ]
then
	# load the default fixture (allgo/main/fixtures/default.json)
	docker exec -- "$ENV-django" python3 manage.py loaddata default

	# load the ssh public keys of the current user into the admin account
	if keys="`ssh-add -L`" ; then
		while read key ; do
			docker exec -- "$ENV-django" python3 manage.py create add_sshkey admin@localhost "$key"
		done <<EOF
$keys
EOF
	fi
fi

if [ -n "$seed_registry" ]
then
	IMAGE="$REGISTRY/sleep:id1"
	echo "build image $IMAGE"
	docker build -t "$IMAGE" -- "`dirname "$0"`/sleep"

	docker login --username token --password "`<data/django/ro/controller_token`" -- "$IMAGE"
	docker push   -- "$IMAGE"
	docker logout -- "$IMAGE"
fi
