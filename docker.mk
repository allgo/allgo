
DOCKER_DEPS = .deps.docker.mk

# prefix to be prepended before the path of the context (for 'docker build')
# (may be needed when building on a host where user namespaces are enabled)
DOCKER_CONTEXT_PREFIX =

_docker_default: build-all

include $(DOCKER_DEPS)

STAMPS   = $(addprefix .stamp.,$(IMAGES))
PREPARE_TARGETS = $(addprefix prepare-,$(IMAGES))

FORCE:

$(DOCKER_DEPS): FORCE
	./make_deps $@ $(PREFIX)

build-all: $(STAMPS)
rebuild: FORCE
	rm -f .stamp.*
	$(MAKE) build-all


$(IMAGES):%: FORCE
	#
	#	$@
	#
	rm -f   '.stamp.$@'
	$(MAKE) '.stamp.$@'

$(PREPARE_TARGETS):

.stamp.%: image = $(patsubst .stamp.%,%,$@)
.stamp.%: tag = $(PREFIX)$(patsubst .stamp.%,%,$@)
.stamp.%:
	#
	#	$@
	#
	@$(MAKE) "prepare-$(image)"
	docker build -t '$(tag)' '$(DOCKER_CONTEXT_PREFIX)$(shell realpath --relative-to=. $(image))/'
	touch '$@'

clean-untagged-images:
	images="`docker images --no-trunc|grep '^<none>' | awk '{ print $$3 }'`" [ -z "$$images" ] || docker rmi $$images

clean: _docker_clean
_docker_clean: clean-untagged-images
	rm -f .stamp.* '$(DOCKER_DEPS)'


