#!/bin/sh

set -e -x

sed -i "s/^daemons=.*/daemons=1/" /etc/courier/authdaemonrc
sed -i "s/MAXDAEMONS=.*/MAXDAEMONS=1/" /etc/courier/imapd 


useradd --create-home sink
echo sink:sink | chpasswd

mkdir -p /home/sink
ln -s /vol/rw /home/sink/Maildir

rm -rf /context

