-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: libnss-mysql-bg
Binary: libnss-mysql-bg
Architecture: any
Version: 1.5-3
Maintainer: Emmanuel Lacour <elacour@home-dn.net>
Standards-Version: 3.9.3
Build-Depends: debhelper (>= 9), libmysqlclient15-dev, quilt (>= 0.40), autotools-dev, dh-autoreconf
Package-List: 
 libnss-mysql-bg deb admin optional
Checksums-Sha1: 
 08dcc20a07a524bb19daa7bde358023fd09c20b7 335078 libnss-mysql-bg_1.5.orig.tar.gz
 a9772bd010652ceb1c5cd7a71cbe19df16179f5e 4934 libnss-mysql-bg_1.5-3.debian.tar.gz
Checksums-Sha256: 
 1ccd6bbb8f334600093c2ffb949d50ba228e05e561061ffcffdcca42db3b956f 335078 libnss-mysql-bg_1.5.orig.tar.gz
 7d4d8193da822a7755d63cc9673cd25fcec71b6d1920d00500cd77bc0062be04 4934 libnss-mysql-bg_1.5-3.debian.tar.gz
Files: 
 e7ae29d8a9ad55aa9972e47cbe24544d 335078 libnss-mysql-bg_1.5.orig.tar.gz
 b4799069db7e0e4b420d17ff10baff94 4934 libnss-mysql-bg_1.5-3.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQIcBAEBCAAGBQJPqiA0AAoJECrpAeXHAhjS+wcP/3/hTjo7GagHLnESKjdwW6iA
Ly/xqGvUgS11Xb0rk94sx7SWZCr82Aw91bgYMeffM3FGi982r2UyRMX0fxojEqlO
7RNrjq3pbjvgSyripYtv9TWT397fRwzzjxmXzKBV9C4ADzUNjgrB94CAAV4PcK9E
z8k0INu94lif5vVQtQ3DmdL7nJRk5e4KoV8mVTbxIvik68r/FyumrzILK+Vc+M6T
5iHuwYKpAjvSu9TQNIqWz+uWVTTIXYQkXLs9f2AOMSbVjR/vI52EWLBxDRp+beDs
CT9Rj6YkQNn9j70BZxwGl4gWuVvJDHpat9iCzsFAhNPFctR8tzmyHnN5dI7VYY+t
L3cJ87xPKfX+tfRvFBhx3AIONl9AssqLfuH12eDtuSakWaXpd4VvKDNuvQOxkXT4
zFXWV/mgaarV+jxuFI1N+uMKJPjSrtHuOoX2HhKkixphAFYPxK2phyBvfBHw1V1s
kJ86gKZKd3YvJNMDxaCj2atKfnR0A1cjjau+NiXHFRedylZxohw69LwXinYaGDV2
4XRFprmCDRo+bC4fNu075Su+q8JCuT3qvJ0LElOgJkoIQiLXJbVkebdpYz7nOueT
RN6Z1qet0Q5BCJc+Ta3q4GBCSYuyEx6wq9vhMKFWU0too+6y000JaiO1o1noy8hl
55CPhvAhsUAPXDtUSfSC
=uGyZ
-----END PGP SIGNATURE-----
