#include<regex.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include<mysql.h>


void die(const char* msg)
{
	fprintf(stderr, "error: %s\n", msg);
	exit(1);
}
void die_mysqlerror(MYSQL* my, const char* msg)
{
	fprintf(stderr, "error: %s: %s\n", msg, mysql_error(my));
	exit(1);
}

int main(int argc, char* argv[])
{
	if (argc != 3) {
		die("usage: allgo-authorized-keys-command WEBAPPNAME ENV");
	}

	const char* user = argv[1];
	const char* env  = argv[2];

	char* db_host = malloc(strlen(env) + 7);
	sprintf(db_host, "%s-mysql", env);

	MYSQL* my = mysql_init(NULL);
	if (!my) {
		die_mysqlerror(my, "mysql_init");
	}

	if (!mysql_real_connect(my, db_host, "ssh", NULL, "allgo", 0, NULL, 0)) {
		die_mysqlerror(my, "mysql_real_connect");
	}

	int name_len = strlen(user);
	char* escaped_name = malloc((name_len+1)*2);
	if (!escaped_name) {
		die("malloc");
	}

	if(mysql_real_escape_string(my, escaped_name, user, name_len) < 0) {
		die_mysqlerror(my, "mysql_real_escape_string");
	}
	
	const char* template = "SELECT sshkey FROM dj_webapps JOIN auth_user ON (auth_user.id=dj_webapps.user_id OR auth_user.is_superuser=1) JOIN dj_users ON dj_users.user_id=auth_user.id WHERE dj_webapps.docker_name='%s'";

	char* cmd = malloc(strlen(template) + strlen(escaped_name));
	if (!cmd) {
		die("malloc");
	}

	sprintf(cmd, template, escaped_name);

	if (mysql_query(my, cmd)) {
		die_mysqlerror(my, "mysql_query");
	}

	MYSQL_RES *result = mysql_store_result(my);
  	if (!result) 
  	{
		die_mysqlerror(my, "mysql_store_result");
  	}

	regex_t reg_sshkey;
	if(regcomp(&reg_sshkey, "[[:blank:]]*([a-z][a-z0-9-]*[[:blank:]]+[A-Za-z0-9+/]+=*)($|[[:blank:]])", REG_EXTENDED))
	{
		die("regcomp");
	}

	MYSQL_ROW row;
	while ((row = mysql_fetch_row(result)))
	{
		unsigned long* lengths = mysql_fetch_lengths(result);
		char* p = row[0];
		char* end = p + lengths[0]; 
		// splitlines
		while (p < end)
		{
			char* nl = strstr(p, "\n");
			if(nl) {
				*nl = '\0';
			}
			
			regmatch_t pm;
			if(regexec(&reg_sshkey, p, 1, &pm, 0) == 0) {
				fwrite(p+pm.rm_so, pm.rm_eo-pm.rm_so, 1, stdout);
				puts("");
			}
			p += strlen(p) + 1;
		}

	}
}

