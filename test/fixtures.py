"""

Django setup script to create a build@test.com user and a fake app build_app

"""
import os
from django.contrib.auth.models import User
from main.models import Webapp, AllgoUser, Runner

print("Load test script ...")
email = 'build@test.com'
username = 'build@test.com'
password = 'build'

if User.objects.get(username=username):
    User.objects.get(username=username).delete()

u = User.objects.create_superuser(username, email, password)
if not AllgoUser.objects.get(user=u):
    AllgoUser(user=u).save()

au = AllgoUser.objects.get(user=u)
au.token = os.environ['ALLGO_TOKEN']
au.save()


app_name = os.environ['ALLGO_APP_NAME']
w = Webapp.objects.filter(name=app_name).first()
if not w:
    w = Webapp()
    w.name = app_name
    w.docker_name = app_name.lower()
    w.sandbox_state = 0
    w.job_queue_id = 1
    w.user_id = u.id
    w.docker_os_id = 1
    w.save()

run_token = os.environ['ALLGO_RUNNER_TOKEN']
if not Runner.objects.filter(token=run_token):
    r = Runner(token=run_token)
    # r.webapps = [w]
    r.save()
