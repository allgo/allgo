import os
from main.models import Webapp, AllgoUser, Runner, Job


app_name = os.environ['ALLGO_APP_NAME']
run_token = os.environ['ALLGO_RUNNER_TOKEN']
w = Webapp.objects.get(name=app_name)
r = Runner.objects.get(token=run_token)
for j in Job.objects.filter(webapp=w):
   print("Set job %s with runner %s" % (j.id, r.id))
   j.runner = r
   j.save()
