import hashlib
import json
import socket
import tarfile
import unittest
import docker
import time
from io import BytesIO

import redis
import requests

client = docker.from_env()

env = {'ALLGO_TOKEN': 'a84475640f5d709d4144887df59cee75',
       'ALLGO_SECRET_KEY': 'a84475640f5d709d4144887df59cee75',
       'ALLGO_RUNNER_TOKEN': 'IPcWhmSfBucEyxyGCCBeYFsGpWeSsWLp',
       'ALLGO_APP_NAME': "test_build" }
        #'ALLGO_APP_NAME': "test_%s" % int(time.time())}


dockerfile = '''
FROM debian
WORKDIR "/tmp"
CMD /bin/bash -c "echo 'Disk usage in progress ... %s'; du -sch /tmp | tee /tmp/results.txt ; echo 'Finish';"
''' % env['ALLGO_APP_NAME']

BUF_SIZE = 65536


def sha1file(filepath):
    sha1 = hashlib.sha1()
    with open(filepath, 'rb') as f:
        while True:
            data = f.read(BUF_SIZE)
            if not data:
                break
            sha1.update(data)
    return sha1.hexdigest()


class TestClass(unittest.TestCase):
    headers = {'Authorization': 'Token token=%s' % env['ALLGO_TOKEN']}
    jobid = None

    def setUp(self):
        self.exec2django('fixtures.py')

    def setup_as_bytes_tar(self, file):
        bio = BytesIO()
        tar = tarfile.open(mode="w:gz", fileobj=bio)
        tar.add(file)
        tar.close()
        bio.seek(0)
        return bio

    def exec2django(self, file):
        print("Exec2Django file %s" % file)
        django = client.containers.get('dev-django')
        print(django.exec_run("rm /tmp/%s" % file))
        django.put_archive('/tmp', self.setup_as_bytes_tar(file))
        cmd = "python3 manage.py shell -c \"exec(open('/tmp/%s').read())\" " % file
        exit_code, output = django.exec_run(cmd, environment=env)
        print(exit_code)
        for l in output.decode('utf8').split('\n'):
            print(l)
        print(env)

    # def tearDown(self):
    # print("tearDown")


    def test_complete(self):
        r = client.login("build@test.com", "build", registry='http://localhost:8080')
        self.assertEquals(r['Status'], 'Login Succeeded')
        print(r['Status'])

        #test_002_docker_build_and_push(self):
        ######################################################
        r = redis.Redis()
        p = r.pubsub(ignore_subscribe_messages=True)
        p.subscribe('registry:hook')
        l = client.login("build@test.com", "build", registry='http://localhost:8080')
        print(l)
        tag = "localhost:8080/%s" % env['ALLGO_APP_NAME']
        print("Build image %s" % tag)
        # client.images.build(path=".", tag=tag)
        f = BytesIO(dockerfile.encode('utf-8'))
        client.images.build(fileobj=f, tag=tag)
        print("Push image %s" % tag)
        messages = client.images.push(tag)
        self.assertFalse('error' in messages.lower(), messages)
        print(type(messages))

        message = p.get_message()
        while not message:
            time.sleep(1)
            message = p.get_message()
        msg = json.loads(message['data'])
        print(msg['target']['repository'])
        self.assertEquals(msg['target']['repository'], tag.split('/')[1])

        # test_003_runner(self):
        ######################################################
        for c in client.containers.list(all=True):
            if c.name == 'test-allgo-runner':
                c.stop()
                c.remove()
        args = "- %s http://%s:8080" % (env['ALLGO_RUNNER_TOKEN'], socket.gethostname())
        print(args)
        volumes = {'/var/run/docker.sock': {'bind': '/var/run/docker.sock', 'mode': 'rw'}}
        c = client.containers.run('allgo-runner', args, detach=True,
                                  volumes=volumes, network_mode='host',
                                  name='test-allgo-runner')
        time.sleep(10)
        self.assertEquals(c.status, 'created')
        print(c.logs())

        #test_004_http_post(self):
        ######################################################
        data = {'job[webapp_id]': env['ALLGO_APP_NAME'],
                'job[param]': '',
                'job[queue]': 'standard'}
        files = {'test.py': open('test.py', 'rb')}
        r = requests.post("http://localhost:8080/api/v1/jobs", data=data, files=files, headers=self.headers)
        print(r.content)
        self.jobid = list(r.json().keys())[0]
        self.assertEquals(r.status_code, 200)

        print("Jobid %s : %s" % (self.jobid, r.json()))
        url = "http://localhost:8080/api/v1/jobs/%s" % self.jobid
        print(url, self.headers)
        r = requests.get(url, headers=self.headers)
        print(r.content)
        self.assertEquals(r.status_code, 200)

        for filename, url in r.json()[self.jobid]['files'].items():
            print(url)
            r = requests.get(url, headers=self.headers)
            self.assertEquals(r.status_code, 200)
            sha1 = hashlib.sha1()
            sha1.update(r.content)
            if filename == 'test.py':
                print("test digest %s %s" % (sha1.hexdigest(), sha1file('test.py')))
                self.assertEquals(sha1.hexdigest(), sha1file('test.py'))

        #test_005_runner_job(self):
        ######################################################
        self.exec2django('job2runner.py')
        url = "http://localhost:8080/api/v1/jobs/%s" % self.jobid
        print(url, self.headers)
        r = requests.get(url, headers=self.headers)
        print("-----", r.content, r.json()[self.jobid]['status'])
        i = 0
        while r.json()[self.jobid]['status'] == 'new' or r.json()[self.jobid]['status'] == 'waiting':
            time.sleep(1)
            r = requests.get(url, headers=self.headers)
            print(r.content)
            i += 1
            if i > 10:
                break
        self.assertFalse(12, 13)

