## Stage 1: build a customised version of openssh (that expects to be installed inside /.toolbox/) ##
FROM allgo/base-debian AS builder
RUN set -x ;\
	sed -i '/^deb /p ; s/^deb /deb-src /' /etc/apt/sources.list	&&\
	apt-getq update							&&\
	apt-getq build-dep openssh					&&\
	mkdir /src && cd /src && apt-get source openssh

COPY ssh_*.diff /src/
RUN set -x ;\
	cd /src/openssh-*			&&\
	patch -p1 < ../ssh_override_paths.diff	&&\
	patch -p1 < ../ssh_static_privsep.diff	&&\
	dpkg-buildpackage -b			&&\
	cd ..					&&\
	for pkg in openssh-client openssh-server ; do mv "$pkg"_*.deb "$pkg".deb ; done


## Stage 2: build the toolbox image ##
FROM allgo/base-debian

RUN apt-getq install	\
	binutils	\
	less		\
	nano		\
	ncurses-term	\
	netcat-openbsd	\
	openssh-client	\
	openssh-server	\
	procps		\
	rsync		\
	vim-gtk		\
	xauth

# overwrite the official packages with our patched packages openssh-{server,client}.deb
# Notes:
# - we put the 'hold' mark so that automatic upgrades will not replace them
# - we touch every installed files because as of stretch the debian builds are
#   reproducible (otherwise, because the timestamp of the package files is
#   identical when the package is rebuilt, docker would not detect the change
#   and would not install the new file in the resulting image
COPY --from=builder /src/*.deb /tmp/
RUN set -x -e;\
	for pkg in openssh-client openssh-server ; do		\
		dpkg -i "/tmp/$pkg.deb"				;\
		apt-mark hold "$pkg"				;\
		dpkg -L "$pkg" | xargs -d \\n touch		;\
		rm -f "/tmp/$pkg.deb"				;\
	done

COPY files/. /

CMD ["toolbox-update"]
