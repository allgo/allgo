**This folder contains a set of tools in order to build AllGo
plateform on Vagrand infrastructure :**

-  clean_vagrant_allgo.sh:
   * Clean all vagrant container
-  launch_vagrant_allgo.sh: 
   * Prepare your environement and create a vagrant container depending you have a 2 or 4 CPU machine. Then launch AllGo platform inside this container
-  Vagrantfile		
   * Configuration file for vagrant environement
-  Vagrantfile-2CPU	
   * template for a 2CPU machine
-  Vagrantfile-4CPU	
   * template for a 4CPU machine
-  ansible.cfg		
   * ansible configuration filer
-  allgo.yml
   * allgo configuration file for vagrant environement
-  install_docker.yml	
   * docker configuration file for vagrant environement
-  ssh_tunnel.sh
   * configuration file containing ssh parameters for connecting to AllGo platform from terminal
