#!/bin/sh -v
# Lancement d'un conteneur vagrand pour allgo

# ___________________________ STEP 1 ______________________________________________________
brew upgrade -v; brew update -v
# ___________________________ STEP 2 ______________________________________________________
brew install ansible docker-compose -v
# ___________________________ STEP 3 ______________________________________________________
brew install --cask vagrant
# ___________________________ STEP 4 ______________________________________________________
ansible-galaxy install geerlingguy.docker
# ___________________________ STEP 5 ______________________________________________________
#vagrant init
# ___________________________ STEP 6 ______________________________________________________
#vagrant destroy -f
# ___________________________ STEP 7 ______________________________________________________
# Check CPU numbers
if [[ $(sysctl -a | grep "machdep.cpu.core_count: 4") ]]; then
    cp Vagrantfile-2CPU Vagrantfile
else
    cp Vagrantfile-4CPU Vagrantfile
fi

vagrant up --provision
# ___________________________ STEP 8 ______________________________________________________
bash ssh_tunnel.sh& sleep 10; open https://localhost:8443/

# ssh a la main
# !!__id_file__!! cat /tmp/ssh-config | grep IdentityFile | awk '{print $2}'
# ssh -p 2222 -i !!__id_file__!! 

# If it does not start
# * Check containers
#   localhost> vagrant ssh;
#   vagrant machine> cd allgo; sudo rm -rf data/*; ./bootstrap; docker ps; docker-compose ps; docker-compose up -d
#   localhost>  bash ssh_tunnel.sh& sleep 10; open https://localhost:8443/

# if guest/admin/devel account does not work :
#  ./django/tools/seed-dev.sh

