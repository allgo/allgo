#!/bin/bash
set -x
lsof -i:8443
lsof -ti:8443 | xargs kill -9
lsof -i:8443
# be sure that VM is up before starting ssh-config
vagrant up
ssh_config=$(vagrant ssh-config > /tmp/ssh-config)
port=$(cat /tmp/ssh-config | grep Port | awk '{print $2}')
identity=$(cat /tmp/ssh-config | grep IdentityFile | awk '{print $2}')
local_port=8443
distant_port=443
# -o "StrictHostKeyChecking no" to avoid host key checking
ssh -o "StrictHostKeyChecking no" -N -p $port -i $identity vagrant@localhost -L $local_port:localhost:$distant_port
